-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 22-06-2021 a las 04:40:34
-- Versión del servidor: 10.4.18-MariaDB
-- Versión de PHP: 8.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `shape_pc`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auriculars`
--

CREATE TABLE `auriculars` (
  `id_auriculars` int(4) NOT NULL,
  `tipus` varchar(50) NOT NULL,
  `tipus_de_so` varchar(50) NOT NULL,
  `connexio` varchar(50) NOT NULL,
  `dimensions` varchar(50) NOT NULL,
  `sensibilitat` varchar(50) NOT NULL,
  `frequencia` varchar(50) NOT NULL,
  `id_producte_fk` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `auriculars`
--

INSERT INTO `auriculars` (`id_auriculars`, `tipus`, `tipus_de_so`, `connexio`, `dimensions`, `sensibilitat`, `frequencia`, `id_producte_fk`) VALUES
(1, 'Gaming', ' DTS Headphone:X 2.0, 3D, 7.1 canals', 'Connector alàmbric', '182 x 82 x 172 mm', '107 db', '20 - 20000 Hz', 1),
(2, 'Gaming', 'So Virtual amb 7.1 canals', 'USB 2.0', '170 x 80 x 200 mm', '109 dB ± 3 dB', '20Hz - 20KHz', 2),
(3, 'Gaming', 'So amb 7.1 canals, virtual', 'USB', '220 mm x 70 x 210 mm', '108dB ± 3dB', '20Hz - 20kHz', 3),
(4, 'Gaming', 'Estèreo, 7.1 canals virtuals', 'Jack 3.5 estéreo', 'Auriculars de 40 mm', '92 ± 4dB', '20Hz-20,000Hz', 4),
(5, 'Gaming', 'So envolvent virtual, 7.1 canals', 'Wifi, inalambric', '210 x 118 x 230 mm', '99 ± 2dB', '20Hz - 20.000Hz', 5),
(6, 'Gaming', 'So THX Spatial', 'USB inalámbrica', '185 x 219 x 100 mm', '107 ± 3 dB', '20 - 20000 Hz', 6),
(7, 'Bàsic', 'Estereo, binaural 3D', 'USB', '180 x 165 x 68 mm', '95 dB', '42 - 17000 Hz', 7),
(8, 'Bàsic', 'Binaural 3D', 'USB', '170 x 67 x 170 mm', '110 dB', '20 - 20000 Hz', 8),
(9, 'Bàsic', 'Binaural 3D', 'USB', '172 x 76 x 154 mm', '-40 dBV/Pa +/- 3 dB', '20 Hz – 20.000 Hz', 9),
(10, 'Bàsic', 'Binaural 3D', 'USB', '200 x 245 x 72 mm', '94 dBV/Pa +/- 3 dB', '20 Hz – 20 kHz', 10),
(11, 'Gaming', 'Estereo, virtual 7.1', 'Jack 3.5', '205 x 101 x 198 mm', '116 dB', '20 Hz – 20.000 Hz', 11),
(12, 'Gaming', 'DTS headphone', 'USB 2.0', '180 x 190 x 88 mm', '87,5 dB SPL/mW', '20 - 20000 Hz', 12),
(13, 'Gaming', 'THX Spatial Audio, 7.1', 'USB', '208 x 110 x 212 mm', '100 dBSPL/mW', '12 Hz–28 kHz', 13),
(14, 'Bàsic', 'Binaural 3D, 2.0', 'USB', '140 x 180 x 60', '105 dB', '20Hz - 20KHz', 14);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `caixes`
--

CREATE TABLE `caixes` (
  `id_caixa` int(4) NOT NULL,
  `material` varchar(50) NOT NULL,
  `tipus` varchar(50) NOT NULL,
  `slots_pci` varchar(50) NOT NULL,
  `connexions` varchar(100) NOT NULL,
  `ventilacio` varchar(50) NOT NULL,
  `retroiluminacio` varchar(50) NOT NULL,
  `dimensions` varchar(50) NOT NULL,
  `pes` varchar(50) NOT NULL,
  `id_producte_fk` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `caixes`
--

INSERT INTO `caixes` (`id_caixa`, `material`, `tipus`, `slots_pci`, `connexions`, `ventilacio`, `retroiluminacio`, `dimensions`, `pes`, `id_producte_fk`) VALUES
(1, 'SPCC (Negre)', 'ATX, Micro-ATX', '7', 'USB 2.0 x 2 + USB 3.0 x 1', '1 ventilador posterior inclòs', 'RGB', '340x180x410 mm', '3.1 kg', 15),
(2, 'Acer SPCC 0.6MM', 'ATX, Micro-ATX', '7', 'USB 3.0 x 1 + USB 2.0 x 2 + HD Audio x 1', '1 ventilador posterior inclòs', 'RGB', '400x181x440 mm', '3.40 kg', 16),
(3, 'SPCC 0.6mm, Vidre temperat', 'ATX', '7', 'USB 3.0 x 1 + USB 2.0 x 2 + HD Audio x 1', '3 ventiladors frontals i 1 posterior inclosos', 'ARGB', '380x210x470 mm', '4.10 kg', 17),
(4, 'Acer 0,6mm SPCC, Vidre temperat', 'E-ATX, ATX, Micro-ATX', '7', 'USB 3.0 x 2 + HD Audio x1', '3 ventiladors frontals i 1 posterior inclosos', 'RGB', '435mm x 201mm x 435mm', '6,2 kg', 18),
(5, 'Acer de 0.6mm, Vidre temperat', 'E-ATX, ATX', '7', ' 2 x USB 2.0 + 1 x USB 3.0 + HD Audio i Micro x 1', '3 ventiladors frontals i 1 posterior inclosos', 'ARGB', '400 x 208 x 458 mm', '5.5 kg', 19),
(6, 'Acer SPCC 0.5 mm, Frontal plàstic ABS', 'ATX, Micro-ATX', '7', 'USB 3.0 x 1 + USB 2.0 x 2 + HD audio x 1', '1 ventiladors frontal i 1 posterior inclosos', 'ARGB', '206 x 460 x 470 mm', '4.8 kg', 20),
(7, 'Acer, ABS, Vidre temperat', 'ATX, Micro-ATX', '7', 'USB 3.2 Gen1 Tipo-A x 2 + HD Audio x 1 + Micrófon x 1', '2 ventiladors frontals i 1 posterior inclosos', 'RGB', '421 x 210 x 499 mm', '5.6 kg', 21),
(8, 'SPCC', 'ATX, ITX', '7', 'USB 2.0 x 2 + USB 3.1 Gen1 Tipo-A x 1 + HD Audio x 1', '1 ventilaor posterior inclòs', 'ARGB', '465 x 218 x 472 mm', '6.2 kg', 22),
(9, 'SPCC, Vidre ABS', 'ATX, Micro-ATX', '7', 'USB 3.0 x 1 + USB 2.0 x 2 + HD Audio i Microfon x 1', '1 ventilaor posterior inclòs', 'RGB', '198 x 459 x 413 mm', '3,8 kg', 23),
(10, 'Acer, Vidre temperat', 'ATX', '7', 'USB 3.0 x 1 + USB 2.0 x 2 + HD Audio x 1', '2 ventiladors frontals i 1 posterior inclosos', 'RGB', '440 x 210 x 450 mm', '6.35 kg', 24),
(11, 'SPCC, Vidre temperat', 'ATX, Micro-ATX, ITX', '7', 'SB 3.0 x 1 + USB 2.0 x 2 + HD Audio  i micro x 1', '1 ventilaor posterior inclòs', 'RGB ', '403x437x202 mm', '4.4 kg', 25),
(12, ' Malla, ABS, Acer, Vidre temperat', 'ATX, Micro-ATX', '7', ' USB 3.2 Gen 1 (3.1 Gen 1) x 2', '3 ventiladors frontals i 1 posterior inclosos', 'ARGB', '380 x 211 x 450 mm', '6.5 kg', 26);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

CREATE TABLE `categoria` (
  `id_cat` int(4) NOT NULL,
  `nom` varchar(100) NOT NULL,
  `imatge` text NOT NULL,
  `logo` text NOT NULL,
  `id_supercat_fk` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`id_cat`, `nom`, `imatge`, `logo`, `id_supercat_fk`) VALUES
(1, 'PROCESSADORS', '/imatges/categories/cpus.jpg', '/imatges/categories/logos/cpu.png', 1),
(2, 'PLAQUES MARE', '/imatges/categories/plaques.jpg', '/imatges/categories/logos/placa_mare.png', 1),
(3, 'AURICULARS', '/imatges/categories/auriculars.jpg', '/imatges/categories/logos/auriculars.png', 2),
(4, 'GRAFIQUES', '/imatges/categories/grafiques.jpg', '/imatges/categories/logos/grafica.png', 1),
(5, 'MEMORIES', '/imatges/categories/memories.jpg', '/imatges/categories/logos/ram.png /imatges/categories/logos/ssd.png /imatges/categories/logos/hdd.png', 1),
(6, 'MONITORS', '/imatges/categories/monitors.jpg', '/imatges/categories/logos/monitor.png', 2),
(7, 'RATOLINS', '/imatges/categories/ratolins.jpg', '/imatges/categories/logos/ratoli.png', 2),
(8, 'FONTS ALIMENTACIO', '/imatges/categories/alimentacio.jpg', '/imatges/categories/logos/font.png', 1),
(9, 'REFRIGERACIO', '/imatges/categories/refrigeracio.jpg', '/imatges/categories/logos/refrigeracio.png', 1),
(10, 'CAIXES', '/imatges/categories/caixes.jpg', '/imatges/categories/logos/caixa.png', 1),
(11, 'TECLATS', '/imatges/categories/teclats.jpg', '/imatges/categories/logos/teclat.png', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dades_facturacio`
--

CREATE TABLE `dades_facturacio` (
  `id_facturacio` int(4) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `pais` varchar(50) NOT NULL,
  `provincia` varchar(50) NOT NULL,
  `ciutat` varchar(50) NOT NULL,
  `direccio` varchar(50) NOT NULL,
  `detall_dir` varchar(50) NOT NULL,
  `codi_postal` varchar(5) NOT NULL,
  `telefon` varchar(10) NOT NULL,
  `id_ordre_fk` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fonts_alimentacio`
--

CREATE TABLE `fonts_alimentacio` (
  `id_font` int(4) NOT NULL,
  `potencia` varchar(50) NOT NULL,
  `connexions` varchar(50) NOT NULL,
  `dimensions` varchar(50) NOT NULL,
  `id_producte_fk` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `fonts_alimentacio`
--

INSERT INTO `fonts_alimentacio` (`id_font`, `potencia`, `connexions`, `dimensions`, `id_producte_fk`) VALUES
(1, '650 W', '5 de 4 pins, 8 SATA, 1 CPU', '160 x 150 x 86 mm', 43),
(2, '650 W', '2 PATA, 5 SATA, 1 CPU', '150 x 140 x 85 mm', 44),
(3, '600 W', '1 CPU, 3 SATA, 2 IDE', '130 x 1250 x 75 mm', 45),
(4, '750 W', '1 de 24 pins, 8 SATA, 1 CPU', '170 x 150 x 86 mm', 46),
(5, '650 W', '7 SATA, 2 PCI, 1 CPU', '150 x 125 x 86 mm', 47),
(6, '550 W', '7 SATA, 2 PCI, 1 CPU', '150 x 125 x 86 mm', 48),
(7, '500 W', '4 SATA, 3 PCI, 1 CPU', '150 x 140 x 86 mm', 49),
(8, '750 W', '6 SATA, 1 PCI, 1 CPU', '150 x 160 x 85 mm', 50),
(9, '700 W', '6 SATA, 1 PCI, 1 CPU, 4 Periferics', '150 x 140 x 86 mm', 51),
(10, '700 W', '6 SATA, 1 PCI, 1 CPU, 5 Periferics', '150 x 140 x 86 mm', 52);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grafiques`
--

CREATE TABLE `grafiques` (
  `id_grafica` int(4) NOT NULL,
  `nuclis` varchar(50) NOT NULL,
  `frequencia` varchar(50) NOT NULL,
  `memoria` varchar(50) NOT NULL,
  `adaptador` varchar(50) NOT NULL,
  `interficie` varchar(50) NOT NULL,
  `max_resolucio` varchar(50) NOT NULL,
  `id_producte_fk` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `grafiques`
--

INSERT INTO `grafiques` (`id_grafica`, `nuclis`, `frequencia`, `memoria`, `adaptador`, `interficie`, `max_resolucio`, `id_producte_fk`) VALUES
(1, '3584', '1544 MHz', '11 GB', 'GDDR5X', 'PCI Express x 16 3.0', '7680 x 4320 pixels', 128),
(2, '1920', '1784 MHz', '8 GB', 'GDDR5', 'PCI-E 3.0 x 16', '7680 x 4320 pixels', 129),
(3, '2560', '1605 MHz', '8 GB', 'GDDR6', 'PCI Express 3.0', '7680 x 4320 píxels', 130),
(4, '1920', '1755 MHz', '6 GB', 'GDDR6', 'PCI-E 3.0 x 16', ' 7680 x 4320 píxels', 131),
(5, '1408', '1830 MHz', '6 GB', 'GDDR6', 'PCI-E 3.0 x 16', '7680 x 4320 píxels', 132),
(6, '1920', '1695 MHz', '6 GB', 'GDDR6', 'PCI-E 3.0', '7680 x 4320 píxels', 133),
(7, '2100', '1183 MHz', '2 GB', 'GDDR5', 'PCI Express x16 3.0', '5120 x 2880 píxels', 134),
(8, '5120', '1825 - 2250 MHz', '16 GB', 'GDDR6', 'PCI Express 4.0', '7680 x 4320 píxels', 135),
(9, '3072', '15500 MHz', '8 GB', 'GDDR6', 'PCI-E 3.0 x 16', '7680 x 4320 píxels', 136),
(10, '10496', '1785 MHz', '24 GB', 'GDDR6', 'PCI-Express x4', '7680 x 4320 píxels', 137),
(11, '5120', '14000 MHz', '8 GB', 'GDDR6', 'PCI-Express x16', '7680 x 4320 píxels', 138),
(12, '3584', '15000 MHz', '12 GB', 'GDDR6', 'PCI-E 3.0 x 16', '7680 x 4320 píxels', 139);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `linia_ordre`
--

CREATE TABLE `linia_ordre` (
  `id_linia` int(4) NOT NULL,
  `quantitat` int(4) NOT NULL,
  `preu` decimal(6,2) NOT NULL,
  `id_ordre_fk` int(4) NOT NULL,
  `id_producte_fk` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `memories`
--

CREATE TABLE `memories` (
  `id_memoria` int(4) NOT NULL,
  `tipus` varchar(50) NOT NULL,
  `capacitat` varchar(50) NOT NULL,
  `velocitat` varchar(50) NOT NULL,
  `retroiluminacio` varchar(50) NOT NULL,
  `latencia` varchar(50) NOT NULL,
  `dimensions` varchar(50) NOT NULL,
  `id_producte_fk` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `memories`
--

INSERT INTO `memories` (`id_memoria`, `tipus`, `capacitat`, `velocitat`, `retroiluminacio`, `latencia`, `dimensions`, `id_producte_fk`) VALUES
(1, 'RAM', '8GB', '3200Mhz', 'RGB', 'CL16', '7 x 133 x 34 mm', 53),
(2, 'RAM', '16GB 2x8GB', '3200Mhz', 'No', 'CL16', '7 x 133 x 34 mm', 54),
(3, 'RAM', '8GB', 'DDR4 3200Mhz', 'No', 'CL16', '7 x 133 x 34 mm', 55),
(4, 'RAM', '16GB 2x8GB', 'DDR4 3200MHz', 'No', 'CL16', '7 x 133 x 34 mm', 56),
(5, 'RAM', '16GB 2x8GB', 'DDR4 3200Mhz', 'RGB', 'CL16', '7 x 138 x 51 mm', 57),
(6, 'RAM', '32GB 2x16GB', 'DDR4 3200MHz', 'RGB', 'CL16', '7 x 133 x 41 mm', 58),
(7, 'RAM', '16GB 2x8GB', 'DDR4 3200Mhz', 'RGB', 'CL16', '160 x 137 x 14 ', 59),
(8, 'SSD NVMe', '256GB', 'R (3400MB/s) - W (1950MB/s)', 'No', '-', '80 x 22 x 2,5 mm', 60),
(9, 'SSD Sata', '480GB', 'R (555MB/s) - W (540MB/s)', 'No', '-', '100 x 70 x 7 mm', 61),
(10, 'SSD Sata', '480GB', 'R (500MB/s) - W (450MB/s)', 'No', '-', '100 x 70 x 7 mm', 62),
(11, 'SSD Sata', '120GB', 'R (500MB/s) - W (350MB/s)', 'No', '-', '100 x 70 x 7 mm', 63),
(12, 'SSD NVMe', '500GB', 'R (3400MB/s) - W (2500MB/s)', 'No', '-', '2.5 x 80 x 22 mm', 64),
(13, 'SSD NVMe', '1TB', 'R (3470MB/s) - W (3000MB/s)', 'No', '-', '2.5 x 80 x 22 mm', 65),
(14, 'SSD Sata', '240GB', 'R (530MB/s) - W (400MB/s)', 'No', '-', '70 x 100 x 7 mm', 66),
(15, 'SSD Sata', '1TB', 'R (560MB/s) - W (530MB/s)', 'No', '-', '70 x 100 x 7 mm', 67),
(16, 'HDD Sata', '4TB', '5400 rpm', 'No', '6 ms', '100 x 20 x 147 mm', 68),
(17, 'HDD Sata', '1TB', '7200 RPM', 'No', '7 ms', '100 x 20 x 147 mm', 69),
(18, 'HDD Sata', '2TB', '7200 RPM', 'No', '6 ms', '100 x 20 x 147 mm', 70);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `monitors`
--

CREATE TABLE `monitors` (
  `id_monitor` int(4) NOT NULL,
  `resolucio` varchar(50) NOT NULL,
  `nits` varchar(50) NOT NULL,
  `material` varchar(50) NOT NULL,
  `dimensions` varchar(50) NOT NULL,
  `refresc` varchar(50) NOT NULL,
  `consum` varchar(50) NOT NULL,
  `pes` varchar(50) NOT NULL,
  `id_producte_fk` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `monitors`
--

INSERT INTO `monitors` (`id_monitor`, `resolucio`, `nits`, `material`, `dimensions`, `refresc`, `consum`, `pes`, `id_producte_fk`) VALUES
(1, 'FullHD', '250', 'LED IPS', '23.8\" (540 x 420 x 220 mm)', '75 Hz', '25 W', '3,5 kg', 71),
(2, 'QHD', '350', 'VA', '27\" (360 x 610 x 105 mm)', '165 Hz', '55 W', '7 kg', 72),
(3, 'FullHD', '250', 'LED IPS', '24\" (550 x 332 x 85 mm)', '75 Hz', '20 W', '3 kg', 73),
(4, 'FullHD', '250', 'LED IPS', '23.8\" (540 x 415 x 209 mm)', '60 Hz', '15 W', '3 kg', 74),
(5, 'WQHD', '350', 'VA E-LED', '34\" (808 x 360 x 125 mm)', '144 Hz', '60 W', '7 kg', 75),
(6, 'FullHD', '250', 'LED IPS', '23.8\" (540 x 422 x 219 mm)', '60 Hz', '25 W', '3,5 kg', 76),
(7, 'FullHD', '400', 'LED IPS', '24.5\" (540 x 302 x 87 mm)', '144 Hz', '22 W', '5 kg', 77),
(8, 'FullHD', '250', 'LED IPS', '23.8\" (540 x 391 x 211 mm)', '70 Hz', '15 W', '3 kg', 78),
(9, 'FullHD', '350', 'TN LED', '24\" (561 x 489 x 211 mm)', '165 Hz', '60 W', '5.2 kg', 79),
(10, 'FullHD', '350', 'VA LED', '23.6\" (537 x 320 x 70 mm)', '144 Hz', '25 W', '5.5 kg', 80),
(11, 'FullHD', '400', 'TN LED', '24.5\" (560 x 345 x 65 mm)', '144 Hz', '30 W', '4.6 kg', 81);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ordre`
--

CREATE TABLE `ordre` (
  `id_ordre` int(4) NOT NULL,
  `quantitat` int(4) NOT NULL,
  `preu` decimal(6,2) NOT NULL,
  `data` date NOT NULL,
  `id_usuari_fk` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `plaques_mare`
--

CREATE TABLE `plaques_mare` (
  `id_placa` int(4) NOT NULL,
  `tipus` varchar(50) NOT NULL,
  `socket` varchar(50) NOT NULL,
  `chipset` varchar(50) NOT NULL,
  `slot_SSD_NVMe` varchar(50) NOT NULL,
  `memoria` varchar(50) CHARACTER SET utf8 NOT NULL,
  `slots` varchar(50) NOT NULL,
  `ports_usb` varchar(100) NOT NULL,
  `lan` varchar(50) NOT NULL,
  `so` varchar(50) NOT NULL,
  `id_producte_fk` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `plaques_mare`
--

INSERT INTO `plaques_mare` (`id_placa`, `tipus`, `socket`, `chipset`, `slot_SSD_NVMe`, `memoria`, `slots`, `ports_usb`, `lan`, `so`, `id_producte_fk`) VALUES
(1, 'ATX', 'AM4', 'AMD X470', 'No', 'DDR4-SDRAM', '4', 'USB 3.2 Gen 1 x 4 + USB 2.0 x 2 + USB 3.2 Gen 2 x 2', 'RJ-45 x 1', '7.1 canals', 82),
(2, 'Micro-ATX', 'LGA1151', 'Intel B365', 'Si', 'DDR4-SDRAM', '4', 'USB 3.0 Gen 1 x 1 + USB 2.0 x 2', 'RJ-45 x 1', '7.1 canals', 83),
(3, 'ATX', 'LGA1200', 'Intel Z490', 'Si', 'DDR4', '4', 'USB 3.2 Gen 2 x 2 + USB 3.2 Gen 1 x 4 + USB 2.0 x 3', 'RJ-45 x 1', 'Audio 8-Channel(7.1)', 84),
(4, 'Micro-ATX', 'AM4', 'AMD B450', 'Si', 'DDR4-DIMM', '4', 'USB 3.1 Gen 1 x 2 + USB 2.0/1.1 x 8', 'RJ-45 x 1', '7.1 canals', 85),
(5, 'Micro-ATX', 'LGA1200', 'Intel H470', 'Si', 'DDR4', '2', 'USB 2.0 x 2 + USB 3.2 Gen1 x 4', 'RJ-45 x 1', '7.1 canals HD', 86),
(6, 'ATX', 'AM4', 'AMD X570', 'Si', 'DDR4-DIMM', '4', 'USB 2.0 x 4 + USB 3.2 Gen1 x 4 + USB 3.2 Gen 2 x 2', 'RJ-45 x 1', 'High Definition Audio, 5.1 canals', 87),
(7, 'ATX', 'LGA1200', 'Intel B460', 'Si', 'DDR4-DIMM', '4', 'USB 3.2 Gen 1 x 6 + USB 2.0 x 4', 'RJ-45 x 1', 'ealtek ALC S1200A 8-canals', 88),
(8, 'ATX', 'AM4', 'AMD B450', 'Si', 'DDR4-DIMM', '4', 'USB 3.1 Gen 1 x 4 + USB 2.0 x 8', 'RJ-45 x 1', 'Codec Realtek® ALC892', 89),
(9, 'Micro-ATX', 'AM4', 'AMD B550', 'Si', 'DDR4-DIMM', '4', 'USB 3.2 Gen 1 x 1 + USB 2.0 x 2 + ', 'RJ-45 x 1', 'Realtek ALC S1200A 7.1 Surround High Definition', 90),
(10, 'ATX', 'LGA1151', 'Intel Z390 ', 'Si', 'DDR4', '4', 'USB 3.1 Gen2 x 2 + USB 3.1 Gen1 x 5 + USB 2.0 x 4', 'RJ-45 x 1', '7.1 canals', 91),
(11, 'Micro-ATX', 'AM4', 'AMD B450', 'Si', 'DDR4-SDRAM', '2', 'USB 2.0 x 2 + USB 3.2 Gen 1 x 1 + USB 3.2 Gen 1 Tipo A x 4', 'RJ-45 x 1', '7.1 canals', 92),
(12, 'ATX', 'AM4', ' AMD B450', 'Si', 'DDR4-DIMM', '4', 'USB 3.2 Gen 2 x 2 + USB 3.2 Gen 1 x 4 + USB 2.0 x 6', 'RJ-45 x 1', 'Audio de 8 canals HD', 93),
(13, 'Micro-ATX', 'AM4', 'AMD B450', 'Si', 'DDR4-SDRAM', '2', 'USB 3.2 Gen 1 x 1 + USB 2.0 x 4', 'RJ-45 x 1', '7.1 canals HD', 94),
(14, 'ATX', 'AM4', 'AMD B550', 'Si', 'DDR4-DIMM', '4', 'USB 3.2 Gen 2 x 2 + USB 3.2 Gen 1 x 5 + USB 2.0 x 6 ', 'RJ-45 x 1', 'Audio de 8 canals HD', 95),
(15, 'ATX', 'AM4', 'AMD X570', 'Si', 'DDR4-SDRAM', '4', 'USB 3.1 Gen 2 x 1 + USB 3.2 Gen 1 x 1 + USB 2.0 x 2', 'RJ-45 x 1', '7.1 canals', 96);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `processadors`
--

CREATE TABLE `processadors` (
  `id_cpu` int(4) NOT NULL,
  `socket` varchar(50) NOT NULL,
  `nuclis` varchar(50) NOT NULL,
  `threads` varchar(50) NOT NULL,
  `velocitat` varchar(50) NOT NULL,
  `cache` varchar(50) NOT NULL,
  `cmos` varchar(50) NOT NULL,
  `versio_pci` varchar(50) NOT NULL,
  `consum` varchar(50) NOT NULL,
  `id_producte_fk` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `processadors`
--

INSERT INTO `processadors` (`id_cpu`, `socket`, `nuclis`, `threads`, `velocitat`, `cache`, `cmos`, `versio_pci`, `consum`, `id_producte_fk`) VALUES
(1, 'AM4', '6', '12', '3.6GHz', 'L2 (3MB) + L3 (32MB)', 'TSMC 7nm FinFET', 'PCIe 4.0 x16', '65W', 27),
(2, 'AM4', '8', '16', 'Fins a 4.7GHz', 'L2 (4MB) + L3 (32MB)', 'TSMC 7nm FinFET', 'PCIe 4.0', '105W', 28),
(3, 'LGA1200', '6', '12', 'Fins a 4.3GHz', '12 MB Intel® Smart Cache ', 'unknown', 'PCIe 3.0', '65 W', 29),
(4, 'AM4', '6', '12', 'Fins a 4.6GHz', 'L2 (3MB) + L3 (32MB)', 'TSMC 7nm FinFET', 'PCIe 4.0', '65 W', 30),
(5, 'LGA1200', '8', '16', 'Fins a 5.10 GHz', '16 MB Intel® Smart Cache ', 'unknown', 'PCIe 3.0', '125 W ', 31),
(6, 'LGA1200', '4', '8', 'Fins a 4.30 GHz', '6 MB Intel® Smart Cache ', 'unknown', 'PCIe 3.0', '65 W', 32),
(7, 'LGA1151', '8', '8', '3 GHz', '12 MB Intel® Smart Cache ', 'unknown', 'PCIe 3.0', '65 W', 33),
(8, 'AM4', '4', '4', '3,5 GHz', 'L3 (4MB)', '14nm FinFET', 'PCIe 3.0', '65 W', 34),
(9, 'LGA1151', '6', '6', 'Fins a 4,10 GHz', '9 MB Intel® Smart Cache ', 'unknown', 'PCIe 3.0', '65 W', 35),
(10, 'AM4', '8', '16', 'Fins a 4.4GHz', 'L2 (4MB) + L3 (32MB)', 'TSMC 7nm FinFET', 'PCIe 4.0 x16', '65W', 36),
(11, 'AM4', '6', '12', 'Fins a 4.4GHz', 'L2 (3MB) + L3 (32MB)', 'TSMC 7nm FinFET', 'PCIe 4.0 x16', '95W', 37),
(12, 'AM4', '6', '6', '3.6GHz', 'L2 (3MB) + L3 (32MB)', 'TSMC 7nm FinFET', 'PCIe 4.0 x16', '65W', 38),
(13, 'AM4', '12', '24', 'Fins a 4.6GHz', 'L2 (6MB) + L3 (64MB)', 'TSMC 7nm FinFET', 'PCIe 4.0 x16', '105W', 39),
(14, 'LGA1151', '8', '8', 'Fins a 4,7 GHz', '12 MB Intel® Smart Cache ', 'unknown', 'PCIe 3.0', '65 W', 40),
(15, 'LGA1200', '8', '16', 'Fins a 4,8 GHz', '16 MB Intel® Smart Cache ', 'unknown', 'PCIe 3.0', '65 W', 41),
(16, ' AM4', '6', '12', '3,6 GHz', 'L3 (16MB)', '12nm FinFET', 'PCIe 3.0 x16', '95 W', 42);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producte`
--

CREATE TABLE `producte` (
  `id_prod` int(4) NOT NULL,
  `nom` varchar(100) NOT NULL,
  `marca` varchar(100) NOT NULL,
  `preu` decimal(6,2) NOT NULL,
  `imatge` text NOT NULL,
  `stock` int(6) NOT NULL,
  `id_categoria_fk` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `producte`
--

INSERT INTO `producte` (`id_prod`, `nom`, `marca`, `preu`, `imatge`, `stock`, `id_categoria_fk`) VALUES
(1, 'Logitech G432', 'Logitech', '80.00', '/imatges/productes/auriculars/logi_g432.jpg', 5, 3),
(2, 'MSI Immerse GH50', 'MSI', '84.95', '/imatges/productes/auriculars/msi_gh50.jpg', 5, 3),
(3, 'Tempest GHS301 Barbarian', 'Tempest', '49.50', '/imatges/productes/auriculars/temp_ghs301.jpg', 5, 3),
(4, 'Newskill Drakain', 'Newskill', '40.00', '/imatges/productes/auriculars/news_drakain.jpg', 5, 3),
(5, 'HyperX Cloud Flight', 'HyperX', '159.99', '/imatges/productes/auriculars/hyp_flight.jpg', 5, 3),
(6, 'Razer Nari Essential', 'Razer', '85.00', '/imatges/productes/auriculars/raz_nari.jpg', 5, 3),
(7, 'Conceptronic Polona', 'Conceptronic', '14.50', '/imatges/productes/auriculars/con_polona.jpg', 5, 3),
(8, 'Equip 245301', 'Equip', '13.75', '/imatges/productes/auriculars/equi_245301.jpg', 5, 3),
(9, 'Logitech H340 Headset', 'Logitech', '60.00', '/imatges/productes/auriculars/logi_h340.jpg', 5, 3),
(10, 'Logitech H390', 'Logitech', '65.00', '/imatges/productes/auriculars/logi_h390.jpg', 5, 3),
(11, 'Corsair HS50 Pro Stereo', 'Corsair', '63.99', '/imatges/productes/auriculars/cors_hs50.jpg', 5, 3),
(12, 'Logitech G733', 'Logitech', '210.00', '/imatges/productes/auriculars/logi_g733.jpg', 5, 3),
(13, 'Razer BlackShark V2', 'Razer', '109.99', '/imatges/productes/auriculars/raz_blackshark.jpg', 5, 3),
(14, 'Trust HS-200', 'Trust', '19.96', '/imatges/productes/auriculars/trus_hs200.jpg', 5, 3),
(15, 'Tempest Spectra con Ventana', 'Tempest', '35.00', '/imatges/productes/caixes/temp_spectra.jpg', 5, 10),
(16, 'Nfortec Lynx Cristal Templado', 'Nfortec', '40.00', '/imatges/productes/caixes/nfor_lynx.jpg', 5, 10),
(17, 'Newskill Manticore', 'Newskill', '99.95', '/imatges/productes/caixes/news_manticore.jpg', 5, 10),
(18, 'Nfortec Draco V2', 'Nfortec', '68.00', '/imatges/productes/caixes/nfor_draco.jpg', 5, 10),
(19, 'Nfortec Krater', 'Nfortec', '79.94', '/imatges/productes/caixes/nfor_krater.jpg', 5, 10),
(20, 'Nox Hummer MC Pro', 'Nox', '46.98', '/imatges/productes/caixes/nox_hummer.jpg', 5, 10),
(21, 'MSI MAG Forge 100M', 'MSI', '48.00', '/imatges/productes/caixes/msi_mag.jpg', 5, 10),
(22, 'Nox Infinity Neon', 'Nox', '51.00', '/imatges/productes/caixes/nox_infinity.jpg', 5, 10),
(23, 'AeroCool Cylon', 'AeroCool', '43.80', '/imatges/productes/caixes/aero_cylon.jpg', 5, 10),
(24, 'Corsair Carbide Spec-Delta', 'Corsair', '80.54', '/imatges/productes/caixes/cors_carbide.jpg', 5, 10),
(25, 'Tacens Mars Gaming MCL', 'Tacens', '41.99', '/imatges/productes/caixes/tacen_mars.jpg', 5, 10),
(26, 'BitFenix Nova Mesh SE TG', 'BitFenix', '66.00', '/imatges/productes/caixes/bitfe_nova.jpg', 5, 10),
(27, 'AMD Ryzen 5 3600', 'AMD', '194.50', '/imatges/productes/cpus/ryz_3600.jpg', 5, 1),
(28, 'AMD Ryzen 7 5800X', 'AMD', '455.00', '/imatges/productes/cpus/ryz_5800x.jpg', 5, 1),
(29, 'Intel Core i5-10400', 'Intel', '160.00', '/imatges/productes/cpus/intel_10400.jpg', 5, 1),
(30, 'AMD Ryzen 5 5600X', 'AMD', '360.99', '/imatges/productes/cpus/ryz_5600x.jpg', 5, 1),
(31, 'Intel Core i7-10700K', 'Intel', '310.00', '/imatges/productes/cpus/intel_10700k.jpg', 5, 1),
(32, 'Intel Core i3-10100', 'Intel', '122.90', '/imatges/productes/cpus/intel_10100.jpg', 5, 1),
(33, 'Intel Core i7-9700F', 'Intel', '221.94', '/imatges/productes/cpus/intel_9700f.jpg', 5, 1),
(34, 'AMD Ryzen 3 2200G', 'AMD', '135.00', '/imatges/productes/cpus/ryz_2200g.jpg', 5, 1),
(35, 'Intel Core i5-9400F', 'Intel', '120.00', '/imatges/productes/cpus/intel_9400f.jpg', 5, 1),
(36, 'AMD Ryzen 7 3700X', 'AMD', '310.50', '/imatges/productes/cpus/ryz_3700x.jpg', 5, 1),
(37, 'AMD Ryzen 5 3600X', 'AMD', '260.00', '/imatges/productes/cpus/ryz_3600x.jpg', 5, 1),
(38, 'AMD Ryzen 5 3500X', 'AMD', '178.99', '/imatges/productes/cpus/ryz_3500x.jpg', 5, 1),
(39, 'AMD Ryzen 9 3900X', 'AMD', '450.00', '/imatges/productes/cpus/ryz_3900x.jpg', 5, 1),
(40, 'Intel Core i7-9700', 'Intel', '233.90', '/imatges/productes/cpus/intel_9700.jpg', 5, 1),
(41, 'Intel Core i7-10700F', 'Intel', '260.50', '/imatges/productes/cpus/intel_10700f.jpg', 5, 1),
(42, 'AMD Ryzen 5 2600X', 'AMD', '174.91', '/imatges/productes/cpus/ryz_2600x.jpg', 5, 1),
(43, 'MSI MPG A650GF', 'MSI', '122.99', '/imatges/productes/alimentacio/msi_a650.jpg', 5, 8),
(44, 'Tempest Gaming GPSU', 'Tempest', '40.00', '/imatges/productes/alimentacio/temp_gpsu.jpg', 5, 8),
(45, 'Owlotech BPS', 'Owlotech', '23.50', '/imatges/productes/alimentacio/owlo_bps.jpg', 5, 8),
(46, 'Nfortec Sagitta', 'Nfortec', '95.00', '/imatges/productes/alimentacio/nfor_sagitta.jpg', 5, 8),
(47, 'Corsair CV Series CV650', 'Corsair', '69.00', '/imatges/productes/alimentacio/cors_cv650.jpg', 5, 8),
(48, 'Corsair CV550 CV Series', 'Corsair', '48.50', '/imatges/productes/alimentacio/cors_cv550.jpg', 5, 8),
(49, 'Seasonic Core GC 500', 'Seasonic', '55.94', '/imatges/productes/alimentacio/sea_gc500.jpg', 5, 8),
(50, 'Nox Urano VX', 'Nox', '57.00', '/imatges/productes/alimentacio/nox_urano.jpg', 5, 8),
(51, 'Thermaltake Smart', 'Thermaltake', '74.00', '/imatges/productes/alimentacio/therm_smart.jpg', 5, 8),
(52, 'Thermaltake TR2 S', 'Thermaltake', '56.00', '/imatges/productes/alimentacio/therm_tr2.jpg', 5, 8),
(53, 'Kingston HyperX Fury', 'Kingston', '49.00', '/imatges/productes/memories/kings_fury.jpg', 5, 5),
(54, 'Kingston HyperX Fury Black', 'Kingston', '97.50', '/imatges/productes/memories/kings_furybl.jpg', 5, 5),
(55, 'Kingston HyperX Fury Black 2', 'Kingston', '49.00', '/imatges/productes/memories/kings_furybl2.jpg', 5, 5),
(56, 'Corsair Vengeance LPX', 'Corsair', '94.99', '/imatges/productes/memories/cors_lpx.jpg', 5, 5),
(57, 'Corsair Vengeance Pro', 'Corsair', '93.99', '/imatges/productes/memories/cors_pro.jpg', 5, 5),
(58, 'Kingston HyperX Fury RGB', 'Kingston', '204.00', '/imatges/productes/memories/kings_fury.jpg', 5, 5),
(59, 'G.Skill Trident Z', 'G.Skill', '105.99', '/imatges/productes/memories/skill_trident.jpg', 5, 5),
(60, 'Nfortec Alcyon X', 'Nfortec', '75.60', '/imatges/productes/memories/nfor_alcyon.jpg', 5, 5),
(61, 'Kioxia EXCERIA', 'Kioxia', '54.99', '/imatges/productes/memories/kiox_exceria.jpg', 5, 5),
(62, 'Kingston A400', 'Kingston', '55.00', '/imatges/productes/memories/kings_a400.jpg', 5, 5),
(63, 'Kingston A400 2', 'Kingston', '24.00', '/imatges/productes/memories/kings_a4002.jpg', 5, 5),
(64, 'Kioxia EXCERIA PLUS', 'Kioxia', '87.00', '/imatges/productes/memories/kiox_exceriapl.jpg', 5, 5),
(65, 'Western Digital Black SN750', 'Western Digital', '143.50', '/imatges/productes/memories/west_sn750.jpg', 5, 5),
(66, 'SanDisk Plus', 'SanDisk', '41.00', '/imatges/productes/memories/sand_plus.jpg', 5, 5),
(67, 'Samsung 870 EVO', 'Samsung', '114.00', '/imatges/productes/memories/sams_870.jpg', 5, 5),
(68, 'Seagate BarraCuda 3.5', 'Seagate', '0.00', '/imatges/productes/memories/sea_35.jpg', 5, 5),
(69, 'Seagate BarraCuda 3.5 2', 'Seagate', '34.00', '/imatges/productes/memories/sea_352.jpg', 5, 5),
(70, 'Seagate BarraCuda 3.5 3', 'Seagate', '48.99', '/imatges/productes/memories/sea_353.jpg', 5, 5),
(71, 'MSI Optix G241V', 'MSI', '139.99', '/imatges/productes/monitors/msi_g241v.jpg', 5, 6),
(72, 'Newskill Icarus', 'Newskill', '284.00', '/imatges/productes/monitors/news_icarus.jpg', 5, 6),
(73, 'LG 24MP59G-P', 'LG', '129.50', '/imatges/productes/monitors/lg_24mp5.jpg', 5, 6),
(74, 'Philips 243V7QDSB', 'Philips', '125.00', '/imatges/productes/monitors/phil_243v7.jpg', 5, 6),
(75, 'Newskill Icarus IC34W4-V', 'Newskill', '449.94', '/imatges/productes/monitors/news_ic34.jpg', 5, 6),
(76, 'MSI Optix G241', 'MSI', '250.00', '/imatges/productes/monitors/msi_g241.jpg', 5, 6),
(77, 'BenQ EX2510 MOBIUZ', 'BenQ', '230.00', '/imatges/productes/monitors/ben_ex2510.jpg', 5, 6),
(78, 'Asus VZ249HE-W', 'Asus', '129.00', '/imatges/productes/monitors/asu_vz249.jpg', 5, 6),
(79, 'Asus VG248QG', 'Asus', '248.99', '/imatges/productes/monitors/asu_vg24.jpg', 5, 6),
(80, 'Asus TUF Gaming VG24VQ', 'Asus', '220.00', '/imatges/productes/monitors/asu_tuf.jpg', 5, 6),
(81, 'HP Omen 25', 'HP', '267.00', '/imatges/productes/monitors/hp_omen25.jpg', 5, 6),
(82, 'MSI X470 Plus Max', 'MSI', '95.00', '/imatges/productes/plaques/msi_x470.jpg', 5, 2),
(83, 'Gigabyte B365M-DS3H', 'Gigabyte', '75.99', '/imatges/productes/plaques/giga_b365.jpg', 5, 2),
(84, 'MSI Z490-A PRO', 'MSI', '160.00', '/imatges/productes/plaques/msi_z490.jpg', 5, 2),
(85, 'Gigabyte B450M DS3H V2', 'Gigabyte', '71.99', '/imatges/productes/plaques/giga_b450.jpg', 5, 2),
(86, 'AsRock H470M-HDV/M.2', 'Asrock', '70.00', '/imatges/productes/plaques/asr_h470.jpg', 5, 2),
(87, 'Gigabyte X570 Aorus Elite', 'Gigabyte', '200.00', '/imatges/productes/plaques/giga_x570.jpg', 5, 2),
(88, 'Asus TUF GAMING B460-PLUS', 'Asus', '114.95', '/imatges/productes/plaques/asu_b460.jpg', 5, 2),
(89, 'Gigabyte B450 Aorus Elite V2', 'Gigabyte', '97.00', '/imatges/productes/plaques/giga_b450eli.jpg', 5, 2),
(90, 'Asus TUF GAMING B550-PLUS', 'Asus', '149.50', '/imatges/productes/plaques/asu_b550.jpg', 5, 2),
(91, 'MSI Z390-A PRO', 'MSI', '121.00', '/imatges/productes/plaques/msi_z390.jpg', 5, 2),
(92, 'Gigabyte B450M S2H V2', 'Gigabyte', '49.99', '/imatges/productes/plaques/giga_b450m.jpg', 5, 2),
(93, 'MSI B450 Gaming Plus MAX', 'MSI', '100.00', '/imatges/productes/plaques/msi_b450.jpg', 5, 2),
(94, 'MSI B450M-A PRO MAX', 'MSI', '57.00', '/imatges/productes/plaques/msi_b450ma.jpg', 5, 2),
(95, 'MSI MAG B550 TOMAHAWK ', 'MSI', '174.94', '/imatges/productes/plaques/msi_b550.jpg', 5, 2),
(96, 'Asus ROG Strix X570-E', 'Asus', '310.00', '/imatges/productes/plaques/asu_x570.jpg', 5, 2),
(97, 'MSI Clutch GM20 Elite', 'MSI', '30.00', '/imatges/productes/ratolins/msi_clutch.jpg', 5, 7),
(98, 'Newskill Habrok', 'Newskill', '49.95', '/imatges/productes/ratolins/news_habrok.jpg', 5, 7),
(99, 'Logitech G203 Lightsync 2nd Gen', 'Logitech', '28.00', '/imatges/productes/ratolins/log_g203.jpg', 5, 7),
(100, 'Logitech G502 Hero', 'Logitech', '92.98', '/imatges/productes/ratolins/log_g502.jpg', 5, 7),
(101, 'Logitech G402 Hyperion Fury', 'Logitech', '55.00', '/imatges/productes/ratolins/log_g402.jpg', 5, 7),
(102, 'Razer DeathAdder V2', 'Razer', '79.95', '/imatges/productes/ratolins/raz_death.jpg', 5, 7),
(103, 'Razer Naga Trinity', 'Razer', '110.00', '/imatges/productes/ratolins/raz_naga.jpg', 5, 7),
(104, 'Razer Viper Ultimate', 'Razer', '162.00', '/imatges/productes/ratolins/raz_viper.jpg', 5, 7),
(105, 'Razer Mamba Elite', 'Razer', '89.99', '/imatges/productes/ratolins/raz_mamba.jpg', 5, 7),
(106, 'Asus ROG Chakram', 'Asus', '140.00', '/imatges/productes/ratolins/asu_chakram.jpg', 5, 7),
(107, 'Newskill EOS', 'Newskill', '49.95', '/imatges/productes/ratolins/news_eos.jpg', 5, 7),
(108, 'HP X200', 'HP', '18.00', '/imatges/productes/ratolins/hp_x200.jpg', 5, 7),
(109, 'Logitech Pebble M350', 'Logitech', '18.98', '/imatges/productes/ratolins/log_pebble.jpg', 5, 7),
(110, 'HP X1500', 'HP', '10.00', '/imatges/productes/ratolins/hp_x1500.jpg', 5, 7),
(111, 'Nfortec Hydrus', 'Nfortec', '55.00', '/imatges/productes/refrigeracio/nfor_hydrus.jpg', 5, 9),
(112, 'MSI MAG CoreLiquid', 'MSI', '120.00', '/imatges/productes/refrigeracio/msi_mag.jpg', 5, 9),
(113, 'Cooler Master MasterLiquid ML360R', 'Cooler Master', '118.70', '/imatges/productes/refrigeracio/cool_ml360.jpg', 5, 9),
(114, 'Corsair iCUE H150i ELITE CAPELLIX', 'Corsair', '188.00', '/imatges/productes/refrigeracio/cors_h150i.jpg', 5, 9),
(115, 'Nox Hummer H-240', 'Nox', '85.50', '/imatges/productes/refrigeracio/nox_h240.jpg', 5, 9),
(116, 'Cooler Master MasterLiquid Lite 240', 'Cooler Master', '88.50', '/imatges/productes/refrigeracio/cool_240.jpg', 5, 9),
(117, 'Tempest Subzero', 'Tempest', '35.00', '/imatges/productes/refrigeracio/temp_subzero.jpg', 5, 9),
(118, 'Nfortec Sculptor', 'Nfortec', '49.95', '/imatges/productes/refrigeracio/nfor_sculptor.jpg', 5, 9),
(119, 'Cooler Master Hyper 212', 'Cooler Master', '65.00', '/imatges/productes/refrigeracio/cool_212.jpg', 5, 9),
(120, 'Cooler Master Hyper 212 Turbo', 'Cooler Master', '42.00', '/imatges/productes/refrigeracio/cool_212turb.jpg', 5, 9),
(121, 'Noctua NH-D15', 'Noctua', '120.00', '/imatges/productes/refrigeracio/noct_d15.jpg', 5, 9),
(122, 'Cooler Master MA610P', 'Cooler Master', '57.00', '/imatges/productes/refrigeracio/cool_ma610p.jpg', 5, 9),
(123, 'Arctic Freezer 34 eSports', 'Arctic', '45.25', '/imatges/productes/refrigeracio/arct_free34.jpg', 5, 9),
(124, 'Nfortec Oberon Combo', 'Nfortec', '40.00', '/imatges/productes/refrigeracio/nfor_oberon.jpg', 5, 9),
(125, 'Corsair LL120', 'Corsair', '100.00', '/imatges/productes/refrigeracio/cors_ll120.jpg', 5, 9),
(126, 'AeroCool Cosmo', 'AeroCool', '6.00', '/imatges/productes/refrigeracio/aero_cosmo.jpg', 5, 9),
(127, 'Corsair ML120 PRO', 'Corsair', '75.00', '/imatges/productes/refrigeracio/cors_ml120pro.jpg', 5, 9),
(128, 'Gigabyte Aorus GTX 1080 Ti', 'Gigabyte', '675.00', '/imatges/productes/grafiques/giga_1080.jpg', 5, 4),
(129, 'Gigabyte GeForce GTX 1070 G1', 'Gigabyte', '359.99', '/imatges/productes/grafiques/giga_1070.jpg', 5, 4),
(130, 'Asus ROG Strix GeForce RTX 2070 Super', 'Asus', '535.00', '/imatges/productes/grafiques/asu_2070.jpg', 5, 4),
(131, 'Gigabyte GeForce RTX 2060', 'Gigabyte', '500.00', '/imatges/productes/grafiques/giga_2060.jpg', 5, 4),
(132, 'Gigabyte GeForce GTX 1660 SUPER', 'Gigabyte', '209.45', '/imatges/productes/grafiques/giga_1660.jpg', 5, 4),
(133, 'KFA2 GeForce RTX 2060', 'KFA2', '810.00', '/imatges/productes/grafiques/kfa_2060.jpg', 5, 4),
(134, 'AsRock Phantom Radeon RX550', 'Asrock', '159.00', '/imatges/productes/grafiques/asr_rx550.jpg', 5, 4),
(135, 'MSI AMD Radeon 6900 XT', 'MSI', '1649.90', '/imatges/productes/grafiques/msi_6900.jpg', 5, 4),
(136, 'Gigabyte GeForce RTX 2080 SUPER', 'Gigabyte', '680.00', '/imatges/productes/grafiques/giga_2080.jpg', 5, 4),
(137, 'Msi Rtx 3090 Gaming X', 'MSI', '1560.00', '/imatges/productes/grafiques/msi_3090.jpg', 5, 4),
(138, 'Gigabyte RTX 3070', 'Gigabyte', '650.00', '/imatges/productes/grafiques/giga_3070.jpg', 5, 4),
(139, 'Gigabyte GeForce RTX 3060', 'Gigabyte', '400.99', '/imatges/productes/grafiques/giga_3060.jpg', 5, 4),
(140, 'MSI Vigor GK50', 'MSI', '120.00', '/imatges/productes/teclats/msi_vigor.jpg', 5, 11),
(141, 'MSI Vigor GK50 Elite', 'MSI', '84.99', '/imatges/productes/teclats/msi_gk50.jpg', 5, 11),
(142, 'Logitech Desktop MK120', 'Logitech', '18.00', '/imatges/productes/teclats/logi_mk120.jpg', 5, 11),
(143, 'Razer Huntsman Tournament Edition', 'Razer', '140.94', '/imatges/productes/teclats/raz_huntsman.jpg', 5, 11),
(144, 'Newskill Serike', 'Newskill', '59.96', '/imatges/productes/teclats/news_serike.jpg', 5, 11),
(145, 'Newskill Serike TKL', 'Newskill', '50.00', '/imatges/productes/teclats/news_tkl.jpg', 5, 11),
(146, 'Natec Barracuda Slim', 'Natec', '22.50', '/imatges/productes/teclats/nat_barrac.jpg', 5, 11),
(147, 'HyperX Alloy Core', 'HyperX', '59.99', '/imatges/productes/teclats/hype_alloy.jpg', 5, 11),
(148, 'Logitech Wireless K270', 'Logitech', '28.00', '/imatges/productes/teclats/log_k270.jpg', 5, 11),
(149, 'Genesis Thor 300', 'Genesis', '60.50', '/imatges/productes/teclats/gene_thor.jpg', 5, 11),
(150, 'Microsoft Wired 600', 'Microsoft', '16.00', '/imatges/productes/teclats/mic_600.jpg', 5, 11);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ratolins`
--

CREATE TABLE `ratolins` (
  `id_ratoli` int(4) NOT NULL,
  `tipus` varchar(50) NOT NULL,
  `botons` varchar(50) NOT NULL,
  `connexio` varchar(50) NOT NULL,
  `retroiluminacio` varchar(50) NOT NULL,
  `sensor` varchar(50) NOT NULL,
  `resposta` varchar(50) NOT NULL,
  `dimensions` varchar(50) NOT NULL,
  `pes` varchar(50) NOT NULL,
  `id_producte_fk` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `ratolins`
--

INSERT INTO `ratolins` (`id_ratoli`, `tipus`, `botons`, `connexio`, `retroiluminacio`, `sensor`, `resposta`, `dimensions`, `pes`, `id_producte_fk`) VALUES
(1, 'Gaming', '6 laterals i 4 superiors', 'Cable USB', 'Mystic Light RGB', 'PixArt PAW 3309 (max 6400 DPI)', '1000 Hz', '130 x 83 x 41 mm', '98 g', 97),
(2, 'Gaming', '3 laterals i 4 superiors', 'Cable USB', 'RGB LIGHT', 'PIXART 3360 (max 16000 DPI)', '1800 Hz', '122 x 66 x 39 mm', '135 g', 98),
(3, 'Gaming', '2 laterals i 4 superiors', 'Cable USB', 'RGB LIGHTSYNC', 'Logitech G HUB (max 8000 DPI)', '1000 Hz', '116 x 62 x 38 mm', '85 g', 99),
(4, 'Gaming', '4 laterals i i 4 superiors', 'Cable USB', 'RGB LIGHTSYNC', 'HERO (max 16000 DPI)', '1000 Hz', '132 x 75 x 40 mm', '121 g', 100),
(5, 'Gaming', '5 laterals i i 4 superiors', 'Cable USB', 'No', 'Motor Fusion (max 4000 DPI)', '1000 Hz', '136 x 72 x 41 mm', '144 g', 101),
(6, 'Gaming', '2 laterals i i 4 superiors', 'Cable USB', 'Razer Chroma RGB', ' Focus+ (max 20000 DPI)', '1500 Hz', '127 x 61 x 42 mm', '82 g', 102),
(7, 'Gaming', '19 botons programables', 'Cable USB', 'No', '5G (max 16000 DPI)', '1000 Hz', '119 x 74 x 43 mm', '120 g', 103),
(8, 'Gaming', '2 laterals i i 4 superior', 'USB inalambric', 'No', ' Focus+ (max 20000 DPI)', '2000Hz', '127 x 66 x 38 mm', '74 g', 104),
(9, 'Gaming', '2 laterals i i 4 superior', 'Cable USB', 'Razer Chroma RGB', '5G (max 16000 DPI)', '1000 Hz', '125 x 70 x 43 mm', '96 g', 105),
(10, 'Gaming', '6 laterals i i 4 superior', 'USB inalambric', 'RGB ASUS Aura Sync', 'max 16000 DPI', '1000 Hz', '132 x 76 x 43 mm', '122 g', 106),
(11, 'Gaming', '2 laterals i 4 superiors', 'Cable USB', 'RGB', 'PMW3360 (max 16000 DPI)', '1000 Hz', '124 x 65 x 40 mm', '135 g', 107),
(12, 'Bàsic', '3 superiors', 'USB inalambric', 'No', 'max 1600 DPI', '500 Hz', '63 x 36 x 15 mm', '60 g', 108),
(13, 'Bàsic', '3 superiors', 'USB inalàmbric', 'No', '1000 DPI', '500 Hz', '107 x 59 x 26', '100 g', 109),
(14, 'Bàsic', '3 superiors', 'Cable USB', 'No', '900 DPI', '500 Hz', '119 x 69 x 38 mm', '130 g', 110);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `refrigeracio`
--

CREATE TABLE `refrigeracio` (
  `id_refrigeracio` int(4) NOT NULL,
  `tipus` varchar(50) NOT NULL,
  `material` varchar(50) NOT NULL,
  `soroll` varchar(50) NOT NULL,
  `compatibilitat` varchar(50) NOT NULL,
  `retroiluminacio` varchar(50) NOT NULL,
  `consum` varchar(50) NOT NULL,
  `ventiladors` varchar(50) NOT NULL,
  `velocitat` varchar(50) NOT NULL,
  `id_producte_fk` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `refrigeracio`
--

INSERT INTO `refrigeracio` (`id_refrigeracio`, `tipus`, `material`, `soroll`, `compatibilitat`, `retroiluminacio`, `consum`, `ventiladors`, `velocitat`, `id_producte_fk`) VALUES
(1, 'Líquida', 'Alumini', '32.0 dB', '120 x 120 x 25 mm', 'RGB', '3.5W', '1', '2600 RPM', 111),
(2, 'Líquida', 'Alumini', '14.3 - 34.3 dB', '120 x 120 x 25 mm', 'ARGB', '1.8 W', '1', '500 - 2000 RPM', 112),
(3, 'Líquida', 'Alumini', '30.0 dB', '120 x 120 x 25 mm', 'RGB', '4 W', '3', '650 - 2000 RPM', 113),
(4, 'Líquida', 'Cobre', '10 - 37 dB', '120 x 40 x 25', 'LED CAPELLIX RGB', '3.8 W', '3', '2400 RPM', 114),
(5, 'Líquida', 'Cobre, Alumini', '18.0 - 26.4 dB', '120 x 36 x 25 mm', 'ARGB', '4 W', '2', '700 - 1500 RPM', 115),
(6, 'Liquida', 'Alumini', '6.0 - 30.0 dB', '120 x 25 x 12 mm', 'No', '3.2 W', '2', '650 - 2000 RPM', 116),
(7, 'CPU', 'Alumini', '20 dB', '128 x 77 x 157 mm', 'RGB', '4.2 W', '1', '800 - 1650 RPM', 117),
(8, 'CPU', 'Alumini, Cobre, Niquel', '27 dB', '154 x 123 x 106 mm', 'ARGB', '2.5 W', '2', '700 - 1800 RPM', 118),
(9, 'CPU', 'Alumini', '8 - 30 dBA', '158.8 x 120 x 79.6 mm', 'RGB', '1.92 W', '1', '650 - 2.000 RPM', 119),
(10, 'CPU', 'Alumini', '9 - 31 dB', '163 x 120 x 108 mm', 'No', '2.28 W', '2', '600 - 1600 RPM', 120),
(11, 'CPU', 'Alumini, Cobre', '24.6 dB', '161 x 160 x 150 mm', 'No', '2.3 W', '2', '300 - 1500 RPM', 121),
(12, 'CPU', 'Alumini', '35 dB max', '130.9 x 112.8 x 166.5 mm', 'LED RGB', '4.44 W', '2', '600 - 1.800 RPM', 122),
(13, 'CPU', 'Alumini', '28 dB', '157 x 124 x 88 mm', 'No', '2.6 W', '1', '200 - 2100 RPM', 123),
(14, 'Suplementaris', 'Alumini', '25 dB', '120 x 120 x 24 mm', 'RGB', '2.3 W', '3', '1500 RPM', 124),
(15, 'Suplementaris', 'Alumini', '24,8 dB', '120 x 120 x 25 mm', 'RGB', '3.2 W', '3', '600 - 1500 RPM', 125),
(16, 'Suplementaris', 'Alumini', '24,8 dB', '120 x 120 x 25 mm', 'RGB', '3.2 W', '1', '600 - 1500 RPM', 126),
(17, 'Suplementaris', 'Alumini', '25 dB', '120 x 120 x 25 mm', 'RGB', '3.7 W', '3', '400 - 1600 RPM', 127);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `subscripcio`
--

CREATE TABLE `subscripcio` (
  `id_subs` int(4) NOT NULL,
  `email` varchar(100) NOT NULL,
  `data` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `super_categoria`
--

CREATE TABLE `super_categoria` (
  `id_supercat` int(4) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `descripcio` varchar(200) NOT NULL,
  `imatge` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `super_categoria`
--

INSERT INTO `super_categoria` (`id_supercat`, `nom`, `descripcio`, `imatge`) VALUES
(1, 'COMPONENTS', 'Aquí trobaràs tots els components que componen els PC de sobre taula per a que puguis renovar o actualitzar el teu ordinador. El nostre catàleg s\'actualitza dia a dia!', '/imatges/categories/components.jpg'),
(2, 'PERIFERICS', 'Com no pot ser d\'una altre manera, un bon ordinador no és res sense tots els perifèrics que el complementen. Des del monitor al teclat, passant pel ratolí o altaveus i micròfons.', '/imatges/categories/periferics.jpg'),
(3, 'CREADOR', 'Aquí trobaràs l\'eina perfecta perquè triïs una a una les peces del teu ordinador i provis diferents configuracions i pressupostos de forma ràpida, resumida i intuïtiva.', '/imatges/categories/configuracio.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `teclats`
--

CREATE TABLE `teclats` (
  `id_teclat` int(4) NOT NULL,
  `tipus` varchar(50) NOT NULL,
  `switches` varchar(100) NOT NULL,
  `retroiluminacio` varchar(50) NOT NULL,
  `connexio` varchar(50) NOT NULL,
  `dimensions` varchar(50) NOT NULL,
  `id_producte_fk` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `teclats`
--

INSERT INTO `teclats` (`id_teclat`, `tipus`, `switches`, `retroiluminacio`, `connexio`, `dimensions`, `id_producte_fk`) VALUES
(1, 'Gaming', 'Kailh Low Profile', 'RGB', 'USB 2.0', '435 x 141 x 34 mm', 140),
(2, 'Gaming', 'Kailh Box White', 'RGB', 'USB 2.0', '435 x 135 x 38 mm', 141),
(3, 'Bàsic', 'unknown', 'No', 'USB', '450 x 155 x 25 mm', 142),
(4, 'Gaming', 'Óptics lineals Razer', 'Razer Chroma RGB', 'USB', '380 x 150 x 35 mm', 143),
(5, 'Gaming', 'Mecànics', 'RGB', 'USB', '456 x 218 x 40 mm', 144),
(6, 'Gaming', 'Blue mechanic', 'RGB', 'USB', '356 x 141 x 30 mm', 145),
(7, 'Bàsic', 'unknown', 'No', 'USB tipo A', '447 x 167 x 22 mm', 146),
(8, 'Gaming', 'Membrane cherry MX', 'RGB', 'USB 2.0', '443 x 175 x 35 mm', 147),
(9, 'Bàsic', 'unknown', 'No', 'wireless usb', '315 x 150 x 15 mm', 148),
(10, 'Gaming', 'Mechanic brown', 'RGB', 'USB 2.0', '442 x 136 x 36 mm', 149),
(11, 'Bàsic', 'Silenciosos', 'No', 'USB', '450 x 160 x 42 mm', 150);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuari`
--

CREATE TABLE `usuari` (
  `id_usuari` int(4) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `contrassenya` varchar(200) NOT NULL,
  `direccio` varchar(100) NOT NULL,
  `detall_dir` varchar(100) NOT NULL,
  `pais` varchar(50) NOT NULL,
  `provincia` varchar(50) NOT NULL,
  `ciutat` varchar(50) NOT NULL,
  `codi_postal` varchar(5) NOT NULL,
  `telefon` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `valoracio`
--

CREATE TABLE `valoracio` (
  `id_val` int(4) NOT NULL,
  `puntuacio` decimal(4,2) NOT NULL,
  `comentari` varchar(400) NOT NULL,
  `data` varchar(50) NOT NULL,
  `id_producte_fk` int(4) NOT NULL,
  `id_usuari_fk` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wishlist`
--

CREATE TABLE `wishlist` (
  `id_wish` int(4) NOT NULL,
  `id_producte_fk` int(4) NOT NULL,
  `id_usuari_fk` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `auriculars`
--
ALTER TABLE `auriculars`
  ADD PRIMARY KEY (`id_auriculars`),
  ADD KEY `producte_auriculars` (`id_producte_fk`);

--
-- Indices de la tabla `caixes`
--
ALTER TABLE `caixes`
  ADD PRIMARY KEY (`id_caixa`),
  ADD KEY `producte_caixa` (`id_producte_fk`);

--
-- Indices de la tabla `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`id_cat`),
  ADD KEY `cat_supercat` (`id_supercat_fk`);

--
-- Indices de la tabla `dades_facturacio`
--
ALTER TABLE `dades_facturacio`
  ADD PRIMARY KEY (`id_facturacio`),
  ADD KEY `ordre_dades_factura` (`id_ordre_fk`);

--
-- Indices de la tabla `fonts_alimentacio`
--
ALTER TABLE `fonts_alimentacio`
  ADD PRIMARY KEY (`id_font`),
  ADD KEY `producte_font` (`id_producte_fk`);

--
-- Indices de la tabla `grafiques`
--
ALTER TABLE `grafiques`
  ADD PRIMARY KEY (`id_grafica`),
  ADD KEY `producte_tarjeta` (`id_producte_fk`);

--
-- Indices de la tabla `linia_ordre`
--
ALTER TABLE `linia_ordre`
  ADD PRIMARY KEY (`id_linia`),
  ADD KEY `ordre_linia_ordre` (`id_ordre_fk`),
  ADD KEY `producte_linia_ordre` (`id_producte_fk`);

--
-- Indices de la tabla `memories`
--
ALTER TABLE `memories`
  ADD PRIMARY KEY (`id_memoria`),
  ADD KEY `producte_memoria` (`id_producte_fk`);

--
-- Indices de la tabla `monitors`
--
ALTER TABLE `monitors`
  ADD PRIMARY KEY (`id_monitor`),
  ADD KEY `producte_monitor` (`id_producte_fk`);

--
-- Indices de la tabla `ordre`
--
ALTER TABLE `ordre`
  ADD PRIMARY KEY (`id_ordre`),
  ADD KEY `usuari_ordre` (`id_usuari_fk`);

--
-- Indices de la tabla `plaques_mare`
--
ALTER TABLE `plaques_mare`
  ADD PRIMARY KEY (`id_placa`),
  ADD KEY `producte_placa` (`id_producte_fk`);

--
-- Indices de la tabla `processadors`
--
ALTER TABLE `processadors`
  ADD PRIMARY KEY (`id_cpu`),
  ADD KEY `producte_cpu` (`id_producte_fk`);

--
-- Indices de la tabla `producte`
--
ALTER TABLE `producte`
  ADD PRIMARY KEY (`id_prod`),
  ADD KEY `prod_cat` (`id_categoria_fk`);

--
-- Indices de la tabla `ratolins`
--
ALTER TABLE `ratolins`
  ADD PRIMARY KEY (`id_ratoli`),
  ADD KEY `producte_ratoli` (`id_producte_fk`);

--
-- Indices de la tabla `refrigeracio`
--
ALTER TABLE `refrigeracio`
  ADD PRIMARY KEY (`id_refrigeracio`),
  ADD KEY `produte_refrigeracio` (`id_producte_fk`);

--
-- Indices de la tabla `subscripcio`
--
ALTER TABLE `subscripcio`
  ADD PRIMARY KEY (`id_subs`);

--
-- Indices de la tabla `super_categoria`
--
ALTER TABLE `super_categoria`
  ADD PRIMARY KEY (`id_supercat`);

--
-- Indices de la tabla `teclats`
--
ALTER TABLE `teclats`
  ADD PRIMARY KEY (`id_teclat`),
  ADD KEY `producte_teclat` (`id_producte_fk`);

--
-- Indices de la tabla `usuari`
--
ALTER TABLE `usuari`
  ADD PRIMARY KEY (`id_usuari`);

--
-- Indices de la tabla `valoracio`
--
ALTER TABLE `valoracio`
  ADD PRIMARY KEY (`id_val`),
  ADD KEY `val_prod` (`id_producte_fk`),
  ADD KEY `val_usu` (`id_usuari_fk`);

--
-- Indices de la tabla `wishlist`
--
ALTER TABLE `wishlist`
  ADD PRIMARY KEY (`id_wish`),
  ADD KEY `prod_wish` (`id_producte_fk`),
  ADD KEY `usu_wish` (`id_usuari_fk`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `auriculars`
--
ALTER TABLE `auriculars`
  MODIFY `id_auriculars` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `caixes`
--
ALTER TABLE `caixes`
  MODIFY `id_caixa` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `categoria`
--
ALTER TABLE `categoria`
  MODIFY `id_cat` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `dades_facturacio`
--
ALTER TABLE `dades_facturacio`
  MODIFY `id_facturacio` int(4) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `fonts_alimentacio`
--
ALTER TABLE `fonts_alimentacio`
  MODIFY `id_font` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `grafiques`
--
ALTER TABLE `grafiques`
  MODIFY `id_grafica` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `linia_ordre`
--
ALTER TABLE `linia_ordre`
  MODIFY `id_linia` int(4) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `memories`
--
ALTER TABLE `memories`
  MODIFY `id_memoria` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de la tabla `monitors`
--
ALTER TABLE `monitors`
  MODIFY `id_monitor` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `ordre`
--
ALTER TABLE `ordre`
  MODIFY `id_ordre` int(4) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `plaques_mare`
--
ALTER TABLE `plaques_mare`
  MODIFY `id_placa` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `processadors`
--
ALTER TABLE `processadors`
  MODIFY `id_cpu` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `producte`
--
ALTER TABLE `producte`
  MODIFY `id_prod` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=151;

--
-- AUTO_INCREMENT de la tabla `ratolins`
--
ALTER TABLE `ratolins`
  MODIFY `id_ratoli` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `refrigeracio`
--
ALTER TABLE `refrigeracio`
  MODIFY `id_refrigeracio` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT de la tabla `subscripcio`
--
ALTER TABLE `subscripcio`
  MODIFY `id_subs` int(4) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `super_categoria`
--
ALTER TABLE `super_categoria`
  MODIFY `id_supercat` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `teclats`
--
ALTER TABLE `teclats`
  MODIFY `id_teclat` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `usuari`
--
ALTER TABLE `usuari`
  MODIFY `id_usuari` int(4) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `valoracio`
--
ALTER TABLE `valoracio`
  MODIFY `id_val` int(4) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `wishlist`
--
ALTER TABLE `wishlist`
  MODIFY `id_wish` int(4) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `auriculars`
--
ALTER TABLE `auriculars`
  ADD CONSTRAINT `producte_auriculars` FOREIGN KEY (`id_producte_fk`) REFERENCES `producte` (`id_prod`);

--
-- Filtros para la tabla `caixes`
--
ALTER TABLE `caixes`
  ADD CONSTRAINT `producte_caixa` FOREIGN KEY (`id_producte_fk`) REFERENCES `producte` (`id_prod`);

--
-- Filtros para la tabla `categoria`
--
ALTER TABLE `categoria`
  ADD CONSTRAINT `cat_supercat` FOREIGN KEY (`id_supercat_fk`) REFERENCES `super_categoria` (`id_supercat`);

--
-- Filtros para la tabla `dades_facturacio`
--
ALTER TABLE `dades_facturacio`
  ADD CONSTRAINT `ordre_dades_factura` FOREIGN KEY (`id_ordre_fk`) REFERENCES `ordre` (`id_ordre`) ON DELETE CASCADE;

--
-- Filtros para la tabla `fonts_alimentacio`
--
ALTER TABLE `fonts_alimentacio`
  ADD CONSTRAINT `producte_font` FOREIGN KEY (`id_producte_fk`) REFERENCES `producte` (`id_prod`);

--
-- Filtros para la tabla `grafiques`
--
ALTER TABLE `grafiques`
  ADD CONSTRAINT `producte_tarjeta` FOREIGN KEY (`id_producte_fk`) REFERENCES `producte` (`id_prod`);

--
-- Filtros para la tabla `linia_ordre`
--
ALTER TABLE `linia_ordre`
  ADD CONSTRAINT `ordre_linia_ordre` FOREIGN KEY (`id_ordre_fk`) REFERENCES `ordre` (`id_ordre`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `prod_linia_ordre` FOREIGN KEY (`id_producte_fk`) REFERENCES `producte` (`id_prod`);

--
-- Filtros para la tabla `memories`
--
ALTER TABLE `memories`
  ADD CONSTRAINT `producte_memoria` FOREIGN KEY (`id_producte_fk`) REFERENCES `producte` (`id_prod`);

--
-- Filtros para la tabla `monitors`
--
ALTER TABLE `monitors`
  ADD CONSTRAINT `producte_monitor` FOREIGN KEY (`id_producte_fk`) REFERENCES `producte` (`id_prod`);

--
-- Filtros para la tabla `ordre`
--
ALTER TABLE `ordre`
  ADD CONSTRAINT `usuari_ordre` FOREIGN KEY (`id_usuari_fk`) REFERENCES `usuari` (`id_usuari`) ON DELETE CASCADE;

--
-- Filtros para la tabla `plaques_mare`
--
ALTER TABLE `plaques_mare`
  ADD CONSTRAINT `producte_placa` FOREIGN KEY (`id_producte_fk`) REFERENCES `producte` (`id_prod`);

--
-- Filtros para la tabla `processadors`
--
ALTER TABLE `processadors`
  ADD CONSTRAINT `producte_cpu` FOREIGN KEY (`id_producte_fk`) REFERENCES `producte` (`id_prod`);

--
-- Filtros para la tabla `producte`
--
ALTER TABLE `producte`
  ADD CONSTRAINT `prod_cat` FOREIGN KEY (`id_categoria_fk`) REFERENCES `categoria` (`id_cat`);

--
-- Filtros para la tabla `ratolins`
--
ALTER TABLE `ratolins`
  ADD CONSTRAINT `producte_ratoli` FOREIGN KEY (`id_producte_fk`) REFERENCES `producte` (`id_prod`);

--
-- Filtros para la tabla `refrigeracio`
--
ALTER TABLE `refrigeracio`
  ADD CONSTRAINT `produte_refrigeracio` FOREIGN KEY (`id_producte_fk`) REFERENCES `producte` (`id_prod`);

--
-- Filtros para la tabla `teclats`
--
ALTER TABLE `teclats`
  ADD CONSTRAINT `producte_teclat` FOREIGN KEY (`id_producte_fk`) REFERENCES `producte` (`id_prod`);

--
-- Filtros para la tabla `valoracio`
--
ALTER TABLE `valoracio`
  ADD CONSTRAINT `val_prod` FOREIGN KEY (`id_producte_fk`) REFERENCES `producte` (`id_prod`),
  ADD CONSTRAINT `val_usu` FOREIGN KEY (`id_usuari_fk`) REFERENCES `usuari` (`id_usuari`) ON DELETE CASCADE;

--
-- Filtros para la tabla `wishlist`
--
ALTER TABLE `wishlist`
  ADD CONSTRAINT `prod_wish` FOREIGN KEY (`id_producte_fk`) REFERENCES `producte` (`id_prod`) ON DELETE CASCADE,
  ADD CONSTRAINT `usu_wish` FOREIGN KEY (`id_usuari_fk`) REFERENCES `usuari` (`id_usuari`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
