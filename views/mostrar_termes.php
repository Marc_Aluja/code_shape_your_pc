<!doctype html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title>Shape Your PC</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
              integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
              integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
        <link rel="stylesheet" href="<?php echo BASE_URL ?>/views/css/style_info.css">
        <link rel="stylesheet" href="<?php echo BASE_URL ?>/views/css/style_footer.css">
        <link rel="stylesheet" href="<?php echo BASE_URL ?>/views/css/style_header.css">
    </head>
    <body>
        <?php if(isset($_SESSION['sessio_iniciada'])) {
                include __DIR__.'/../views/includes/header_sessio.php';;
            } else {
                include __DIR__.'/../views/includes/header.php';
        } ?>

        <br/><br/>

        <div class="container">
          <div class="row">
            <div class="col-12 titol_super">
              <h3 class="text-uppercase">Termes i condicions<h3>
              <hr class="linia_titol" />
            </div>
          </div>
          <div class="container">
            <div class="my-paragraph my-paragraph">
                <h4>Introducció</h4>
                <p>
                  Aquests termes i condicions estàndard del lloc web que s'escriuen en aquesta pàgina web gestionaran l'ús del nostre lloc web, el nom
                  de ShapeYourPC accessible al lloc shapeyourPC.com.
                </p>
                <p>
                  Aquests termes s'aplicaran plenament i afectaran l'ús d'aquest lloc web. En utilitzar aquest lloc web, heu acordat acceptar tots els
                  termes i condicions que s'hagin escrit aquí. No heu d'utilitzar aquest lloc web si no esteu d'acord amb cap d'aquests termes i
                  condicions estàndard del lloc web.
                  Als menors de 18 anys no se'ls permet utilitzar aquest lloc web.
                </p>
            </div>
            <div class="my-paragraph my-paragraph">
                <h4>Drets de propietat intel·lectual</h4>
                <p>
                  A part del contingut que posseeixes, sota aquests termes, ShapeYourPC i/o els seus llicenciadors posseeixen tots
                  els drets de propietat intel·lectual i materials continguts en aquest lloc web.
                  Només se't concedeix una llicència limitada per veure el material contingut en aquest lloc web.
                </p>
            </div>
            <div class="my-paragraph my-paragraph">
                <h4>Restriccions</h4>
                <p>
                  Estàs específicament restringit a tots els següents:
                </p>
                <p>
                  <ul class="priv">
                      <li>publicar qualsevol material del lloc web en qualsevol altre mitjà;</li>
                      <li>la venda, la subscripció i/o la comercialització de qualsevol material del lloc web;</li>
                      <li>realitzant o mostrant públicament qualsevol material del lloc web;</li>
                      <li>utilitzar aquest lloc web de qualsevol manera que sigui o sigui perjudicial per a aquest lloc web;</li>
                      <li>utilitzar aquest lloc web de qualsevol manera que afecti l'accés de l'usuari a aquest lloc web;</li>
                      <li>utilitzar aquest lloc web en contra de les lleis i reglaments aplicables, o de qualsevol manera pot causar danys al
                        lloc web, o a qualsevol persona o entitat empresarial;</li>
                      <li>participar en qualsevol mineria de dades, recollida de dades, extracció de dades o qualsevol altra activitat similar
                        en relació amb aquest lloc web;</li>
                      <li>utilitzant aquest lloc web per participar en qualsevol publicitat o màrqueting.</li>
                  </ul>
                </p>
                <p>
                  Determinades àrees d'aquest lloc web estan limitades a l'accés per part vostra i ShapeYourPC pot restringir
                  l'accés de vosaltres a qualsevol àrea d'aquest lloc web, en qualsevol moment, a discreció absoluta. Qualsevol ID d'usuari i
                  contrasenya que tingueu per a aquest lloc web són confidencials i també heu de mantenir la confidencialitat.
                </p>
            </div>
            <div class="my-paragraph my-paragraph">
                <h4>El vostre contingut</h4>
                <p>
                  En aquests termes i condicions estàndard del lloc web, “Contingut del vostre” voldrà dir qualsevol àudio, text de vídeo,
                  imatges o qualsevol altre material que escolliu mostrar en aquest lloc web. Mitjançant la visualització del vostre contingut,
                  concediu a ShapeYourPC una llicència no exclusiva, irrevocable i subarrendable a tot el món per utilitzar, reproduir, adaptar,
                  publicar, traduir i distribuir en qualsevol i tots els mitjans.
                </p>
                <p>
                  El seu contingut ha de ser propi i no ha d'invadir els drets de cap tercer. ShapeYourPC es reserva el dret d'eliminar
                  qualsevol del vostre contingut d'aquest lloc web en qualsevol moment sense avís previ.
                </p>
            </div>
            <div class="my-paragraph my-paragraph">
                <h4>Sense garanties</h4>
                <p>
                  Aquest lloc web es proporciona "com està", amb totes les falles, i ShapeYourPC no expressa cap representació o garantia,
                  de cap mena relacionada amb aquest lloc web o els materials continguts en aquest lloc web. A més, res contingut en aquest lloc
                  web s'interpretarà com a assessorament.
                </p>
            </div>
            <div class="my-paragraph my-paragraph">
                <h4>Limitació de la responsabilitat</h4>
                <p>
                  En cap cas ShapeYourPC, ni cap dels seus oficials, directors i empleats, seran considerats responsables de qualsevol
                  cosa que sorgeixi o que estigui relacionada amb l'ús d'aquesta pàgina web, independentment que aquesta responsabilitat estigui
                  sota contracte. ShapeYourPC, inclosos els seus oficials, directors i empleats, no es considerarà responsable de cap
                  responsabilitat indirecta, consegüent o especial que sorgeixi o de qualsevol forma relacionada amb l'ús d'aquest lloc web.
                </p>
            </div>
            <div class="my-paragraph my-paragraph">
                <h4>Indemnització</h4>
                <p>
                  Per tant, indemnitza a la màxima extensió de ShapeYourPC i en contra de qualsevol responsabilitat, costos, demandes,
                  causes d'acció, danys i despeses derivades de qualsevol forma relacionada amb el seu incompliment de qualsevol de les disposicions
                  d'aquests termes.
                </p>
            </div>
            <div class="my-paragraph my-paragraph">
                <h4>Variació de termes</h4>
                <p>
                  ShapeYourPC està permès de revisar aquests termes en qualsevol moment, i s'espera que mitjançant aquest lloc web reviseu
                  aquests termes de forma regular.
                </p>
            </div>
            <div class="my-paragraph my-paragraph">
                <h4>Assignació</h4>
                <p>
                  ShapeYourPC està permès d'assignar, transferir i subcontractar els seus drets i/o obligacions sota aquests termes sense
                  cap notificació. No obstant això, no podeu assignar, transferir o subcontractar cap dels vostres drets i/o obligacions en virtut
                  d'aquests termes.
                </p>
            </div>
            <div class="my-paragraph my-paragraph">
                <h4>Llei de govern i jurisdicció</h4>
                <p>
                  Aquests termes seran governats i interpretats d'acord amb les lleis de Espanya, i se sotmetrà a la jurisdicció no
                  exclusiva dels tribunals estatals i federals situats a Espanya per a la resolució de qualsevol disputa.
                </p>
            </div>
          </div>
        </div>

        <br/><br/>

        <?php include __DIR__.'/../views/includes/footer.php'; ?>

    </body>
</html>
