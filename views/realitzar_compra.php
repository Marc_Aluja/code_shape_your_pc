<!doctype html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title>Shape Your PC</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
              integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
              integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
        <link rel="stylesheet" href="<?php echo BASE_URL ?>/views/css/style_comprar.css">
        <link rel="stylesheet" href="<?php echo BASE_URL ?>/views/css/style_footer.css">
        <link rel="stylesheet" href="<?php echo BASE_URL ?>/views/css/style_header.css">
    </head>
    <body>
        <?php if(isset($_SESSION['sessio_iniciada'])) {
                include __DIR__.'/../views/includes/header_sessio.php';;
            } else {
                include __DIR__.'/../views/includes/header.php';
        } ?>

        <br/><br/>

        <div class="container cont-data">
          <div class="row">
            <div class="col-12 titol_super">
              <h3 class="text-uppercase">Realitzar compra<h3>
              <hr class="linia_titol" />
            </div>
          </div>
          <div class="container">
              <div class="row">
                <div class="stepwizard col-md-offset-12">
                  <div class="stepwizard-row setup-panel">
                      <div class="stepwizard-step">
                        <a id="btn-s1" type="button" class="btn btn-primary btn-circle" disabled="disabled">1</a>
                      </div>
                      <div class="stepwizard-step">
                        <a id="btn-s2" type="button" class="btn btn-light btn-circle" disabled="disabled">2</a>
                      </div>
                      <div class="stepwizard-step">
                        <a id="btn-s3" type="button" class="btn btn-light btn-circle" disabled="disabled">3</a>
                      </div>
                  </div>
                </div>
              </div>
              <form class="" action="<?php echo BASE_URL ?>/index.php?action=confirmar_compra" method="post">
                <section class="mt-4" id="step-1">
                  <div class="row justify-content-center">
                    <div class="col-lg-6">
                        <div class="mx-auto mb-4">
                            <h3>Dades d'enviament</h3>
                        </div>
                        <div class="card">
                            <div class="card-header">
                                <div class="bg-white shadow-sm px-2 py-2">
                                  <div class="card-body">
                                    <div class="md-form md-outline mb-lg-3">
                                      <label for="envia-nom">Nom i Cognom</label>
                                      <input type="text" id="envia-nom" name="envia-nom" class="form-control" required="required" value="<?php echo $dades['nom'] ?>" readonly>
                                    </div>
                                    <div class="md-form md-outline mb-lg-3">
                                      <label for="envia-pais">Pais: </label>
                                      <input type="text" id="envia-pais" name ="envia-pais" class="form-control" required="required" value="<?php echo $dades['pais'] ?>" readonly>
                                    </div>
                                    <div class="md-form md-outline mb-lg-3">
                                      <label for="envia-provincia">Provincia: </label>
                                      <input type="text" id="envia-provincia" name ="envia-provincia" class="form-control" required="required" value="<?php echo $dades['provincia'] ?>" readonly>
                                    </div>
                                    <div class="md-form md-outline mb-lg-3">
                                      <label for="envia-ciutat">Ciutat: </label>
                                      <input type="text" id="envia-ciutat" name ="envia-ciutat" class="form-control" required="required" value="<?php echo $dades['ciutat'] ?>" readonly>
                                    </div>
                                    <div class="md-form md-outline mb-lg-3">
                                      <label for="envia-direccio">Direcció</label>
                                      <input type="text" id="envia-direccio" name="envia-direccio" placeholder="House number and street name" class="form-control"
                                             required="required" value="<?php echo $dades['direccio'] ?>" readonly>
                                    </div>
                                    <div class="md-form md-outline mb-lg-3">
                                      <label for="envia-detalls-dir">Detalls direcció</label>
                                      <input type="text" id="envia-detalls-dir" name="envia-detalls-dir" placeholder="Porta, pis, escala..." class="form-control"
                                             required="required" value="<?php echo $dades['detall_dir'] ?>" readonly>
                                    </div>
                                    <div class="md-form md-outline mb-lg-3">
                                      <label for="envia-cp">Codi postal</label>
                                      <input type="text" id="envia-cp" name="envia-cp" class="form-control" required="required" value="<?php echo $dades['codi_postal'] ?>" readonly>
                                    </div>
                                    <div class="md-form md-outline mb-lg-3">
                                      <label for="envia-telefon">Teléfon</label>
                                      <input type="number" id="envia-telefon" name="envia-telefon" class="form-control" required="required" value="<?php echo $dades['telefon'] ?>" readonly>
                                    </div>
                                    <div class="md-form md-outline mb-lg-3">
                                      <input id="factura-diff" type="checkbox">
                                      <label class="control-label text-termes" for="termes">Direcció de facturació diferent</label>
                                    </div>
                                    <div class="md-form md-outline mb-lg-3">
                                      <span class="col-md-12 control-label text-termes pl-0" for="termes">*Modifica el perfil per actualitzar les dades d'enviament.</span>
                                    </div>
                                  </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 dades-facturacio">
                        <div class="mx-auto mb-4">
                            <h3>Dades de facturació</h3>
                        </div>
                        <div class="card">
                            <div class="card-header">
                                <div class="bg-white shadow-sm px-2 py-2">
                                  <div class="card-body">
                                    <div class="md-form md-outline mb-lg-3">
                                      <label for="factura-nom">Nom i Cognom</label>
                                      <input type="text" id="factura-nom" name="factura-nom" class="form-control" required="required" value="">
                                    </div>
                                    <div class="md-form md-outline mb-lg-3">
                                      <label for="factura-pais">Pais: </label>
                                      <input type="text" id="factura-pais" name ="factura-pais" class="form-control" required="required" value="">
                                    </div>
                                    <div class="md-form md-outline mb-lg-3">
                                      <label for="factura-provincia">Provincia: </label>
                                      <input type="text" id="factura-provincia" name ="factura-provincia" class="form-control" required="required" value="">
                                    </div>
                                    <div class="md-form md-outline mb-lg-3">
                                      <label for="factura-ciutat">Ciutat: </label>
                                      <input type="text" id="factura-ciutat" name ="factura-ciutat" class="form-control" required="required" value="">
                                    </div>
                                    <div class="md-form md-outline mb-lg-3">
                                      <label for="factura-direccio">Direcció</label>
                                      <input type="text" id="factura-direccio" name="factura-direccio" placeholder="" class="form-control"
                                             required="required" value="">
                                    </div>
                                    <div class="md-form md-outline mb-lg-3">
                                      <label for="factura-detalls-dir">Detalls direcció</label>
                                      <input type="text" id="factura-detalls-dir" name="factura-detalls-dir" placeholder="Porta, pis, escala..." class="form-control"
                                             required="required" value="">
                                    </div>
                                    <div class="md-form md-outline mb-lg-3">
                                      <label for="factura-cp">Codi postal</label>
                                      <input type="text" id="factura-cp" name="factura-cp" class="form-control" required="required" value="">
                                    </div>
                                    <div class="md-form md-outline mb-lg-3">
                                      <label for="factura-telefon">Teléfon</label>
                                      <input type="number" id="factura-telefon" name="factura-telefon" class="form-control" required="required" value="">
                                    </div>
                                    <div class="billing-error mt-2 mb-2">

                                    </div>
                                  </div>
                                </div>
                            </div>
                        </div>
                    </div>
                  </div>
                </section>
                <section class="mt-3" id="step-2">
                  <div class="row mb-2">
                      <div class="col-lg-12 mx-auto">
                          <h3>Resum</h3>
                      </div>
                  </div>
                  <div class="row justify-content-center mb-2">
                    <div class="col-lg-6">
                      <div class="card">
                          <div class="card-header">
                              <div class="bg-white shadow-sm px-2 py-2">
                                <div class="card-body resum-content">
                                  <ul class="ul-resum">
                                    <li><h5>Direcció d'enviament</h5></li>
                                    <li><span class="resum-envia-nom"><?php echo $dades['nom'] ?></span></li>
                                    <li><span class="resum-envia-direccio"><?php echo $dades['direccio'] ?></span>,
                                      <span class="resum-envia-detalls-dir"><?php echo $dades['detall_dir'] ?></span></li>
                                    <li><span class="resum-envia-provincia"><?php echo $dades['provincia'] ?></span>,
                                      <span class="resum-envia-ciutat"><?php echo $dades['ciutat'] ?></span> <span class="resum-envia-cp"> </span></li>
                                    <li><span class="resum-envia-pais"><?php echo $dades['pais'] ?></span></li>
                                    <li>Telèfon: <span class="resum-envia-telefon"><?php echo $dades['telefon'] ?></span></li>
                                    <br/>
                                    <li><h5>Direcció de facturació</h5></li>
                                    <li><span class="resum-factura-nom"> </span></li>
                                    <li><span class="resum-factura-email"> </span></li>
                                    <li><span class="resum-factura-direccio"> </span>, <span class="resum-factura-detalls-dir"> </span></li>
                                    <li><span class="resum-factura-provincia"> </span>, <span class="resum-factura-ciutat"> </span> <span class="resum-factura-cp"> </span></li>
                                    <li><span class="resum-factura-pais"> </span></li>
                                    <li>Telèfon: <span class="resum-factura-telefon"> </span></li>
                                  </ul>
                                </div>
                              </div>
                          </div>
                      </div>
                      <div class="card mt-4 mb-4 col-lg-9">
                        <div class="card-body">
                          <a class="dark-grey-text d-flex justify-content-between" data-toggle="collapse" href="#collapseExample"
                            aria-expanded="false" aria-controls="collapseExample">
                            Afegeix un codi de descompte (opcional)
                            <span><i class="fas fa-chevron-down pt-1"></i></span>
                          </a>
                          <div class="collapse" id="collapseExample">
                            <div class="mt-3">
                              <div class="md-form md-outline mb-0">
                                <input type="text" id="discount-code" class="form-control font-weight-light"
                                  placeholder="Enter discount code">
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="card">
                          <div class="card-header">
                              <div class="bg-white shadow-sm px-2 py-2">
                                <div class="card-body">
                                  <h5 class="mb-3">Total carret</h5>
                                  <ul class="list-group list-group-flush">
                                    <?php foreach ($productes_carret as $producte):?>
                                      <?php $imatge = explode(" ", $producte['imatge']); ?>
                                      <li class="list-group-item d-flex justify-content-between align-items-center border-0 px-0 pb-0">
                                        <img class="img-prod-check img-fluid" src="<?php echo BASE_URL ?><?php echo $imatge[0] ?>">
                                        <span><?php echo $producte['nom'] ?></span>
                                        <span>Qty: <?php echo $_SESSION['carret']['productes'][$producte['id_prod']]['quantitat'] ?></span>

                                      </li>
                                    <?php endforeach; ?>
                                    <li class="list-group-item d-flex justify-content-between align-items-center border-0 px-0 pb-0">
                                      Subtotal items <span>&euro; <?php echo $preu_carret ?></span>
                                    </li>
                                    <li class="list-group-item d-flex justify-content-between align-items-center px-0">
                                      Enviament <span>&euro; 3.00</span>
                                    </li>
                                    <li class="list-group-item d-flex justify-content-between align-items-center border-0 px-0 mb-3">
                                      <div>
                                        <?php if($_SESSION['carret']['ordinador'] == "true") { ?>
                                          <div class="md-form md-outline">
                                            <input id="factura-diff" type="checkbox">
                                            <label class="control-label text-termes ml-2" for="termes">Marca si vols rebre l'ordinador muntat</label>
                                          </div>
                                        <?php } ?>
                                        <strong>Preu total de la compra</strong>
                                        <strong>
                                          <p class="mb-0">(incloent IVA)</p>
                                        </strong>
                                      </div>
                                      <span><strong>&euro; <?php echo ($preu_carret + 3) ?></strong></span>
                                    </li>
                                  </ul>
                                </div>
                              </div>
                          </div>
                      </div>
                    </div>
                  </div>
                </section>
                <section class="mt-3" id="step-3">
                  <div class="row justify-content-center mb-2">
                      <div class="col-lg-6 mx-auto">
                          <h3>Mètode de pagament</h3>
                      </div>
                  </div>
                  <div class="row justify-content-center">
                    <div class="col-lg-6">
                        <div class="card ">
                            <div class="card-header">
                                <div class="bg-white shadow-sm pt-4 pl-2 pr-2 pb-2">
                                    <ul role="tablist" class="nav bg-light nav-pills rounded nav-fill mb-3">
                                        <li class="nav-item"> <a data-toggle="pill" href="#credit-card" class="nav-link active "> <i class="fas fa-credit-card mr-2"></i> Credit Card </a> </li>
                                    </ul>
                                </div>
                                <div class="tab-content">
                                  <div id="credit-card" class="tab-pane fade show active pt-3">
                                        <div class="form-group">
                                            <label for="username"> <h6>Titular de la terjeta</h6> </label>
                                            <input type="text" name="username" placeholder="Card Owner Name" required class="form-control"
                                                   pattern="^(([A-Za-z]+[,.]?[ ]?|[a-z]+['-]?)+)$">
                                        </div>
                                        <div class="form-group">
                                          <label for="cardNumber"> <h6>Número de tarjeta</h6></label>
                                          <div class="input-group"> <input type="text" id="card_number" name="cardNumber" placeholder="Valid card number" class="form-control "
                                                                     pattern="(\d+ +\d+ +\d+ +\d+)" maxlength="19" required>
                                              <div class="input-group-append">
                                                <span class="input-group-text text-muted"> <i class="fab fa-cc-visa mx-1"></i> <i class="fab fa-cc-mastercard mx-1"></i> <i class="fab fa-cc-amex mx-1"></i> </span>
                                              </div>
                                          </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-8">
                                                <div class="form-group">
                                                  <label>
                                                    <span class="hidden-xs">
                                                            <h6>Data de caducitat</h6>
                                                    </span>
                                                  </label>
                                                  <div class="input-group"> <input type="text" placeholder="MM" name="" class="form-control" pattern="\d*" maxlength="2"required>
                                                    <input type="text" placeholder="YY" name="" class="form-control" pattern="\d*" maxlength="2" required>
                                                  </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group mb-4">
                                                  <label data-toggle="tooltip" title="Three digit CV code on the back of your card">
                                                        <h6>CVV <i class="fa fa-question-circle d-inline"></i></h6>
                                                  </label>
                                                  <input type="text" required class="form-control" pattern="\d*" maxlength="3" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-footer">
                                          <button type="submit" class="subscribe btn btn-primary btn-block shadow-sm">Confirmar pagament</button>
                                        </div>
                                  </div>
                                </div>
                            </div>
                        </div>
                    </div>
                  </div>
                </section>
              </form>
              <div class="row justify-content-between mt-5">
                <button class="btn btn-primary backBtn btn-md pull-right ml-4" type="button">Back</button>
                <button class="btn btn-primary nextBtn btn-md pull-right mr-4" type="button">Next</button>
              </div>
            </div>
          </div>
        </div>

        <br/><br/>

        <?php include __DIR__.'/../views/includes/footer.php'; ?>

        <!--Scripts particulars de la pagina-->
        <script src="<?php echo BASE_URL ?>/views/js/comprar.js"></script>
    </body>
</html>
