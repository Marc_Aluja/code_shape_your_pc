<!doctype html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title>Shape Your PC</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
              integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
              integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
              <link rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" />
        <link rel="stylesheet" href="<?php echo BASE_URL ?>/views/css/style_ordres.css">
        <link rel="stylesheet" href="<?php echo BASE_URL ?>/views/css/style_footer.css">
        <link rel="stylesheet" href="<?php echo BASE_URL ?>/views/css/style_header.css">
    </head>
    <body>
        <?php if(isset($_SESSION['sessio_iniciada'])) {
                include __DIR__.'/../views/includes/header_sessio.php';;
            } else {
                include __DIR__.'/../views/includes/header.php';
        } ?>

        <br/><br/>

        <div class="container">
          <div class="row">
            <div class="col-12 titol_super">
              <h3 class="text-uppercase">Ordre XGBQW-JUTYH-<?php echo $ordre['id_ordre'] ?><h3>
              <hr class="linia_titol" />
            </div>
          </div>
          <div class="container cont-orders">
            <div class="d-flex flex-column justify-content-center align-items-center" id="ordre-head">
                <div class="h4">Data de l'ordre: <?php echo $ordre['data'] ?></div>
                <div class="pt-1">
                    <p>L'ordre #XGBQW-JUTYHGFV-<?php echo $ordre['id_ordre']?> esta en<b class="text-dark"> proces</b></p>
                </div>
            </div>
            <div class="sub-contenidor bg-white">
                <div class="row justify-content-between mb-4 mt-4">
                    <div class="col-2">
                      <span class="ml-4"><b>Producte</b></span>
                    </div>
                    <div class="col-2">
                      <span class="ml-2"><b>Preu</b></span>
                    </div>
                </div>
                <?php foreach ($detalls as $linia):?>
                <div class="row mb-4">
                    <div class="col">
                        <div class="card card-2">
                            <div class="card-body py-1">
                                <div class="media">
                                    <div class="media-body my-auto text-right">
                                        <div class="row my-auto flex-column flex-md-row">
                                            <?php $imatge = explode(" ", $linia['img_prod']); ?>
                                            <img class="col-2 img w-25 my-auto align-self-center pl-0 p-0 m-0"
                                            src="<?php echo BASE_URL ?><?php echo $imatge[0] ?>" />
                                            <div class="col-4 my-auto">
                                                <h6 class="mb-0"><?php echo $linia['nom_prod'] ?></h6>
                                            </div>
                                            <div class="col-2 my-auto"><b><?php echo $linia['marca_prod'] ?></b></div>
                                            <div class="col-2 my-auto"> <span>Qty : <?php echo $linia['quantitat'] ?></span></div>
                                            <div class="col-2 my-auto">
                                                <h6 class="mb-0">&euro; <?php echo $linia['preu'] ?></h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>
                <div class="pt-2 border-bottom mb-3"></div>
                <div class="d-flex justify-content-start align-items-center pl-3">
                    <div class="text-muted">Mètode de pagament</div>
                    <div class="ml-auto"> <img src="https://www.freepnglogos.com/uploads/mastercard-png/mastercard-logo-logok-15.png" alt="" width="30" height="30"> <label>Mastercard ******5342</label> </div>
                </div>
                <div class="d-flex justify-content-start align-items-center py-1 pl-3">
                    <div class="text-muted">Enviament</div>
                    <div class="ml-auto"> <label>&euro; 3.00</label> </div>
                </div>
                <div class="d-flex justify-content-start align-items-center pl-3 py-3 mb-4 border-bottom">
                    <div class="text-muted"> Total ordre </div>
                    <div class="ml-auto h5">&euro; <?php echo $ordre['preu'] ?> </div>
                </div>
                <div class="row border rounded p-1 my-3">
                    <div class="col-md-6 py-3">
                        <div class="d-flex flex-column align-items start"> <b>Adreça d'enviament</b>
                            <p class="text-justificat pt-2"><?php echo $dades_envia['nom'] ?>, <?php echo $dades_envia['direccio'] ?>,
                               <?php echo $dades_envia['detall_dir'] ?>, <?php echo $dades_envia['codi_postal'] ?></p>
                            <p class="text-justificat"><?php echo $dades_envia['pais'] ?>, <?php echo $dades_envia['provincia'] ?>, <?php echo $dades_envia['ciutat'] ?></p>
                            <p class="text-justificat">Telf. <?php echo $dades_envia['telefon'] ?></p>
                        </div>
                    </div>
                    <div class="col-md-6 py-3">
                        <div class="d-flex flex-column align-items start"> <b>Adreça de facturació</b>
                          <p class="text-justificat pt-2"><?php echo $dades_facturacio['nom'] ?>, <?php echo $dades_facturacio['direccio'] ?>,
                             <?php echo $dades_facturacio['detall_dir'] ?>, <?php echo $dades_envia['codi_postal'] ?></p>
                          <p class="text-justificat"><?php echo $dades_facturacio['pais'] ?>, <?php echo $dades_facturacio['provincia'] ?>,
                             <?php echo $dades_facturacio['ciutat'] ?></p>
                          <p class="text-justificat">Telf. <?php echo $dades_facturacio['telefon'] ?></p>
                        </div>
                    </div>
                </div>
            </div>
          </div>
        </div>

        <br/><br/>

        <?php include __DIR__.'/../views/includes/footer.php'; ?>

    </body>
</html>
