<!doctype html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title>Shape Your PC</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
              integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
              integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
        <link rel="stylesheet" href="<?php echo BASE_URL ?>/views/css/style_creador.css">
        <link rel="stylesheet" href="<?php echo BASE_URL ?>/views/css/style_footer.css">
        <link rel="stylesheet" href="<?php echo BASE_URL ?>/views/css/style_header.css">
    </head>
    <body>
        <?php if(isset($_SESSION['sessio_iniciada'])) {
                include __DIR__.'/../views/includes/header_sessio.php';;
            } else {
                include __DIR__.'/../views/includes/header.php';
        } ?>

        <br/><br/>

        <div class="container cont-crea">
          <div class="row">
            <div class="col-12 titol_super">
              <h3><?php echo $nom_super_cat ?><h3>
              <hr class="linia_titol" />
            </div>
          </div>
          <br/>

          <div class="row col-md-offset-2 custyle justify-content-center">
            <div class="col-8">
              <table class="table table-custom">
                <tr>
                    <th colspan="4">Selecciona los componentes per a configurar el teu PC a mesura.
                      <p>Els elements marcats amb un * son obligatoris</p>
                      <p>Pots escollir entre una de les dos memories secundàries (SSD, HDD o les dues).</p></th>
                </tr>
                <?php foreach ($categories as $categoria):
                  if($categoria['nom'] != "MEMORIES") { ?>
                      <tr class="grisa">
                          <?php $logo = explode(" ", $categoria['logo']); ?>
                          <td class="col-logo"><img class="logo ml-4" src="<?php echo BASE_URL ?><?php echo $logo[0] ?>" alt="Sample"></td>
                          <?php if($categoria['nom'] != "RATOLINS" && $categoria['nom'] != "TECLATS" && $categoria['nom'] != "AURICULARS"
                                && $categoria['nom'] != "MONITORS") { ?>
                              <td class="col-nom"><?php echo $categoria['nom'] ?> *</td>
                          <?php } else { ?>
                              <td class="col-nom"><?php echo $categoria['nom'] ?></td>
                          <?php } ?>
                          <td class="text-center">
                            <button type="button" class="btn btn-rounded btn-info" data-toggle="modal" data-target="#modalSeleccionar"
                                    onclick="selec_producte('<?php echo $categoria['nom'] ?>', '')">
                              <i class="fas fa-plus"></i></button>
                          </td>
                      </tr>
                      <tr id="seleccio-<?php echo $categoria['nom'] ?>"></tr>
                  <?php } else { ?>
                      <?php $logo = explode(" ", $categoria['logo']); ?>
                      <tr class="grisa">
                          <td class="col-logo"><img class="logo ml-4" src="<?php echo BASE_URL ?><?php echo $logo[0] ?>" alt="Sample"></td>
                          <td class="col-nom">MEMORIA RAM *</td>
                          <td class="text-center">
                            <button type="button" class="btn btn-rounded btn-info" data-toggle="modal" data-target="#modalSeleccionar"
                                    onclick="selec_producte('<?php echo $categoria['nom'] ?>', 'ram')">
                              <i class="fas fa-plus"></i></button>
                          </td>
                      </tr>
                      <tr id="seleccio-<?php echo $categoria['nom'] ?>ram"></tr>
                      <tr class="grisa">
                          <td class="col-logo"><img class="logo ml-4" src="<?php echo BASE_URL ?><?php echo $logo[1] ?>" alt="Sample"></td>
                          <td class="col-nom">MEMORIA SSD *</td>
                          <td class="text-center">
                            <button type="button" class="btn btn-rounded btn-info" data-toggle="modal" data-target="#modalSeleccionar"
                                    onclick="selec_producte('<?php echo $categoria['nom'] ?>', 'ssd')">
                              <i class="fas fa-plus"></i></button>
                          </td>
                      </tr>
                      <tr id="seleccio-<?php echo $categoria['nom'] ?>ssd"></tr>
                      <tr class="grisa">
                          <td class="col-logo"><img class="logo ml-4" src="<?php echo BASE_URL ?><?php echo $logo[2] ?>" alt="Sample"></td>
                          <td class="col-nom">MEMORIA HDD *</td>
                          <td class="text-center">
                            <button type="button" class="btn btn-rounded btn-info" data-toggle="modal" data-target="#modalSeleccionar"
                                    onclick="selec_producte('<?php echo $categoria['nom'] ?>', 'hdd')">
                              <i class="fas fa-plus"></i></button>
                          </td>
                      </tr>
                      <tr id="seleccio-<?php echo $categoria['nom'] ?>hdd"></tr>
                  <?php } ?>
                <?php endforeach; ?>
              </table>
            </div>
            <div class="col-4">
              <section class="shopping-cart dark">
          	 		<div class="container pt-4">
                  <div class="content">
                    <div class="row">
          			 			<div class="col-12">
          			 				<div id="resum-seleccio" class="summary">
                          <h3>Resum</h3>
                          <div class="summary-item"><span class="text">Nº items</span><span class="preu">0 items</span></div>
                          <div class="summary-item"><span class="text">Total</span><span class="preu">0 €</span></div>
                          <button type="button" class="btn btn-primary btn-lg btn-block">Afegir al carret</button>
          				 			</div>
          			 			</div>
          		 			</div>
                  </div>
          	 		</div>
          		</section>
            </div>
          </div>

        </div>

        <br/><br/>

        <?php include __DIR__.'/../views/includes/footer.php'; ?>

        <!-- Modal -->
        <div class="modal fade" id="modalSeleccionar" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Seleccionar producte</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div id="selec-prod-body" class="modal-body">

              </div>
              <div class="modal-footer">
                <span>Selecciona un producte de la llista</span>
              </div>
            </div>
          </div>
        </div>

        <!--Scripts particulars de la pagina-->
        <script src="<?php echo BASE_URL ?>/views/js/creador.js" type="text/babel"></script>
    </body>
</html>
