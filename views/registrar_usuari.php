<!doctype html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title>Shape Your PC</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
              integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
              integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
        <link rel="stylesheet" href="<?php echo BASE_URL ?>/views/css/style_registre_perfil.css">
        <link rel="stylesheet" href="<?php echo BASE_URL ?>/views/css/style_footer.css">
        <link rel="stylesheet" href="<?php echo BASE_URL ?>/views/css/style_header.css">
    </head>
    <body>
        <?php if(isset($_SESSION['sessio_iniciada'])) {
                include __DIR__.'/../views/includes/header_sessio.php';
            } else {
                include __DIR__.'/../views/includes/header.php';
        } ?>

        <br/><br/>

        <div class="container cont-reg">
          <div class="row">
            <div class="col-12 titol_registre">
              <h3 class="text-uppercase">Registre d'usuari<h3>
              <hr class="linia_titol" />
            </div>
          </div>
          <br/>
          <form class="form-inline needs-validation" action="" method="post" novalidate>
          <fieldset>
            <div class="form-group">
              <label class="col-md-4 control-label" for="nom">Nom: </label>
              <div class="col-md-6">
                <input id="nom" name="registre_nom" type="text" placeholder="ex: Marc Garcia" class="form-control input-md" size="50"
                       pattern="^[A-Za-zÀ-ÿ ,.'-]+$" title="Solo letras, sin numeros o caracteres especiales" required>
                <div class="invalid-feedback mt-2">
                  <p class="text_error ml-2">Indica correactament el nom d'usuari</p>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-4 control-label" for="email">Email: </label>
              <div class="col-md-6">
                <input id="email" name="registre_email" type="email" placeholder="ex: exemple@gmail.com" class="form-control input-md"
                      size="50" required="required">
                <div class="invalid-feedback mt-2">
                  <p class="text_error ml-2">Indica correactament l'email</p>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-4 control-label" for="pass">Contrassenya: </label>
              <div class="col-md-6">
                <input id="pass" name="registre_contrassenya" type="password" placeholder="ex: algoexemple67" class="form-control input-md"
                       pattern="[A-Za-z0-9]+" size="50" maxlength="15" required="required">
                <div class="invalid-feedback mt-2">
                  <p class="text_error ml-2">Indica correactament la contrassenya</p>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-4 control-label" for="rpass">Repeteix contrassenya: </label>
              <div class="col-md-6">
                <input id="rpass" name="registre_rcontrassenya" type="password" placeholder="" class="form-control input-md" size="50"
                       pattern="[A-Za-z0-9]+" maxlength="15" required="required">
                <div class="invalid-feedback mt-2">
                  <p class="text_error ml-2">No es igual a la contrasssenya anterior</p>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-4 control-label" for="direccio">Direccio: </label>
              <div class="col-md-6">
                <input id="direccio" name="registre_direccio" type="text" placeholder="ex: Plaça tormenta 56" class="form-control input-md"
                       pattern="[A-Za-z0-9'\.\-\s\,]" size="50" required="required">
                <div class="invalid-feedback mt-2">
                  <p class="text_error ml-2">Indica correactament la direcció</p>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-4 control-label" for="detalls_dir">Detalls direcció: </label>
              <div class="col-md-6">
                <input id="detalls_dir" name="registre_detalls" type="text" placeholder="ex: pis, porta, escala..." class="form-control input-md"
                       pattern="[A-Za-z0-9'\.\-\s\,]" size="50" required="required">
                <div class="invalid-feedback mt-2">
                  <p class="text_error ml-2">Indica correactament els detalls de la direcció</p>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-4 control-label" for="pais">País: </label>
              <div class="col-md-6">
                <input id="pais" name="registre_pais" type="text" placeholder="ex: Espanya" class="form-control input-md"
                       pattern="^(?:[A-Za-z]{2,}(?:(\.\s|'s\s|\s?-\s?|\s)?(?=[A-Za-z]+))){1,2}(?:[A-Za-z]+)?$" size="50" required="required">
                <div class="invalid-feedback mt-2">
                  <p class="text_error ml-2">Indica correactament la ciutat de residencia</p>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-4 control-label" for="provincia">Província: </label>
              <div class="col-md-6">
                <input id="provincia" name="registre_provincia" type="text" placeholder="ex: Teruel" class="form-control input-md"
                       pattern="^(?:[A-Za-z]{2,}(?:(\.\s|'s\s|\s?-\s?|\s)?(?=[A-Za-z]+))){1,2}(?:[A-Za-z]+)?$" size="50" required="required">
                <div class="invalid-feedback mt-2">
                  <p class="text_error ml-2">Indica correactament la ciutat de residencia</p>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-4 control-label" for="ciutat">Ciutat: </label>
              <div class="col-md-6">
                <input id="ciutat" name="registre_ciutat" type="text" placeholder="ex: Barcelona" class="form-control input-md"
                       pattern="^(?:[A-Za-z]{2,}(?:(\.\s|'s\s|\s?-\s?|\s)?(?=[A-Za-z]+))){1,2}(?:[A-Za-z]+)?$" size="50" required="required">
                <div class="invalid-feedback mt-2">
                  <p class="text_error ml-2">Indica correactament la ciutat de residencia</p>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-4 control-label" for="zip">Codi postal: </label>
              <div class="col-md-6">
                <input id="zip" name="registre_cp" type="text" placeholder="ex: 09067" class="form-control input-md" size="50"
                       pattern="[0-9]{5}" required="required">
                <div class="invalid-feedback mt-2">
                  <p class="text_error ml-2">Indica correactament el codi postal</p>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-4 control-label" for="telefon">Telèfon: </label>
              <div class="col-md-6">
                <input id="telefon" name="registre_telefon" type="text" placeholder="ex: 874532211" class="form-control input-md"
                       pattern="[0-9]{9}" size="50" required="required">
                <div class="invalid-feedback mt-2">
                  <p class="text_error ml-2">Indica correactament el telèfon</p>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-4 control-label" for="termes"></label>
              <div class="col-md-6">
                <input id="termes" type="checkbox" name="termes" required="required">
                <label class="col-md-4 control-label text-termes" for="termes">Estic d'acord amb els <a>termes i condicions </a>de servei i privacitat.</label>
                <div class="invalid-feedback mt-2">
                  <p class="text_error ml-2">Marca la casella si us plau</p>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-4 control-label" for="submit"></label>
              <div class="col-md-6 mt-3">
                <button id="submit" name="submit_registrar" class="btn btn-primary btn-block btn-lg active rounded">Registrar-se</button>
              </div>
            </div>
          </fieldset>
          </form>
        </div>

        <br/><br/>

        <?php include __DIR__.'/../views/includes/footer.php'; ?>

        <!--Scripts particulars de la pagina-->
        <script src="<?php echo BASE_URL ?>/views/js/usuari.js"></script>

        <?php if(isset($_SESSION['avis_registrat'])) {
          echo $_SESSION['avis_registrat'];
          unset($_SESSION['avis_registrat']);
        } ?>
    </body>
</html>
