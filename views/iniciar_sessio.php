<!doctype html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title>Shape Your PC</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
              integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
              integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
        <link rel="stylesheet" href="<?php echo BASE_URL ?>/views/css/style_sessio.css">
        <link rel="stylesheet" href="<?php echo BASE_URL ?>/views/css/style_footer.css">
        <link rel="stylesheet" href="<?php echo BASE_URL ?>/views/css/style_header.css">
    </head>
    <body>
        <?php if(isset($_SESSION['sessio_iniciada'])) {
                include __DIR__.'/../views/includes/header_sessio.php';
            } else {
                include __DIR__.'/../views/includes/header.php';
        } ?>

        <br/><br/>

        <div class="container cont-log">
          <div class="row">
            <div class="col-12 titol_registre">
              <h3 class="text-uppercase">Inici de sessió<h3>
              <hr class="linia_titol" />
            </div>
          </div>
          <div class="login-form">
              <form class="needs-validation" action="" method="post" novalidate>
                  <h2 class="text-center titol mb-4">Compte</h2>
                  <div class="form-group">
                      <input type="email" name="sessio_usuari" class="form-control rounded-pill" placeholder="Usuari" required="required">
                      <div class="invalid-feedback mt-2">
                        <p class="text_error ml-2">Indica correactament el nom d'usuari.</p>
                      </div>
                  </div>
                  <div class="form-group">
                      <input type="password" name="sessio_contrassenya" class="form-control rounded-pill" placeholder="Contrassenya" pattern="[A-Za-z0-9]+" required="required">
                      <div class="invalid-feedback mt-2">
                        <p class="text_error ml-2">Indica correctament la contrassenya.</p>
                      </div>
                  </div>
                  <div class="form-group">
                      <button type="submit" name="submit_sessio" class="btn btn-ses btn-primary btn-block btn-lg active rounded-pill">Log in</button>
                  </div>
                  <div class="clearfix">
                      <label class="float-left form-check-label"><input type="checkbox"> Recordar-me</label>
                      <a href="#" class="float-right">Has oblidat la contrassenya?</a>
                  </div>
              </form>
              <p class="text-center">Si no tens un compte...&ensp;&ensp;<a href="<?php echo BASE_URL ?>/index.php?action=registrar_usuari">Registra't!</a></p>
              <p class="text_dades_incorrectes"><?php echo $valor ?></p>
          </div>
        </div>


        <?php include __DIR__.'/../views/includes/footer.php'; ?>

        <!--Scripts particulars de la pagina-->
        <script src="<?php echo BASE_URL ?>/views/js/usuari.js"></script>
    </body>
</html>
