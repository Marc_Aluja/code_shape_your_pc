
<h5><span><?php echo count($valoracions); ?></span> valoracions per <span><?php echo $nom_prod ?></span></h5>
<?php foreach ($valoracions as $key => $valoracio):?>
  <div class="media mt-3 mb-4">
    <div class="media-body">
      <div class="d-sm-flex justify-content-between">
        <p class="mt-1 mb-2">
          <strong> <?php echo $usuaris[$key] ?> </strong>
          <span> – </span><span><?php echo $valoracio['data'] ?></span>
        </p>
        <ul class="rating mb-sm-0">
          <?php for($i = 0; $i < 5; $i++) {
            if($i < $valoracio['puntuacio']) { ?>
              <li>
                <i class="fas fa-star fa-sm"></i>
              </li>
            <?php } else { ?>
              <li>
                <i class="far fa-star fa-sm"></i>
              </li>
            <?php }
          } ?>
        </ul>
      </div>
      <p class="mb-0"><?php echo $valoracio["comentari"] ?></p>
    </div>
  </div>
<?php endforeach; ?>
