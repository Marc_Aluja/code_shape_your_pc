<!doctype html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title>Shape Your PC</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
              integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
              integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
        <link rel="stylesheet" href="<?php echo BASE_URL ?>/views/css/style_registre_perfil.css">
        <link rel="stylesheet" href="<?php echo BASE_URL ?>/views/css/style_footer.css">
        <link rel="stylesheet" href="<?php echo BASE_URL ?>/views/css/style_header.css">
    </head>
    <body>
        <?php if(isset($_SESSION['sessio_iniciada'])) {
                include __DIR__.'/../views/includes/header_sessio.php';
            } else {
                include __DIR__.'/../views/includes/header.php';
        } ?>

        <br/><br/>

        <div class="container cont-log">
          <div class="row">
            <div class="col-12 titol_registre">
              <h3 class="text-uppercase">Inici de sessió i perfil<h3>
              <hr class="linia_titol" />
            </div>
          </div>
          <br/>
          <div class="row">
            <form class="form-inline needs-validation col-12 justify-content-center" action="" method="post" novalidate>
              <fieldset class="col-10">
                <table class="taula-dades table table-bordered">
                <tbody>
                  <tr>
                    <td>
                      <div class="form-group mt-3">
                        <label class="col-md-4 control-label" for="nom">Nom: </label>
                        <div class="col-md-4">
                          <input id="nom" name="dades_nom" type="text" placeholder="ex: Marc Garcia" class="form-control input-md" size="50"
                                 pattern="^[A-Za-zÀ-ÿ ,.'-]+$" title="Sense caractes especials" value="<?php echo $dades['nom'] ?>" required>
                          <div class="invalid-feedback mt-2">
                            <p class="text_error ml-2">Indica correactament el nom d'usuari</p>
                          </div>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <div class="form-group mt-3">
                        <label class="col-md-4 control-label" for="email">Email: </label>
                        <div class="col-md-4">
                          <input id="email" name="dades_email" type="email" placeholder="ex: exemple@gmail.com" class="form-control input-md"
                                size="50" value="<?php echo $dades['email'] ?>" required="required">
                          <div class="invalid-feedback mt-2">
                            <p class="text_error ml-2">Indica correactament l'email</p>
                          </div>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <div class="form-group mt-3">
                        <label class="col-md-4 control-label" for="pass">Contrassenya: </label>
                        <div class="col-md-4">
                          <input id="pass" name="dades_contrassenya" type="password" placeholder="ex: algoexemple67" class="form-control input-md"
                                 pattern="[A-Za-z0-9]+" size="50">
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <div class="form-group mt-3">
                        <label class="col-md-4 control-label" for="rpass">Repeteix contrassenya: </label>
                        <div class="col-md-4">
                          <input id="rpass" name="dades_rcontrassenya" type="password" placeholder="" class="form-control input-md" size="50"
                                 pattern="[A-Za-z0-9]+">
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <div class="form-group mt-3">
                        <label class="col-md-4 control-label" for="direccio">Direccio: </label>
                        <div class="col-md-4">
                          <input id="direccio" name="dades_direccio" type="text" placeholder="ex: Plaça tormenta 56" class="form-control input-md"
                                 pattern="[A-Za-z0-9'\.\-\s\,]" size="50" value="<?php echo $dades['direccio'] ?>" required="required">
                          <div class="invalid-feedback mt-2">
                            <p class="text_error ml-2">Indica correactament la direcció</p>
                          </div>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <div class="form-group mt-3">
                        <label class="col-md-4 control-label" for="detalls_direccio">Detalls direccio: </label>
                        <div class="col-md-4">
                          <input id="detalls_direccio" name="dades_detalls_direccio" type="text" placeholder="ex: Plaça tormenta 56" class="form-control input-md"
                                 pattern="[A-Za-z0-9'\.\-\s\,]" size="50" value="<?php echo $dades['detall_dir'] ?>" required="required">
                          <div class="invalid-feedback mt-2">
                            <p class="text_error ml-2">Indica correactament el format dels detalls</p>
                          </div>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <div class="form-group mt-3">
                        <label class="col-md-4 control-label" for="pais">País: </label>
                        <div class="col-md-4">
                          <input id="pais" name="dades_pais" type="text" placeholder="ex: Espanya" class="form-control input-md"
                                 pattern="^(?:[A-Za-z]{2,}(?:(\.\s|'s\s|\s?-\s?|\s)?(?=[A-Za-z]+))){1,2}(?:[A-Za-z]+)?$" size="50"
                                 value="<?php echo $dades['pais'] ?>" required="required">
                          <div class="invalid-feedback mt-2">
                            <p class="text_error ml-2">Indica correactament el país de residencia</p>
                          </div>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <div class="form-group mt-3">
                        <label class="col-md-4 control-label" for="provincia">Província: </label>
                        <div class="col-md-4">
                          <input id="provincia" name="dades_provincia" type="text" placeholder="ex: Almeria" class="form-control input-md"
                                 pattern="^(?:[A-Za-z]{2,}(?:(\.\s|'s\s|\s?-\s?|\s)?(?=[A-Za-z]+))){1,2}(?:[A-Za-z]+)?$" size="50"
                                 value="<?php echo $dades['provincia'] ?>" required="required">
                          <div class="invalid-feedback mt-2">
                            <p class="text_error ml-2">Indica correactament la província de residencia</p>
                          </div>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <div class="form-group mt-3">
                        <label class="col-md-4 control-label" for="ciutat">Ciutat: </label>
                        <div class="col-md-4">
                          <input id="ciutat" name="dades_ciutat" type="text" placeholder="ex: Barcelona" class="form-control input-md"
                                 pattern="^(?:[A-Za-z]{2,}(?:(\.\s|'s\s|\s?-\s?|\s)?(?=[A-Za-z]+))){1,2}(?:[A-Za-z]+)?$" size="50"
                                 value="<?php echo $dades['ciutat'] ?>" required="required">
                          <div class="invalid-feedback mt-2">
                            <p class="text_error ml-2">Indica correactament la ciutat de residencia</p>
                          </div>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <div class="form-group mt-3">
                        <label class="col-md-4 control-label" for="zip">Codi postal: </label>
                        <div class="col-md-4">
                          <input id="zip" name="dades_cp" type="text" placeholder="ex: 09067" class="form-control input-md" size="50"
                                 pattern="[0-9]{5}" value="<?php echo $dades['codi_postal'] ?>" required="required">
                          <div class="invalid-feedback mt-2">
                            <p class="text_error ml-2">Indica correactament el codi postal</p>
                          </div>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <div class="form-group mt-3">
                        <label class="col-md-4 control-label" for="telefon">Telèfon: </label>
                        <div class="col-md-4">
                          <input id="telefon" name="dades_telefon" type="text" placeholder="ex: 874532211" class="form-control input-md"
                                 pattern="[0-9]{9}" size="50" value="<?php echo $dades['telefon'] ?>" required="required">
                          <div class="invalid-feedback mt-2">
                            <p class="text_error ml-2">Indica correactament el telèfon</p>
                          </div>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <div class="row justify-content-between">
                        <div class="mt-3 ml-5 col-3">
                          <a id="submit" href="index.php?action=eliminar_perfil" class="btn btn-primary btn-lg active rounded">Eliminar compte</a>
                        </div>
                        <div class="mt-3 col-3">
                          <button id="submit" name="submit_editar" class="btn btn-primary btn-lg active rounded ml-5">Guardar</button>
                          <p class="text_informe_edit mt-3"><?php echo $missatge ?></p>
                        </div>
                      </div>
                    </td>
                  </tr>
                </tbody>
              </table>
              </fieldset>
            </form>
          </div>
        </div>

        <br/><br/>

        <?php include __DIR__.'/../views/includes/footer.php'; ?>

        <!--Scripts particulars de la pagina-->
        <script src="<?php echo BASE_URL ?>/views/js/usuari.js"></script>
    </body>
</html>
