<!-- Modal -->
<div data-toggle="modal" data-target="#dialeg_notificacio" class="modal fade" id="dialeg_notificacio"
    tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Notificació</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="text-avis-dialeg modal-body">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">D'acord</button>
      </div>
    </div>
  </div>
</div>

<footer class="bg-light">
  <div class="container-fluid pt-3 contain_foot">
    <div class="Logo"><img class="w-75" src="<?php echo BASE_URL ?>/imatges/estils/logo.png" alt="algo"/></div>
    <div id="Contacte" class="mr-4">
        <p class="text_supe">Contactens!</p>
        <p class="text_infe"><a href="mailto:help@shapeyourpc.com">Email: help@shapeyourpc.com</a></p>
        <p class="text_infe">Telèfon: 234567845</p>
    </div>
    <div id="Informacio" class="mr-4">
        <p class="text_supe">Informació</p>
        <p class="text_infe"><a href="<?php echo BASE_URL ?>/index.php?action=mostrar_informacio&opcio=1">Política de privacitat</a></p>
        <p class="text_infe"><a href="<?php echo BASE_URL ?>/index.php?action=mostrar_informacio&opcio=2">Termes de servei</a></p>
        <p class="text_infe"><a href="<?php echo BASE_URL ?>/index.php?action=mostrar_informacio&opcio=3">Política d'enviaments</a></p>
    </div>
    <div id="Xarxes" class="mr-4">
      <p id="text_supe">Segueix-nos a:</p> <i class="fab fa-facebook-square social fa-2x"></i> <i class="fab fa-linkedin social fa-2x"></i>
                                      <i class="fab fa-twitter-square social fa-2x"></i>
    </div>
    <div id="subscripcio" class="mr-4">
        <p id="text_supe">Subscriu-te i estigues atent a noves notícies:</p>
        <div class="input-group mb-3">
          <div id="noti-subscripcio" class="input-group-append">
            <input type="text" class="form-control email-subscripcio" placeholder="Introdueix email" aria-label="Recipient's username"
                   aria-describedby="button-addon2">
          </div>
          <div class="input-group-append">
            <button class="btn my-btn btn-outline-secondary submit-subscripcio" type="button" id="button-addon2">Subscriu-me</button>
          </div>
        </div>
    </div>
  </div>

  <div class="text-center p-2 copyright">
    © 2020 Copyright:
    <a class="text-dark" href="#">ShapeYourPC drets reservats</a>
  </div>
</footer>

<!-- Scripts comuns per totes les pàgines-->
<!-- Load bootstrap scripts -->
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.5.0.js"></script>
<script src="<?php echo BASE_URL ?>/views/js/general.js"></script>
<!-- Load React. -->
<!-- Note: when deploying, replace "development.js" with "production.min.js". -->
<script src="https://unpkg.com/react@17/umd/react.development.js" crossorigin></script>
<script src="https://unpkg.com/react-dom@17/umd/react-dom.development.js" crossorigin></script>
<script src="https://unpkg.com/babel-standalone@6/babel.min.js"></script>
<!-- Load our React component. -->
<script src="<?php echo BASE_URL ?>/views/js/buscar_prod.js" type="text/babel"></script>
