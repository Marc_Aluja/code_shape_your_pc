<header>
  <div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 border-bottom shadow appbar_my">
    <div class="my-0 mr-md-auto"><a href="<?php echo BASE_URL ?>/index.php?accion="><img id="logo"
         src="<?php echo BASE_URL ?>/imatges/estils/logo.png" alt="algo"/></a></div>
    <form id="form-search" class="form-inline my-2 my-lg-0 mr-5">

    </form>
    <nav id="nav-head user-cart" class="nav-user-cart ml-5 my-2 my-md-0 mr-md-3 menu">
      <ul class="llista_opcions">
        <li>
           <a class="titol_opcio mr-3 text-white" href="<?php echo BASE_URL ?>/index.php?action=''">
             <i class="fas fa-user"></i>&ensp;&ensp;Hola, <?php echo explode(" ", $_SESSION['usuari']['nom'])[0]; ?></a>
            <ul class="dropdown">
              <li><a class="item_amagat" href="<?php echo BASE_URL ?>/index.php?action=mostrar_wishlist">Wishlist</a></li>
              <li><a class="item_amagat" href="<?php echo BASE_URL ?>/index.php?action=editar_perfil">Perfil</a></li>
              <li><a class="item_amagat" href="<?php echo BASE_URL ?>/index.php?action=mostrar_ordres">Ordres</a></li>
              <li><a class="item_amagat" href="<?php echo BASE_URL ?>/index.php?action=tancar_sessio">Tancar sessió</a></li>
            </ul>
        </li>
        <li>
          <a id="carret" data-avis="<?php echo $_SESSION['carret']['avis']?>"
             class="titol_opcio text-white" href="<?php echo BASE_URL ?>/index.php?action=mostrar_carret">
            <i class="fas fa-shopping-cart fa-1x"></i>&ensp;&ensp;Carret(<?php echo $_SESSION['carret']['quantitat']?>)</a>
        </li>
      </ul>
    </nav>

  </div>
</header>
