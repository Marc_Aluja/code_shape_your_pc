<!doctype html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title>Shape Your PC</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
              integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
              integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css">
        <link rel="stylesheet" href="<?php echo BASE_URL ?>/views/css/style_carret.css">
        <link rel="stylesheet" href="<?php echo BASE_URL ?>/views/css/style_footer.css">
        <link rel="stylesheet" href="<?php echo BASE_URL ?>/views/css/style_header.css">
    </head>
    <body>
        <?php if(isset($_SESSION['sessio_iniciada'])) {
                include __DIR__.'/../views/includes/header_sessio.php';;
            } else {
                include __DIR__.'/../views/includes/header.php';
        } ?>

        <br/><br/>

        <div class="container">
          <div class="row">
              <div class="col-12">
                  <h3 class="text-uppercase">Carret</h3>
                  <hr class="linia_titol" />
              </div>
          </div>
          <div class="card mt-5">
              <div class="row justify-content-md-center">
                  <div class="col-md-8 cart">
                      <div class="row mt-2 border-bottom">
                          <div class="row main justify-content-around">
                              <div class="col pl-5 ml-5"><b>Item</b></div>
                              <div class="col pl-5 ml-5"><b>Quantitat</b></div>
                              <div class="col pl-5"><b>Preu</b></div>
                          </div>
                      </div>
                      <?php foreach ($productes_carret as $producte):?>
                      <div class="row mt-2 border-bottom">
                          <div class="row main align-items-center">
                              <?php $imatge = explode(" ", $producte['imatge']); ?>
                              <div class="col-2"><img class="img-prod img-fluid" src="<?php echo BASE_URL ?><?php echo $imatge[0] ?>"></div>
                              <div class="col mr-5">
                                  <div class="row text-muted"><?php echo $producte['nom_cat'] ?></div>
                                  <div class="row"><?php echo $producte['nom'] ?></div>
                              </div>
                              <div class="col pr-5 qty">
                                <span id="<?php echo $producte['id_prod'] ?>" class="minus bg-dark noSelect">-</span>
                                <input type="text" class="count contador-quant-<?php echo $producte['id_prod'] ?>" name="qty"
                                       value="<?php echo $_SESSION['carret']['productes'][$producte['id_prod']]['quantitat'] ?>" size="1"
                                       data-stock="<?php echo $producte['stock']?>" disabled>
                                <span id="<?php echo $producte['id_prod'] ?>" class="plus bg-dark noSelect">+</span>
                              </div>
                              <div id="preu-<?php echo $producte['id_prod'] ?>"
                                data-preu="<?php echo $producte['preu'] ?>" class="col">&euro; <?php echo $producte['preu'] ?>
                                <span id="<?php echo $producte['id_prod'] ?>" class="close">&#10005;</span>
                              </div>
                          </div>
                      </div>
                    <?php endforeach; ?>
                      <br/><br/>
                  </div>
                  <div class="col-md-4 resum">
                      <div>
                          <h5><b>Resum (<span class="items-resum"><?php echo $_SESSION['carret']['quantitat'] ?></span> items)</b></h5>
                      </div>
                      <hr>
                      <div class="row justify-content-md-center px-2">
                          <div class="col"><b>Subtotal items</b></div>
                          <div class="col text-right">&euro; <?php echo $preu_carret ?></div>
                      </div>
                      <div class="row justify-content-md-center px-2 py-3">
                        <div class="col"><b>Enviament</b></div>
                        <div class="col text-right">&euro; 3.00</div>
                      </div>
                      <hr>
                      <div class="row justify-content-md-center px-2">
                          <div class="col"><b>Preu total</b></div>
                          <div class="col text-right">&euro; <?php echo ($preu_carret + 3) ?></div>
                      </div>

                      <?php if(isset($_SESSION['usuari']['user_id'])) { ?>
                          <?php if(count($_SESSION['carret']['ids_productes']) > 0) { ?>
                            <button class="btn my-btn btn-block mt-5 btn-comprar">CHECKOUT</button>
                          <?php } else { ?>
                            <button class="btn my-btn btn-block mt-5 btn-comprar" disabled>CHECKOUT</button>
                          <?php } ?>
                      <?php } else { ?>
                          <button class="btn my-btn btn-block mt-5" disabled>CHECKOUT</button>
                          <p class="mt-2">Iniciar sessió per activar la possibilitat de compra</p>
                      <?php } ?>

                  </div>
              </div>
          </div>
        </div>

        <br/><br/>

        <?php include __DIR__.'/../views/includes/footer.php'; ?>

        <!--Scripts particulars de la pagina-->
        <script src="<?php echo BASE_URL ?>/views/js/carret.js"></script>
    </body>
</html>
