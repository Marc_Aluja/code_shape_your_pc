<!doctype html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title>Shape Your PC</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
              integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
              integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
              <link rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" />
        <link rel="stylesheet" href="<?php echo BASE_URL ?>/views/css/style_ordres.css">
        <link rel="stylesheet" href="<?php echo BASE_URL ?>/views/css/style_footer.css">
        <link rel="stylesheet" href="<?php echo BASE_URL ?>/views/css/style_header.css">
    </head>
    <body>
        <?php if(isset($_SESSION['sessio_iniciada'])) {
                include __DIR__.'/../views/includes/header_sessio.php';;
            } else {
                include __DIR__.'/../views/includes/header.php';
        } ?>

        <br/><br/>

        <div class="container">
          <div class="row">
            <div class="col-12 titol_super">
              <h3 class="text-uppercase">Historial ordres<h3>
              <hr class="linia_titol" />
            </div>
          </div>
          <div class="container cont-orders">
            <div class="row mt-4">
              <div class="col-lg-12">
                <?php foreach ($ordres as $ordre):?>
                  <div class="card mb-5">
                    <h5 class="card-header">Ordre Nº XGBQW-JUTYH-<?php echo $ordre['id_ordre'] ?></h5>
                    <div class="card-body">
                      <div class="row">
                        <div class="col-8">
                          <div class="row main justify-content-start">
                              <div class="col-4"><b>Data ordre</b></div>
                              <div class="col-4"><b>Quantitat</b></div>
                              <div class="col-4"><b>Total</b></div>
                          </div>
                          <div class="row main justify-content-start">
                              <div class="col-4"><?php echo $ordre['data'] ?></div>
                              <div class="col-4"><?php echo $ordre['quantitat'] ?> productes</div>
                              <div class="col-4">&euro; <?php echo $ordre['preu'] ?></div>
                          </div>
                        </div>
                        <div class="col-4">
                          <div class="row">
                            <div class="col-12">
                                <button id="<?php echo $ordre['id_ordre'] ?>" type="button" class="btn btn-info btn-rounded btn-detalls mr-4">Detalls</button>
                                <button id="<?php echo $ordre['id_ordre'] ?>" type="button" class="btn btn-danger btn-rounded ml-2 btn-cancelar">Cancel·lar ordre</button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                <?php endforeach; ?>
              </div>
            </div>
          </div>
        </div>

        <br/><br/>

        <?php include __DIR__.'/../views/includes/footer.php'; ?>

        <!--Scripts particulars de la pagina-->
        <script src="<?php echo BASE_URL ?>/views/js/ordres.js"></script>

        <?php if(isset($_SESSION['avis_cancelat'])) {
          echo $_SESSION['avis_cancelat'];
          unset($_SESSION['avis_cancelat']);
        } ?>
    </body>
</html>
