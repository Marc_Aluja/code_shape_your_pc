
<?php if(isset($_SESSION['sessio_iniciada'])) { ?>
      <ul class="llista_opcions">
        <li>
           <a class="titol_opcio mr-3 text-white" href="<?php echo BASE_URL ?>/index.php?action=''">
             <i class="fas fa-user"></i>&ensp;&ensp;Hola, <?php echo explode(" ", $_SESSION['usuari']['nom'])[0]; ?></a>
            <ul class="dropdown">
              <li><a class="item_amagat" href="<?php echo BASE_URL ?>/index.php?action=mostrar_wishlist">Wishlist</a></li>
              <li><a class="item_amagat" href="<?php echo BASE_URL ?>/index.php?action=editar_perfil">Perfil</a></li>
              <li><a class="item_amagat" href="<?php echo BASE_URL ?>/index.php?action=mostrar_ordres">ordres</a></li>
              <li><a class="item_amagat" href="<?php echo BASE_URL ?>/index.php?action=tancar_sessio">Tancar sessió</a></li>
            </ul>
        </li>
        <li>
          <a id="carret" data-avis="<?php echo $_SESSION['carret']['avis']?>"
             class="titol_opcio text-white" href="<?php echo BASE_URL ?>/index.php?action=mostrar_carret">
            <i class="fas fa-shopping-cart fa-1x"></i>&ensp;&ensp;Carret(<?php echo $_SESSION['carret']['quantitat']?>)</a>
        </li>
      </ul>
<?php    } else { ?>
      <ul class="llista_opcions">
        <li>
           <a class="titol_opcio mr-3 text-white" href="<?php echo BASE_URL ?>/index.php?action=iniciar_sessio">
             <i class="fas fa-user"></i>&ensp;&ensp;Compte</a>
        </li>
        <li>
          <a id="carret" data-avis="<?php echo $_SESSION['carret']['avis']?>" class="titol_opcio text-white"
            href="<?php echo BASE_URL ?>/index.php?action=mostrar_carret"><i class="fas fa-shopping-cart fa-1x">
              </i>&ensp;&ensp;Carret(<?php echo $_SESSION['carret']['quantitat']?>)</a>
        </li>
      </ul>
<?php } ?>
