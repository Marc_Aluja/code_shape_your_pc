<!doctype html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title>Shape Your PC</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
              integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
              integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
        <link rel="stylesheet" href="<?php echo BASE_URL ?>/views/css/style_inici.css">
        <link rel="stylesheet" href="<?php echo BASE_URL ?>/views/css/style_footer.css">
        <link rel="stylesheet" href="<?php echo BASE_URL ?>/views/css/style_header.css">
    </head>
    <body>
        <?php if(isset($_SESSION['sessio_iniciada'])) {
                include __DIR__.'/../views/includes/header_sessio.php';;
            } else {
                include __DIR__.'/../views/includes/header.php';
        } ?>

        <div class="container-fluid">
          <div id="my-carousel" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
              <div class="carousel-item active">
                <a href="index.php?action=mostrar_productes&nom=teclats&id=11">
                  <img class="d-block w-100" src="<?php echo BASE_URL ?>/imatges/estils/banner.png" alt="First slide">
                </a>
              </div>
              <div class="carousel-item">
                <a href="index.php?action=mostrar_productes&nom=auriculars&id=3">
                  <img class="d-block w-100" src="<?php echo BASE_URL ?>/imatges/estils/banner2.jpg" alt="Second slide">
                </a>
              </div>
            </div>
            <a class="carousel-control-prev" href="#my-carousel" role="button" data-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#my-carousel" role="button" data-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>
        </div>
        <br/>

        <div class="container-fluid">
          <div class="row justify-content-md-center">
            <div class="col-5">
              <h1 class="text-center titol_pagina">Personalitza el teu ordinador amb combinacions úniques!<h1>
              <hr class="linia_titol" />
            </div>
          </div>
          <br/><br/>
          <div class="row row-cols-4 justify-content-md-center">
            <?php foreach ($super_categories as $super_categoria):?>
            <div id="<?php echo $super_categoria['id_supercat'] ?>" class="col-8 col-lg-3 mb-5 ml-5 mr-5 super_categoria">
              <a class="clicable-super-categoria"
                 href="<?php echo BASE_URL ?>/index.php?action=mostrar_categories&nom=<?php echo $super_categoria['nom'] ?>&id=<?php echo $super_categoria['id_supercat'] ?>">
                <div class="card super_cat">
                  <img src="<?php echo BASE_URL ?><?php echo $super_categoria['imatge'] ?>" class="card-img-top" alt="...">
                  <div class="card-body">
                    <h5 class="card-title"><?php echo $super_categoria['nom'] ?></h5>
                    <p class="card-text"><?php echo $super_categoria['descripcio'] ?></p>
                  </div>
                </div>
              </a>
            </div>
            <?php endforeach; ?>
          </div>
        </div>

        <br/><br/>

        <?php include __DIR__.'/../views/includes/footer.php'; ?>

        <?php if(isset($_SESSION['avis_eliminat'])) {
          echo $_SESSION['avis_eliminat'];
          unset($_SESSION['avis_eliminat']);
        } ?>
    </body>
</html>
