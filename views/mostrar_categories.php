<!doctype html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title>Shape Your PC</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
              integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
              integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
        <link rel="stylesheet" href="<?php echo BASE_URL ?>/views/css/style_categories.css">
        <link rel="stylesheet" href="<?php echo BASE_URL ?>/views/css/style_footer.css">
        <link rel="stylesheet" href="<?php echo BASE_URL ?>/views/css/style_header.css">
    </head>
    <body>
        <?php if(isset($_SESSION['sessio_iniciada'])) {
                include __DIR__.'/../views/includes/header_sessio.php';;
            } else {
                include __DIR__.'/../views/includes/header.php';
        } ?>

        <br/><br/>

        <div class="container">
          <div class="row">
            <div class="col-12 titol_super">
              <h3><?php echo $nom_super_cat ?><h3>
              <hr class="linia_titol" />
            </div>
          </div>
          <br/>
          <div class="row justify-content-md-center">
            <?php foreach ($categories as $categoria):?>
            <a class="clicable-categoria"
               href="<?php echo BASE_URL ?>/index.php?action=mostrar_productes&nom=<?php echo $categoria['nom'] ?>&id=<?php echo $categoria['id_cat'] ?>">
              <div id="<?php echo $categoria['id_cat'] ?>" class="card mb-3 categoria">
                <img class="card-img-top" src="<?php echo BASE_URL ?><?php echo $categoria['imatge'] ?>" alt="Card image cap"
                     style="max-width:900px;max-height:200px;min-width:900px;min-height:200px;">
                <div class="card-body">
                  <h5 class="card-title titol_categoria"><?php echo $categoria['nom'] ?></h5>
                </div>
              </div>
            </a>
            <?php endforeach; ?>
          </div>
        </div>

        <br/><br/>

        <?php include __DIR__.'/../views/includes/footer.php'; ?>
    </body>
</html>
