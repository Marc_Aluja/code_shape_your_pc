<!doctype html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title>Shape Your PC</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
              integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
              integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css">
        <link rel="stylesheet" href="<?php echo BASE_URL ?>/views/css/style_detalls.css">
        <link rel="stylesheet" href="<?php echo BASE_URL ?>/views/css/style_footer.css">
        <link rel="stylesheet" href="<?php echo BASE_URL ?>/views/css/style_header.css">
    </head>
    <body>
        <?php if(isset($_SESSION['sessio_iniciada'])) {
                include __DIR__.'/../views/includes/header_sessio.php';;
            } else {
                include __DIR__.'/../views/includes/header.php';
        } ?>

        <br/><br/>

        <div class="container cont-det">
          <div class="row justify-content-md-center">
            <div id="<?php echo $carac_generals['id_prod'] ?>" class="taula_producte col-md-6">
                <?php $imatges = explode(" ", $carac_generals['imatge']); ?>
                <div class="pro-img-details">
                    <img src="<?php echo BASE_URL ?><?php echo $imatges[0] ?>" alt="">
                </div>
                <div class="pro-img-list">
                  <?php foreach ($imatges as $imatge): ?>
                    <a href="#">
                        <img id="imatge_llista" src="<?php echo BASE_URL ?><?php echo $imatge ?>" alt="">
                    </a>
                  <?php endforeach; ?>
                </div>
            </div>
            <div class="col-md-4">
                <h2 class="pro-d-title"><?php echo $carac_generals['nom'] ?></h2>
                <div class="product_meta mt-3">
                    <span class="posted_in"> <strong>Categoria: </strong><?php echo $carac_generals['nom_cat'] ?></span>
                </div>
                <div class="mt-2">
                  <span id="preu" class="pro-preu" data-preu="<?php echo $carac_generals['preu'] ?>"><?php echo $carac_generals['preu'] ?>€</span>
                </div>
                <div class="reviews_prod mt-2">
                  <ul class="rating">
                    <?php for($i = 1; $i <= 5; $i++) {
                      if($i <= round((float)$carac_generals['avg_puntuacio'])) { ?>
                          <li>
                            <i class="fas fa-star fa-sm"></i>
                          </li>
                    <?php } else { ?>
                          <li>
                            <i class="far fa-star fa-sm"></i>
                          </li>
                    <?php }
                    } ?>
                  </ul>
                  <a class="nova_opinio" href="#reviews-tab"><?php echo $carac_generals['n_valoracions'] ?> opinions</a>
                </div>
                <div class="product-stock">
                  <?php if($carac_generals['stock'] >= 1) { ?>
                      <p class="in_stock"><i class="bi bi-check-circle"></i> En Stock</p>
                  <?php } else { ?>
                      <p class="no_stock"><i class="bi bi-x-circle"></i> Sense Stock</p>
                  <?php } ?>
                </div>
                <div id="quantitat" class="form-group mt-2" data-stock="<?php echo $carac_generals['stock']?>">
                    <label>Quantitat:</label>
                    <div class="qty">
                      <span class="minus bg-dark noSelect">-</span>
                      <input type="text" class="count contador_quant" name="qty" value="1" size="1" disabled>
                      <span class="plus bg-dark noSelect">+</span>
                    </div>
                </div>
                <div class="col-12 cart_wish mt-4">
                  <form action="">
                    <button type="button" class="btn my-btn btn-block rounded afegir_carret"
                            <?php if($carac_generals['stock'] == '0') { ?> disabled <?php } ?>>
                            <i class="fa fa-shopping-cart fa-1x"></i> Afegir al Carret</button>
                    <?php if(isset($_SESSION['wishlist'])) { ?>
                      <button type="button" class="btn btn-block active rounded wish_btn">
                        <?php if(in_array($carac_generals['id_prod'], $_SESSION['wishlist'])) { ?>
                          <i class="fas fa-heart fa-1x wish-bold"></i><span> Afegir a preferits</span>
                        <?php } else { ?>
                          <i class="fas fa-heart fa-1x wish-no-bold"></i><span> Afegir a preferits</span>
                        <?php } ?>
                      </button>
                    <?php } else { ?>
                      <button type="button" class="btn btn-block active rounded wish_btn" disabled>
                      <i class="fas fa-heart fa-1x wish-no-bold"></i><span> Afegir a preferits</span></button>
                    <?php } ?>
                  </form>
                </div>
            </div>
          </div>
          <div class="row justify-content-md-center mt-5">
            <div class="classic-tabs border rounded px-4 pt-1 col-10">
              <ul class="nav tabs-primary nav-justified" id="advancedTab" role="tablist">
                <li class="nav-item">
                  <a class="nav-link seleccionat" id="description-tab" data-toggle="tab" href="#description" role="tab"
                     aria-controls="description">Característiques</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="reviews-tab" data-toggle="tab" href="#reviews" role="tab" aria-controls="reviews">Valoracions</a>
                </li>
              </ul>
              <div class="tab-content" id="advancedTabContent">
                <div class="col-12 mt-4 mb-4" id="description" role="tabpanel" aria-labelledby="description-tab">
                  <?php foreach ($detalls as $key => $value):?>
                    <?php $key = str_replace('_', ' ', $key);?>
                    <p class="titol_detall"><strong><?php echo $key ?>:</strong> <?php echo $value ?></p>
                  <?php endforeach; ?>
                </div>
                <div class="col-6 mt-4 mb-4" id="reviews" role="tabpanel" aria-labelledby="reviews-tab">
                  <div id="valoracions">

                  </div>
                  <hr>
                  <?php if(isset($_SESSION['sessio_iniciada'])) { ?>
                    <div id="afegir_valo">
                      <h5 class="mt-4">Afegeix una review</h5>
                      <div class="estrelles my-3">
                        <ul class="estrelles rating mb-0">
                          <li class="mr-3 text_puntuar">
                            <span>Puntuació:</span>
                          </li>
                          <li>
                            <i id="1" name="rating" class="far fa-star fa-sm"></i>
                          </li>
                          <li>
                            <i id="2" name="rating" class="far fa-star fa-sm"></i>
                          </li>
                          <li>
                            <i id="3" name="rating" class="far fa-star fa-sm"></i>
                          </li>
                          <li>
                            <i id="4" name="rating" class="far fa-star fa-sm"></i>
                          </li>
                          <li>
                            <i id="5" name="rating" class="far fa-star fa-sm"></i>
                          </li>
                          <li>
                            <span name="puntuacio"></span>
                          </li>
                        </ul>
                      </div>
                      <div>
                        <div class="md-form md-outline">
                          <textarea id="form76" name="comentari" class="md-textarea form-control pr-6" rows="4"></textarea>
                          <label for="form76">Comentari</label>
                        </div>
                        <div class="text-right pb-2">
                          <button id="boto_opinio" type="submit" class="btn my-btn btn-primary rounded">Afegeix valoració</button>
                        </div>
                      </div>
                    </div>
                  <?php } else { ?>
                    <p> Inicia sessió per poder valorar el producte </p>
                  <?php } ?>
                </div>
              </div>
            </div>
          </div>
        </div>

        <br/><br/>

        <?php include __DIR__.'/../views/includes/footer.php'; ?>

        <!--Scripts particulars de la pagina-->
        <script src="<?php echo BASE_URL ?>/views/js/detalls.js"></script>
    </body>
</html>
