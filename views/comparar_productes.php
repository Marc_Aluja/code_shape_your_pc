<!doctype html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title>Shape Your PC</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
              integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
              integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css">
        <link rel="stylesheet" href="<?php echo BASE_URL ?>/views/css/style_compare.css">
        <link rel="stylesheet" href="<?php echo BASE_URL ?>/views/css/style_footer.css">
        <link rel="stylesheet" href="<?php echo BASE_URL ?>/views/css/style_header.css">
    </head>
    <body>
        <?php if(isset($_SESSION['sessio_iniciada'])) {
                include __DIR__.'/../views/includes/header_sessio.php';;
            } else {
                include __DIR__.'/../views/includes/header.php';
        } ?>

        <br/><br/>

        <div class="container">
          <div class="row">
              <div class="col-12">
                  <h3 class="text-uppercase">Comparar productes</h3>
                  <hr class="linia_titol" />
              </div>
          </div>
          <div class="row mt-4">
            <div class="container pb-5 mb-2">
                <div class="taula-comparativa">
                  <table class="table table-bordered">
                      <thead class="bg-secundari">
                          <tr>
                              <td class="align-middle">
                              </td>
                              <?php foreach ($generals_productes as $general): ?>
                              <td>
                                  <div class="item-comparacio">
                                      <?php $imatge = explode(" ", $general['imatge']); ?>
                                      <img class="img-fluid w-75" src="<?php echo BASE_URL ?><?php echo $imatge[0] ?>" alt="">
                                  </div>
                              </td>
                              <?php endforeach; ?>
                          </tr>
                      </thead>
                      <tbody id="summary" data-filter="target">
                          <tr class="bg-secundari">
                              <th class="text-uppercase">Resum</th>
                              <?php foreach ($generals_productes as $general): ?>
                                <td><span class="text-dark font-weight-semibold"><?php echo $general['nom'] ?></span></td>
                              <?php endforeach; ?>
                          </tr>
                          <tr>
                              <th>Marca</th>
                              <?php foreach ($generals_productes as $general): ?>
                                <td><?php echo $general['marca'] ?></td>
                              <?php endforeach; ?>
                          </tr>
                          <tr>
                              <th>Preu</th>
                              <?php foreach ($generals_productes as $general): ?>
                                <td>&euro; <?php echo $general['preu'] ?></td>
                              <?php endforeach; ?>
                          </tr>
                          <tr>
                              <th>Valoracio</th>
                              <?php foreach ($generals_productes as $general): ?>
                                <td>
                                  <ul class="rating">
                                  <?php for($i = 1; $i <= 5; $i++) {
                                    if($i <= round($general['avg_puntuacio'])) { ?>
                                        <li>
                                          <i class="fas fa-star fa-sm"></i>
                                        </li>
                                  <?php } else { ?>
                                        <li>
                                          <i class="far fa-star fa-sm"></i>
                                        </li>
                                  <?php }
                                  } ?>
                                  </ul>
                                </td>
                              <?php endforeach; ?>
                          </tr>
                      </tbody>
                      <tbody id="general" data-filter="target">
                          <tr class="bg-secundari">
                              <th class="text-uppercase">Detalls</th>
                              <?php foreach ($generals_productes as $general): ?>
                                <td><span class="text-dark font-weight-semibold"><?php echo $general['nom'] ?></span></td>
                              <?php endforeach; ?>
                          </tr>
                          <?php foreach ($detalls_keys as $key): ?>
                          <tr>
                              <th class="titol_detall"><?php echo $key ?></th>
                              <?php foreach ($detalls_productes as $detall_producte): ?>
                              <td><?php echo $detall_producte[$key] ?></td>
                              <?php endforeach; ?>
                          </tr>
                          <?php endforeach; ?>
                          <tr>
                              <th></th>
                              <?php foreach ($generals_productes as $general): ?>
                              <td>
                                  <button id="<?php echo $general['id_prod']?>" type="button" class="btn afegir_carret btn-outline-primary btn-block"
                                          <?php if($general['stock'] == '0') { ?> disabled <?php } ?>
                                          data-preu="<?php echo $general['preu']?>" data-stock="<?php echo $general['stock']?>">
                                          <i class="fa fa-shopping-cart fa-1x"></i> Afegir al Carret
                                  </button>
                              </td>
                              <?php endforeach; ?>
                          </tr>
                      </tbody>
                  </table>
              </div>
            </div>
          </div>

        </div>

        <br/><br/>

        <?php include __DIR__.'/../views/includes/footer.php'; ?>

        <!--Scripts particulars de la pagina-->
        <script src="<?php echo BASE_URL ?>/views/js/comparar.js"></script>
    </body>
</html>
