<!doctype html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title>Shape Your PC</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
              integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
              integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
        <link rel="stylesheet" href="<?php echo BASE_URL ?>/views/css/style_info.css">
        <link rel="stylesheet" href="<?php echo BASE_URL ?>/views/css/style_footer.css">
        <link rel="stylesheet" href="<?php echo BASE_URL ?>/views/css/style_header.css">
    </head>
    <body>
        <?php if(isset($_SESSION['sessio_iniciada'])) {
                include __DIR__.'/../views/includes/header_sessio.php';;
            } else {
                include __DIR__.'/../views/includes/header.php';
        } ?>

        <br/><br/>

        <div class="container">
          <div class="row">
            <div class="col-12 titol_super">
              <h3 class="text-uppercase">Política de privacitat<h3>
              <hr class="linia_titol" />
            </div>
          </div>
          <div class="container">
            <div class="my-paragraph my-paragraph">
                <h4>Informació Recollida</h4>
                <p>
                  El nostre lloc recopila informació sobre noms de domini, adreça IP (adreça IP estàtica o dinàmica que de vegades apunten
                  a un ordinador d'un particular o dispositiu identificable), tipus de navegador, sistema operatiu, temps d'accés i adreces
                  del lloc web referenciat/sortint. Aquesta informació l'utilitza ShareYourPC. Amb finalitats operatives, per mantenir la
                  qualitat del servei i per fer un seguiment de les tendències i estadístiques del lloc.
                  La major part del temps, aquesta informació no l'identifica personalment.
                </p>
                <p>
                  També recollim tota la informació personal que escolliu proporcionar quan utilitzeu aquest espai.
                  Això pot incloure, però no es limita als esdeveniments quan es tracta de crear un compte, ordre i/o productes de descàrrega,
                  contactar amb el nostre departament de vendes, publicar continguts als nostres fòrums o interactuar amb altres àrees del nostre
                  centre de suport. Aquesta informació de caràcter personal pot incloure, el vostre nom, l'adreça de correu electrònic i qualsevol
                  altra informació seleccionada per compartir.
                  Podem recopilar qualsevol informació personal addicional que proporcioneu de manera conscient i voluntària a través d'aquest
                  desplaçament, contacte o interacció, en casos en què envieu contingut als fòrums, interactueu amb altres àrees del nostre centre
                  de suport, o poseu-vos en contacte amb el nostre departament de vendes o equips de suport, en cas de registrar-vos per qualsevol
                  ShareYourPC: butlletí, Webinar, enquesta, una altra comunicació o només vols fer contacte general amb nosaltres, recollirem
                  informació del teu contacte per satisfer la teva sol·licitud.
                  En cas que vulgueu donar-vos de baixa, seguiu l'enllaç de baixa que apareix al final de cada comunicació o, podeu donar-vos de
                  baixa a través de les vostres preferències de compte.
                </p>
            </div>
            <div class="my-paragraph my-paragraph">
                <h4>Utilitzar, compartir o revelar informació personal</h4>
                <p>
                  Si us plau, tingueu en compte que no venem ni lloem la vostra informació personal a cap tercer.
                  El propòsit principal per al qual usem el vostre personal és comunicar-vos de la forma més eficient.
                  Podem compartir, revelar o proporcionar la vostra informació personal amb o a un tercer:
                  (i) en els casos en què intentem cobrar un pagament o deute,
                  (ii) quan se'ns exigeix lluitar contra el frau o protegir els nostres interessos,
                  (iii) per tal de fer complir els termes d'ús d'aquest lloc o d'un altre ShapeYourPC. Llocs,
                  (iv) en els casos en què se'ns exigeix que ho fem per llei o per a respondre a un procés legal o a peticions legals,
                  incloent-hi l'aplicació de la llei o les agències governamentals, (v) dins i entre les nostres filials i filials,
                  o (vi) com a part d'una fusió o venda d'un negoci.
                  Podem utilitzar o compartir la seva informació personal per a repartir correspondència,
                  comunicacions o serveis, com ara butlletins, esdeveniments, entrenament o programari que sol·liciteu o compreu,
                  A més de fer-vos una notificació sobre l'estat de la vostra ordre. Qualsevol informació que hagi proporcionat
                  Nosaltres podem utilitzar-los per a donar-los suport als clients. Tingueu en compte que aquests tercers són
                  autoritzada a utilitzar la seva informació personal només amb l'únic propòsit de proporcionar-nos aquests serveis.
                </p>
            </div>
            <div class="my-paragraph my-paragraph">
                <h4>Ús de Cookies</h4>
                <p>
                  Les galetes les utilitzem de tant en tant. A la seva natura, les galetes són fitxers de text petits
                  pot ser utilitzat per un lloc web per identificar/reconèixer un visitant repetit al lloc web.
                  Les galetes poden ser utilitzades per nosaltres per raons variables, incloent-hi:
                </p>
                <p>
                  <ul class="priv">
                      <li>Per facilitar la navegació del nostre lloc web</li>
                      <li>Per ajudar al procés de registre i entrada dels nostres serveis</li>
                      <li>Per a personalitzar l'experiència</li>
                      <li>Per a personalitzar l'experiència</li>
                      <li>Per tal de proporcionar característiques de compartició social de ginys i vídeos del lloc web</li>
                      <li>Poder mesurar l'eficàcia de la publicitat i les promocions</li>
                  </ul>
                </p>
                <p>
                  En cas que no desitgeu ShapeYourPC. per desplegar galetes al vostre navegador,
                  podeu sortir canviant la configuració del navegador per rebutjar galetes
                  o per notificar-vos quan un lloc web intenta posar una galeta al vostre programari del navegador.
                  Si trieu desactivar les galetes al vostre navegador, el nostre espai encara pot ser utilitzat per vós
                  però la vostra capacitat d'utilitzar alguns dels productes i/o serveis d'aquest lloc pot estar afectada i la funcionalitat del
                  lloc complet no està garantit.
                </p>
            </div>
            <div class="my-paragraph my-paragraph">
                <h4>Seguretat de la Informació</h4>
                <p>
                  Ens assegurem que es prenguin les mesures de seguretat adequades per protegir la vostra informació personal de l'accés no autoritzat,
                  modificació, ús o divulgació. Entre aquestes mesures hi ha una varietat de tecnologies i procediments de seguretat
                  Igual que les revisions internes de la recollida de dades, les pràctiques relatives a l'emmagatzematge i el processament i
                  també les mesures de seguretat i mesures adequades de xifratge i seguretat física. Utilitzem certificats Secure Socket Layer (SSL)
                  per xifrar el transmissió de dades al ShapeYourPC. pàgines de pagament i d'inici de sessió. Tingueu en compte que no hi ha
                  transmissió a través d'Internet per tant, pot estar plenament segur, per part seva, també és responsable d'adoptar les mesures
                  necessàries per a la protecció de la vostra informació personal a partir d'accés no autoritzat, ús o revelació, inclosa la protecció
                  de la vostra contrasenya.
                  A part de les indicacions sota "Usa, Compartir o Desconnexió d'informació personal",
                  L'accés a la vostra informació personal està restringit només a ShapeYourPC. empleats,
                  contractistes i agents que necessiten saber aquesta informació perquè puguin processar
                  i proporciona-te suport personalitzat al client. Tingueu en compte que aquests individus estan lligats per
                  obligacions estrictes de confidencialitat i poden estar subjectes a disciplina, inclosa la terminació en
                  Si no compleixen aquestes complicacions.
                </p>
            </div>
            <div class="my-paragraph my-paragraph">
                <h4>Processament de targetes de crèdit</h4>
                <p>
                  Un proveïdor de serveis de tercers és utilitzat per a gestionar el processament de targetes de crèdit.
                  Aquest proveïdor de serveis no té permís per emmagatzemar, retenir,
                  o utilitzar la seva informació personal, excepte per a l'únic propòsit del processament de targetes de crèdit en el nostre nom.
                </p>
            </div>
            <div class="my-paragraph my-paragraph">
                <h4>Revisió de dades personals</h4>
                <p>
                    ShareYourPC conserva dades personals mínimes que són raonables i necessàries en la gestió i el manteniment d'un producte
                    proveïdor a relació de client.
                    Els clients de ShapeYourPC poden revisar i actualitzar el seu perfil i informació de subscripció al client del lloc
                    web de ShareYourPC.
                </p>
                <p>
                  Si teniu problemes per revisar les vostres dades personals, envieu un correu a
                  <a href="mailto:help@shapeyourpc.com">help@shapeyourpc.com</a> i indiqueu la vostra
                  necessitat i informació de contacte per obtenir ajuda.
                </p>
            </div>
            <div class="my-paragraph my-paragraph">
                <h4>Retenció i supressió de dades personals</h4>
                <p>
                  ShareYourPC conservarà les vostres dades personals mentre el vostre compte estigui actiu;
                  segons sigui necessari per proporcionar-vos productes o serveis;
                  tal com s'indica en l'acord sobre els termes d'ús; segons sigui necessari per als propòsits descrits en aquesta política o
                  en el moment de la recollida;
                  si cal complir amb les nostres obligacions legals, complir les sol·licituds de SUBSCRIPCIÓ, complir els requisits d'auditoria
                  financera i fer complir els nostres acords o fins a quin punt està permès per la llei.
                </p>
                <p>
                  Si se sol·licita, ShapeYourPC suprimirà les vostres dades personals d'una manera que garanteixi que la vostra identificació
                  no es pot reconstruir o llegir.
                  Una sol·licitud de supressió no pot resultar en la supressió completa de les vostres dades.
                  La informació de compra es mantindrà amb finalitats legals i d'auditoria financera.
                  Les trameses dels fòrums es conservaran segons l'acord de termes d'ús i se suprimirà el vostre nom.
                  Si voleu eliminar les vostres dades personals, envieu un correu a <a href="mailto:help@shapeyourpc.com">help@shapeyourpc.com</a>
                  amb el Subjecte: Elimina les meves dades personals" i declara explícitament la teva necessitat i informació de contacte.
                </p>
            </div>
            <div class="my-paragraph my-paragraph">
                <h4>Transferència de dades personals, processament i emmagatzematge</h4>
                <p>
                  Les dades personals que emmagatzemen no són molt sensibles i són necessàries per a dur a terme pràctiques comercials normals
                  i esperades, especialment, les dades personals, tal com es descriu en aquesta política, s'emmagatzemen en servidors de dades
                  basats en els EUA o en serveis d'emmagatzematge en núvol gestionats per empreses estatunidenques i internacionals.
                  Nosaltres i totes les empreses de tercers amb les quals ens comprometem han assumit el compromís de complir la normativa sobre
                  privacitat dels GDPR de la UE de 2018 per als residents de la UE.
                  Basat en la definició de base legal, tal com es defineix en el Reglament sobre PIB de la UE, l'ús i l'emmagatzematge de dades
                  personals de clients mínims, la base d'interessos legítims és la base jurídica adequada per a l'emmagatzematge i ús de dades.
                  Es tracta d'un ús raonable i esperat de dades de clients i clients potencials.
                </p>
            </div>
          </div>
        </div>

        <br/><br/>

        <?php include __DIR__.'/../views/includes/footer.php'; ?>

    </body>
</html>
