<!doctype html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title>Shape Your PC</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
              integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
              integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
        <link rel="stylesheet" href="<?php echo BASE_URL ?>/views/css/style_info.css">
        <link rel="stylesheet" href="<?php echo BASE_URL ?>/views/css/style_footer.css">
        <link rel="stylesheet" href="<?php echo BASE_URL ?>/views/css/style_header.css">
    </head>
    <body>
        <?php if(isset($_SESSION['sessio_iniciada'])) {
                include __DIR__.'/../views/includes/header_sessio.php';;
            } else {
                include __DIR__.'/../views/includes/header.php';
        } ?>

        <br/><br/>

        <div class="container">
          <div class="row">
            <div class="col-12 titol_super">
              <h3 class="text-uppercase">Política d'enviaments<h3>
              <hr class="linia_titol" />
            </div>
          </div>
          <div class="container">
            <div class="my-paragraph my-paragraph">
                <h4>Temps de processament del lliurament</h4>
                <p>
                  Totes les ordres es processen en 2-3 dies de negoci. Les ordres no s'envien o s'envien caps de setmana o festius.
                </p>
                <p>
                  Si estem experimentant un alt volum de ordres, els enviaments poden retardar-se uns dies. Si us plau permet dies
                  addicionals en trànsit per al lliurament. Si hi haurà un retard significatiu en l'enviament ordre, ens posarem en contacte
                  amb tu per correu electrònic o telèfon.
                </p>
                <p>
                  Actualment no enviem fora d'Espanyas.
                </p>
            </div>
            <div class="my-paragraph my-paragraph">
                <h4>Tarifes d'enviament & estimacions de lliurament</h4>
                <p>
                  Les comissions d'enviament per a la vostra ordre es calcularan i es mostraran en el pagament
                </p>
                <p>
                  <table class="" style="width:100%">
                    <tr>
                      <th>Mètode d'enviament</th>
                      <th>Temps d'entrega estimat</th>
                      <th>Cost d'enviament</th>
                    </tr>
                    <tr>
                      <td>FedEx Standard</td>
                      <td>3-5 dies de treball</td>
                      <td>3€</td>
                    </tr>
                  </table>
                </p>
                <p>
                  Els retards de lliurament poden ocórrer de tant en tant.
                </p>
            </div>
            <div class="my-paragraph my-paragraph">
                <h4>Enviament a capses P.O. o adreces APO/FPO</h4>
                <p>
                  ShapeYourPC es distribueix per adreces dins dels Territoris dels EUA, EUA i APO/FPO/DPO adreces.
                </p>
            </div>
            <div class="my-paragraph my-paragraph">
                <h4>Confirmació de lliurament Seguiment de ordres</h4>
                <p>
                  Rebreu un correu de confirmació de lliurament una vegada que la ordre hagi estat enviada que contingui el vostre
                  missatge número de seguiment. El número de seguiment estarà actiu en 24 hores.
                </p>
            </div>
            <div class="my-paragraph my-paragraph">
                <h4>Duanes, aranzels i impostos</h4>
                <p>
                  ShapeYourPC no és responsable de cap duana i impostos aplicats a la teva ordre. Totes les taxes
                  imposades durant o després del transport són responsabilitat del client (aranzels, impostos, etc.).
                </p>
            </div>
            <div class="my-paragraph my-paragraph">
                <h4>Danys</h4>
                <p>
                  ShapeYourPC no és responsable de cap producte danyat o perdut durant l'enviament. Si tu la ordre
                  ha estat malmesa, poseu-vos en contacte amb l'operador de lliurament per presentar una sol·licitud.
                </p>
                <p>
                  Si us plau, salveu tots els materials d'envasament i els béns danyats abans de presentar una reclamació.
                </p>
            </div>
            <div class="my-paragraph my-paragraph">
                <h4>Retorns</h4>
                <p>
                  Acceptem el retorn fins a 10 dies després del lliurament, si l'article no s'utilitza i en la seva condició original,
                  i reemborsarem l'import complet menys els costos d'enviament per al retorn.
                </p>
                <p>
                  En el cas que la ordre arribi malmesa de qualsevol manera, si us plau, envieu-nos un correu tan aviat com sigui possible
                  a <a href="mailto:help@shapeyourpc.com">help@shapeyourpc.com</a> amb el número de ordre i una foto de la condició de l'ítem.
                  Abordem aquestes qüestions caso per cas, però intentarem treballar de la millor manera possible per a aconseguir una solució
                  satisfactòria.
                </p>
                <p>
                  Si teniu més preguntes, no dubteu a contactar amb nosaltres a <a href="mailto:help@shapeyourpc.com">help@shapeyourpc.com</a>.
                <p>
            </div>
          </div>
        </div>

        <br/><br/>

        <?php include __DIR__.'/../views/includes/footer.php'; ?>

    </body>
</html>
