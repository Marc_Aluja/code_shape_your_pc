<!doctype html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title>Shape Your PC</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
              integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
              integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
        <link rel="stylesheet" href="<?php echo BASE_URL ?>/views/css/style_productes.css">
        <link rel="stylesheet" href="<?php echo BASE_URL ?>/views/css/style_footer.css">
        <link rel="stylesheet" href="<?php echo BASE_URL ?>/views/css/style_header.css">
    </head>
    <body>
        <?php if(isset($_SESSION['sessio_iniciada'])) {
                include __DIR__.'/../views/includes/header_sessio.php';;
            } else {
                include __DIR__.'/../views/includes/header.php';
        } ?>

        <br/><br/>

        <div class="container cont-prod">
          <div class="row d-flex justify-content-between">
            <div class="col-6">
              <h3 class="titol_productes"><?php echo $nom_cat ?></h3>
            </div>
            <div class="col-6">
              <form class="form-inline">
                <div class="form-group ordenar_per">
                  <label class="control-label">Ordena per:&ensp;|&ensp;</label>
                  <select class="form-control form-control-sm selec_ordre">
                    <option value="1">General</option>
                    <option value="2">Mes car a mes barat</option>
                    <option value="3">Mes barat a mes car</option>
                    <option value="4">Més puntuació primer</option>
                  </select>
                </div>
              </form>
            </div>
            <div class="col-12">
              <hr class="linia_titol" />
            </div>
          </div>
          <div class="row">
            <div class="col-2 mb-4">
              <section>
                <h5 class="mb-4">Filtres</h5>
                <section class="mb-4">
                  <h6 class="font-weight-bold mb-3">Marca</h6>
                  <?php $marques = array_unique(array_column($productes, 'marca'));
                  foreach ($marques as $i=>$marca): ?>
                  <div class="form-check mb-2">
                    <input type="checkbox" class="form-check-input filled-in filtre_marca" id="<?php echo $marca ?>" autocomplete="off">
                    <label class="form-check-label small text-uppercase card-link-secondary" for="new"><?php echo $marca ?></label>
                  </div>
                  <?php endforeach; ?>
                </section>
                <?php if(count($tipus) != 0) { ?>
                  <section class="mb-4">
                    <h6 class="font-weight-bold mb-3">Tipus</h6>
                    <?php
                    foreach ($tipus as $tipu): ?>
                    <div class="form-check mb-2">
                      <input type="checkbox" class="form-check-input filled-in filtre_tipus" id="<?php echo $tipu ?>" autocomplete="off">
                      <label class="form-check-label small text-uppercase card-link-secondary" for="new"><?php echo $tipu ?></label>
                    </div>
                    <?php endforeach; ?>
                  </section>
                <?php } ?>
                <section class="mb-4">
                  <h6 class="font-weight-bold mb-3">Preu</h6>
                  <form>
                    <div class="d-flex align-items-center mt-4 pb-1">
                      <div class="md-form md-outline my-0">
                        <input id="from" type="text" class="form-control mb-0 preu_min" placeholder="Min €">
                      </div>
                      <p class="px-2 mb-0 text-muted"> - </p>
                      <div class="md-form md-outline my-0">
                        <input id="to" type="text" class="form-control mb-0 preu_max" placeholder="Max €">
                      </div>
                    </div>
                  </form>
                </section>
                <section class="mb-4">
                  <hr class="style-linia-comp">
                  <form>
                    <div class="d-flex align-items-center mt-3 pb-1">
                      <div class="md-form md-outline my-0">
                        <p>Comparar <span class="n_compare">0</span> productes</p>
                        <p class="text-info">* Màxim 4 productes</p>
                        <button type="button" class="btn btn-primary btn-comparar">Comparar</button>
                      </div>
                    </div>
                  </form>
                </section>
              </section>
            </div>

            <div class="col-10 mb-4">
              <section>
                <div class="row productes" id="productes">
                  <?php foreach ($productes as $producte):?>
                    <div id="<?php echo $producte['id_prod'] ?>" class="col-4 mb-5 producte" data-preu="<?php echo $producte['preu']?>"
                                                                                             data-marca="<?php echo $producte['marca']?>"
                                                                                            <?php if(count($tipus) != 0) { ?>
                                                                                             data-tipus="<?php echo $producte['tipus_prod']?>"
                                                                                           <?php } ?>
                                                                                             data-stock="<?php echo $producte['stock']?>"
                                                                                             data-punts="<?php if($producte['avg_puntuacio'] == "") {
                                                                                               echo 0;
                                                                                             } else { echo $producte['avg_puntuacio']; }?>">
                      <a class="clicable-product" href="<?php echo BASE_URL ?>/index.php?action=mostrar_detalls_producte&producte=<?php echo $producte['nom'] ?>">
                        <div class="view zoom overlay rounded z-depth-2">
                          <?php $imatge = explode(" ", $producte['imatge']); ?>
                          <img class="img-fluid w-100" src="<?php echo BASE_URL ?><?php echo $imatge[0] ?>" alt="Sample">
                        </div>
                        <div class="pt-4">
                          <h5 class="nom_prod"><?php echo $producte['nom'] ?></h5>
                          <ul class="rating pl-0 mb-2">
                            <?php for($i = 1; $i <= 5; $i++) {
                              if($i <= round((float)$producte['avg_puntuacio'])) { ?>
                                  <li>
                                    <i class="fas fa-star fa-sm"></i>
                                  </li>
                            <?php } else { ?>
                                  <li>
                                    <i class="far fa-star fa-sm"></i>
                                  </li>
                            <?php }
                            } ?>
                          </ul>
                          <p class="p-preu"><span class="mr-1"><strong><?php echo $producte['preu']?> &euro;</strong></span></p>
                        </div>
                      </a>
                      <div class="form-check">
                        <input id="<?php echo $producte['nom'] ?>" class="form-check-input compare-check" type="checkbox" value="" autocomplete="off">
                        <label class="form-check-label" for="flexCheckDefault">Comparar</label>
                      </div>
                    </div>
                  <?php endforeach; ?>
                </div>
              </section>
            </div>

            <ul class="mx-auto" id="pagin">

            </ul>
          </div>
        </div>

        <br/><br/>

        <?php include __DIR__.'/../views/includes/footer.php'; ?>

        <!--Scripts particulars de la pagina-->
        <script src="<?php echo BASE_URL ?>/views/js/productes.js"></script>
    </body>
</html>
