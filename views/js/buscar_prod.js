var llista = [];
var class_suggest = "";

fetch("http://localhost/code_shape_your_pc/index.php?action=llista_noms", {
    headers : {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
     }

  }).then((res) => res.json()).then((result) => { llista = result.map(producte => producte.nom); }
);

function Buscar() {
  const [searchTerm, setSearchTerm] = React.useState("");
  const [searchResults, setSearchResults] = React.useState([]);

  const node = React.useRef();
  const [tancar, setTancar] = React.useState(false);

  const handleClickOutside = e => {
    if (node.current.contains(e.target)) {
      // click interior element
      return;
    }
    // click a fora
    setTancar(true);
  };

  React.useEffect(() => {
    if (!tancar) {
      document.addEventListener("mousedown", handleClickOutside);
    } else {
      document.removeEventListener("mousedown", handleClickOutside);
    }
    return () => {
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, [tancar]);

  const handleInputChange = (e) => {
    setTancar(false);
    setSearchTerm(e.target.value);
  };

  const handleInputClick = (e) => {
    setTancar(false);
  }

  const valorLlista = (e) => {
    var element = e.target.getAttribute('value');
    setSearchTerm(element);
    setTancar(true);
  };

  const rutaProducte = () => {
      window.location = "index.php?action=mostrar_detalls_producte&producte=" + searchTerm;
  }

  React.useEffect(() => {
    var results = llista.filter(producte =>
      (producte.includes(searchTerm) ||
      producte.toLowerCase().includes(searchTerm) || producte.toLowerCase().includes(searchTerm.toLowerCase()))
    );
    if(searchTerm === "" || results.length === 0) {
      class_suggest = "";
      setSearchResults([]);
    } else {
      class_suggest = "suggestions";
      results = results.slice(0,6);
      setSearchResults(results);
    }
  }, [searchTerm]);
  return (
    <nav id="nav-head" className="ml-5 my-2 my-md-0 mr-md-3 menu">
      <ul className="llista_opcions">
        <li ref={node}>
            <input
              className="form-control buscador"
              type="text"
              placeholder="Search"
              aria-label="Search"
              value={searchTerm}
              onChange={handleInputChange}
              onClick={handleInputClick}
            />
            {!tancar && (
              <ul className={class_suggest}>
                {searchResults.map(item => (
                  <li value={item} onClick={valorLlista}>{item}</li>
                ))}
              </ul>
            )}
        </li>
        <li>
            <button
              className="btn my-btn btn-outline-info my-2 my-sm-0 ml-2"
              type="button" onClick={rutaProducte}>Search
            </button>
        </li>
      </ul>
    </nav>
  );
}

const rootElement = document.getElementById("form-search");
ReactDOM.render(<Buscar />, rootElement);
