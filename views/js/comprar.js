document.addEventListener("DOMContentLoaded", function(event) {
  $('.dades-facturacio').hide();
  $('[id^=step-]').hide();
  $('#step-1').show();
  var actual_step = 1;

  $('.nextBtn').on("click", function(){
    var valid = true;
    var factura_diferent = $("#factura-diff").is(":checked");
    var list_envia = $('#step-' + actual_step).find("input[name^='envia-']");
    var list_factura = $('#step-' + actual_step).find("input[name^='factura-']");

    $(".md-form").removeClass("has-error");
    if(factura_diferent) {
      for(var i = 0; i < list_factura.length; i++){
          if (!list_factura[i].validity.valid){
              valid = false;
              break;
          }
      }
    }

    if(valid) {
      $('.billing-error').empty();
      $('[id^=step-]').hide();
      if(actual_step < 3) {
        actual_step = actual_step + 1;
      }
      $('#step-' + actual_step).show();
      $('[id^=btn-s]').removeClass('btn-primary').addClass('btn-light');
      $('#btn-s' + actual_step).removeClass('btn-light').addClass('btn-primary');
      if(actual_step == 2) {
        for(var i = 0; i < list_factura.length; i++){
            if(factura_diferent) {
              $('.resum-' + list_factura[i].id).empty();
              $('.resum-' + list_factura[i].id).append(list_factura[i].value);
            } else {
              $('#' + list_factura[i].id).val(list_envia[i].value);
              $('.resum-' + list_factura[i].id).empty();
              $('.resum-' + list_factura[i].id).append(list_envia[i].value);
            }
        }
      }
    } else {
      $('.billing-error').append( "<p>Omple totes les dades correctament</p>" );
    }

  })

  $('.backBtn').on("click", function(){
    $('.billing-error').empty();
    $('[id^=step-]').hide();
    if(actual_step > 1) {
      actual_step = actual_step - 1;
    }
    $('#step-' + actual_step).show();
    $('[id^=btn-s]').removeClass('btn-primary').addClass('btn-light');
    $('#btn-s' + actual_step).removeClass('btn-light').addClass('btn-primary');
  })

  $('#card_number').on("keydown", function() {
    var card_val = $(this).val();
    var card_val_trimmed = card_val.replace(/\s/g,'');
    if(card_val_trimmed.length % 4 === 0 && card_val_trimmed.length > 1 && card_val_trimmed.length < 16) {
      $(this).val($(this).val() + " ");
    }
  })

  $("#factura-diff").on("change", function() {
    if (this.checked) {
      $('.dades-facturacio').show();
    } else {
      $('.dades-facturacio').hide();
    }
  })

});
