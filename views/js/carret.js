document.addEventListener("DOMContentLoaded", function(event) {

  $(".minus").on("click", function() {
    var id_producte = $(this).attr("id");
    var actual_count = $(".contador-quant-" + id_producte).val();
    var stock = document.querySelector(".contador-quant-" + id_producte).dataset.stock;
    var preu = document.querySelector("#preu-" + id_producte).dataset.preu;
    if(actual_count > 0){
      $(".contador-quant-" + id_producte).val(actual_count - 1);
      window.location = 'index.php?action=actualitzar_carret&tipus=decrementar&id=' + id_producte + '&stock=' + stock + '&preu=' + preu;
    }
  });

  $(".plus").on("click", function() {
    var id_producte = $(this).attr("id");
    var actual_count = $(".contador-quant-" + id_producte).val();
    var stock = document.querySelector(".contador-quant-" + id_producte).dataset.stock;
    var preu = document.querySelector("#preu-" + id_producte).dataset.preu;
    if(actual_count < stock){
      actual_count = parseInt(actual_count) + parseInt(1);
      $(".contador-quant-" + id_producte).val(actual_count);
      window.location = 'index.php?action=actualitzar_carret&tipus=incrementar&id=' + id_producte + '&stock=' + stock + + '&preu=' + preu;
    }
  });

  $(".close").on("click", function() {
    var id_producte = $(this).attr("id");
    window.location = 'index.php?action=actualitzar_carret&tipus=decrementar&id=' + id_producte + '&stock=0';
  })

  $(".btn-comprar").on("click", function() {
    window.location = 'index.php?action=realitzar_compra';
  })

});
