document.addEventListener("DOMContentLoaded", function(event) {
  //funció per carregar els productes de la categoria clicada
  $(".submit-subscripcio").on( "click", function() {
    var email = $('.email-subscripcio').val();
    $("#noti-subscripcio").load('index.php?action=subscripcio_web&email=' + email, function() {
      var text_avis = $(".email-subscripcio").attr("data-avis");
      $('.text-avis-dialeg').text(text_avis);
      $('#dialeg_notificacio').trigger("click");
    })
  });

});
