
var selec_llista = [];
var dict_seleccionats = {};
var g_categoria = "";
var g_tipus = "";
var id_item_selec = "";

function SetResum() {
  const [errorText, setErrorText] = React.useState("");
  const afegirOrdinador = () => {
    if("PROCESSADORS" in dict_seleccionats && "PLAQUES MARE" in dict_seleccionats && "GRAFIQUES" in dict_seleccionats &&
       "ram" in dict_seleccionats && "FONTS ALIMENTACIO" in dict_seleccionats && "REFRIGERACIO" in dict_seleccionats &&
       "CAIXES" in dict_seleccionats && ("hdd" in dict_seleccionats || "ssd" in dict_seleccionats)) {
      setErrorText("");
      const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
                    "dict_prods": dict_seleccionats
                  })
      };
      fetch("http://localhost/code_shape_your_pc/index.php?action=afegir_ordinador", requestOptions)
            .then((res) => res.json()).then((result) => {
              if(result == "correcte") {
              window.location = "index.php?action=mostrar_carret";
            }
          });
    } else {
      setErrorText("Error, falten prodectes per la configuració.");
    }
  }
  let preu_seleccio = 0;
  for (const [key, value] of Object.entries(dict_seleccionats)) {
    preu_seleccio = preu_seleccio + parseFloat(value.preu);
  }
  preu_seleccio = preu_seleccio.toFixed(2);
  return(
    <React.Fragment>
      <h3>Resum</h3>
      <div className="summary-item"><span className="text">Nº items</span>
                                    <span className="preu">{Object.keys(dict_seleccionats).length} items</span></div>
      <div className="summary-item"><span className="text">Total</span><span className="preu">{preu_seleccio} €</span></div>
      <button type="button" className="btn btn-primary btn-lg btn-block" onClick={afegirOrdinador}>Afegir al carret</button>
      <div className="error-add mt-4">{errorText}</div>
    </React.Fragment>
  )
}

function SetProducte() {
  let item = selec_llista.find((element) => {
    return element.id_prod === id_item_selec;
  })
  if(g_categoria == "MEMORIES") {
    dict_seleccionats[g_tipus] = item;
  } else {
    dict_seleccionats[g_categoria] = item;
  }
  return (
    <React.Fragment>
      <td className="col-logo" id={item.id_prod}><img className="logo-2 ml-4" src={item.imatge} alt="Sample" /></td>
      <td className="col-nom" id={item.id_prod}>{item.nom}</td>
      <td id={item.id_prod}>{item.preu}€</td>
    </React.Fragment>
  );
}

function App() {
  const [searchTerm, setSearchTerm] = React.useState("");
  const [searchResults, setSearchResults] = React.useState([]);
  const handleChange = e => {
    setSearchTerm(e.target.value);
  };
  const seleccionar_item = (item) => {
    id_item_selec = item.target.getAttribute("id");
    const rootElement = document.getElementById("seleccio-" + g_categoria + g_tipus);
    ReactDOM.render(<SetProducte />, rootElement);
    const rootElement2 = document.getElementById("resum-seleccio");
    ReactDOM.render(<SetResum />, rootElement2);
  };
  React.useEffect(() => {
    const results = selec_llista.filter(el =>
      el.nom.toLowerCase().includes(searchTerm)
    );
    setSearchResults(results);
  }, [searchTerm]);
  React.useEffect(() => {
    setSearchTerm("");
    setSearchResults(selec_llista);
  }, [selec_llista]);
  return (
    <div className="App">
      <input
        className="form-control mb-4 search-selec"
        type="text"
        placeholder="Search"
        value={searchTerm}
        onChange={handleChange}
      />
      <table className="table table-hover table-selec">
        <thead></thead>
        <tbody>
            {searchResults.map(item => (
              <tr onClick={seleccionar_item} data-dismiss="modal">
                <td id={item.id_prod} className="col-logo"><img className="logo ml-4" src={item.imatge} alt="Sample" /></td>
                <td id={item.id_prod}>{item.nom}</td>
              </tr>
            ))}
        </tbody>
      </table>
    </div>
  );
}

function selec_producte(cat, tipus) {
  g_categoria = cat;
  g_tipus = tipus;
  fetch("http://localhost/code_shape_your_pc/index.php?action=productes_creador&categoria=" + cat + "&tipus=" + tipus, {
      headers : {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
    }).then((res) => res.json()).then((result) => { selec_llista = result.map(producte => producte);
                                                    const rootElement = document.getElementById("selec-prod-body");
                                                    ReactDOM.render(<App />, rootElement);
                                                  }
  );
}
