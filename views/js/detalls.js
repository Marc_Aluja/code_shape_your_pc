document.addEventListener("DOMContentLoaded", function(event) {
  //funcions pels detalls de productes
  $(".wish_btn").on("click", function() {
    var tipus = "";
    var id = $(".taula_producte").attr("id");
    var actual_wish = $(this).find(".fa-heart").css("font-weight");
    if(actual_wish > 100) {
      tipus = "eliminar";
    } else {
      tipus = "afegir";
    }
    $(".wish_btn").load('index.php?action=actualitzar_wishlist&tipus=' + tipus + '&id=' + id);
  });

  $(".minus").on("click", function() {
    var actual_count = $(".contador_quant").val();
    if(actual_count > 0){
      $(".contador_quant").val(actual_count - 1);
    }
  });

  $(".plus").on("click", function() {
    const stock = document.querySelector('#quantitat');
    var actual_count = $(".contador_quant").val();
    if(actual_count < stock.dataset.stock){
      actual_count = parseInt(actual_count) + parseInt(1);
      $(".contador_quant").val(actual_count);
    }
  });

  $(".nova_opinio").on("click", function(e) {
    e.preventDefault();
    var aid = $(this).attr("href");
    $('html,body').animate({scrollTop: $(aid).offset().top},'slow');
    $('#reviews-tab').trigger("click");
  })

  $("#description-tab").on("click", function(e) {
    e.preventDefault();
    if($("#reviews").css("display") != "none") {
      $("#reviews").css("display", "none");
      $("#reviews-tab").removeClass("seleccionat");
    }
    $("#description").css("display", "block");
    $("#description-tab").addClass("seleccionat");
  })

  $("#reviews-tab").on("click", function(e) {
    e.preventDefault();
    if($("#description").css("display") != "none") {
      $("#description").css("display", "none");
      $("#description-tab").removeClass("seleccionat");
    }
    $("#reviews").css("display", "block");
    $("#reviews-tab").addClass("seleccionat");
    var id = $(".taula_producte").attr("id");
    var nom = $(".pro-d-title").text();
    $("#valoracions").load('index.php?action=mostrar_valoracions&id_prod=' + id + '&nom_prod=' + encodeURIComponent(nom));
  })

  $(".pro-img-list > a").on("click", function(e) {
    var new_source = $(this).find("#imatge_llista").attr("src");
    $(".pro-img-details > img").attr("src", new_source);
  })

  //codi per afegir al carret
  $(".afegir_carret").on( "click",function() {
      var quantitat = $(".contador_quant").val();
      var id = $(".taula_producte").attr("id");
      var stock = $("#quantitat").attr("data-stock");
      var preu = $("#preu").attr("data-preu");
      $(".nav-user-cart").load('index.php?action=actualitzar_carret&tipus=afegir&quantitat=' + quantitat + '&id=' + id + '&stock=' + stock
        + '&preu=' + preu, function() {
        var text_avis = $("#carret").attr("data-avis");
        $('.text-avis-dialeg').text(text_avis);
        $('#dialeg_notificacio').trigger("click");
      })
  });

  //codi per afegir valoracions
  $(".estrelles > li > i").on("click", function() {
    var valor = $(this).attr("id");
    $('.estrelles').children('li').each(function() {
      if($(this).find('i').attr("id") <= valor) {
        $(this).find('i').css("font-weight", "900");
      } else {
        $(this).find('i').css("font-weight", "400");
      }
    });
    $('span[name="puntuacio"]').val(valor);
  })

  $("#boto_opinio").on("click", function() {
    var id = $(".taula_producte").attr("id");
    var punts = $('span[name="puntuacio"]').val();
    var coment = $('textarea[name="comentari"]').val();
    if((punts != "") && (coment != "")) {
      $("#afegir_valo").load('index.php?action=afegir_valoracio&id_prod=' + id + '&punts=' + punts + '&comentari=' + encodeURIComponent(coment));
    }
  })
});
