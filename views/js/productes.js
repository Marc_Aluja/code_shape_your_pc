document.addEventListener("DOMContentLoaded", function(event) {
  //funció per carregar el detall dels productes clicats
  (window.listener_producte = function() {
      var compare_array = [];
      $(".compare-check").on("change", function() {
        if(this.checked) {
          if(compare_array.length < 4) {
            compare_array.push($(this).attr('id'));
            $('.n_compare').text(compare_array.length);
          }
        } else {
          var eliminar = $(this).attr('id');
          compare_array = $.grep(compare_array, function(a) {
                            return a != eliminar;
                          });
          $('.n_compare').text(compare_array.length);
        }
      });
      $(".btn-comparar").on("click", function() {
        actual_items = $('.n_compare').text();
        var compareArrStr = encodeURIComponent(JSON.stringify(compare_array));
        if(parseInt(actual_items) > 1) {
          window.location = "index.php?action=comparar_productes&productes=" + compareArrStr;
        }
      })
  })();

  var ordenar_productes = function(ordre) {
    switch(ordre) {
      case "1":
        $('.productes div.producte').sort(function(a,b) {
          if($(this).is(":hidden")) {
            $(this).show();
          }
          return parseFloat(a.id) - parseFloat(b.id)
        }).appendTo('.productes');
        break;
      case "2":
        $('.productes div.producte').sort(function(a,b) {
          if($(this).is(":hidden")) {
            $(this).show();
          }
          return parseFloat(b.dataset.preu) - parseFloat(a.dataset.preu)
        }).appendTo('.productes');
        break;
      case "3":
        $('.productes div.producte').sort(function(a,b) {
          if($(this).is(":hidden")) {
            $(this).show();
          }
          return parseFloat(a.dataset.preu) - parseFloat(b.dataset.preu)
        }).appendTo('.productes');
        break;
      case "4":
        $('.productes div.producte').sort(function(a,b) {
          if($(this).is(":hidden")) {
            $(this).show();
          }
          return parseFloat(b.dataset.punts) - parseFloat(a.dataset.punts)
        }).appendTo('.productes');
        break;
    }
    crear_llista_pag();
    listener_pagination();
    showPage(1);
    listener_producte();
  }

  //Funcio pel control de l'ordre dels productes
  $(".selec_ordre").on("change", function() {
    var ordre = $(this).val();
    ordenar_productes(ordre);
  })

  //Actualitzacio de filtres en conjunt
  var filtres_marca = [];
  var filtres_tipus = [];
  var amagats = [];
  function actualitza_filtres() {
    $("#page-navi").remove();
    min = $(".preu_min").val();
    max = $(".preu_max").val();

    //coloquem el minim o el maxim preu que existeix al valor que sigui null
    if(min == "" || max == "") {
      var min_aux = null;
      var max_aux = null;
      $('.productes').children('div').each(function () {
        var preu = parseFloat(this.dataset.preu);
        if ((min_aux===null) || (preu < min_aux)) { min_aux = preu; }
        if ((max_aux===null) || (preu > max_aux)) { max_aux = preu; }
      });
      amagats.forEach(function(item) {
        var preu = parseFloat(item[0].dataset.preu);
        if ((min_aux===null) || (preu < min_aux)) { min_aux = preu; }
        if ((max_aux===null) || (preu > max_aux)) { max_aux = preu; }
      });
      if(min == "") {
        min = min_aux;
      }
      if(max == "") {
        max = max_aux;
      }
    }

    $('.productes').children('div').each(function () {
      if($(this).is(":hidden")) {
        if(parseFloat(this.dataset.preu) <= max && parseFloat(this.dataset.preu) >= min) {
          $(this).show();
        } else {
          amagats.push($(this));
          $(this).remove();
        }
      } else {
        if(parseFloat(this.dataset.preu) > max || parseFloat(this.dataset.preu) < min){
          amagats.push($(this));
          $(this).remove();
        }
      }

      if(filtres_marca.length != 0) {
        if(filtres_marca.includes(this.dataset.marca) == false) {
          amagats.push($(this));
          $(this).remove();
        }
      }

      if(filtres_tipus.length != 0) {
        if(filtres_tipus.some(item => this.dataset.tipus.split(" ").includes(item)) == false) {
          amagats.push($(this));
          $(this).remove();
        }
      }

    });

    amagats.forEach(function(item) {
      var eliminar = true;

      if(parseFloat(item[0].dataset.preu) > max || parseFloat(item[0].dataset.preu) < min) {
        eliminar = false;
      }

      if(filtres_marca.length != 0 && filtres_marca.includes(item[0].dataset.marca) == false) {
        eliminar = false
      }

      if(filtres_tipus.length != 0 && filtres_tipus.some(x => item[0].dataset.tipus.split(" ").includes(x)) == false) {
        eliminar = false;
      }

      if(eliminar == true) {
        var element = item;
        amagats = $.grep(amagats, function(a) {
                          return a != element;
                        });
        $(".productes").append(item);
      }
    });

    $('.productes div.producte').sort(function(a,b) {
     return parseFloat(a.id) - parseFloat(b.id)
    }).appendTo('.productes');

    ordenar_productes($(".selec_ordre").val());
  }

  //canvi filtre de la Marca
  $(".filtre_marca").on("change", function() {
    if (this.checked) {
      filtres_marca.push($(this).attr('id'));
      actualitza_filtres();
    } else {
      var eliminar = $(this).attr('id');
      filtres_marca = $.grep(filtres_marca, function(a) {
                        return a != eliminar;
                      });
      actualitza_filtres();
    }
  })

  //canvi filtre de tipus
  $(".filtre_tipus").on("change", function() {
    if (this.checked) {
      filtres_tipus.push($(this).attr('id'));
      actualitza_filtres();
    } else {
      var eliminar = $(this).attr('id');
      filtres_tipus = $.grep(filtres_tipus, function(a) {
                        return a != eliminar;
                      });
      actualitza_filtres();
    }
  })

  //canvi filtre del Preu
  $(".preu_min, .preu_max").on("change", function() {
    actualitza_filtres();
  })

  //paginacio de productes
  pageSize = 9;
  (window.crear_llista_pag = function() {
    var elements = $("#productes").children().length;
    var n_pag = elements / pageSize;
    $('#pagin').children('li').each(function () {
      if(this.id == "afegit") {
        $(this).remove();
      }
    });
    for (var i = 0; i < n_pag; i++) {
      var num = i + 1;
      if(num == 1) {
        var llista = $( "<li class='n_pag current page-item' id='afegit'><a class='page-link' href='#'>" + num + "</a></li>" )
      } else {
        var llista = $( "<li class='n_pag page-item' id='afegit'><a class='page-link' href='#'>" + num + "</a></li>" )
      }
      $("#pagin").append(llista);
    }
  })();

  (window.showPage = function(page) {
      $(".producte").hide();
      $(".producte").each(function(n) {
          if (n >= pageSize * (page - 1) && n < pageSize * page)
              $(this).show();
      });
  })();

  (window.listener_pagination = function() {
    $(".n_pag").on("click", function() {
        $(".n_pag").removeClass("current");
        $(this).addClass("current");
        showPage(parseInt($(this).text()))
    });
  })();

  showPage(1);
});
