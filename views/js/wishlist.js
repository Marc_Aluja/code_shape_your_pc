document.addEventListener("DOMContentLoaded", function(event) {
  //listener per quan cliquem la x
  $(".remove").on("click", function() {
    var recarregar = "true";
    var id = this.dataset.producte;
    window.location = 'index.php?action=actualitzar_wishlist&id=' + id + '&tipus=eliminar&recarregar=' + recarregar;
  });

  $(".detalls").on( "click", function() {
    var nom = this.dataset.nom;
    window.location = "index.php?action=mostrar_detalls_producte&producte=" + nom;
  });
});
