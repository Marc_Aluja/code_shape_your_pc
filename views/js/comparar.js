
document.addEventListener("DOMContentLoaded", function(event) {
  //codi per afegir al carret
  $(".afegir_carret").on( "click",function() {
      var quantitat = 1;
      var id = $(this).attr("id");
      var stock = this.dataset.stock;
      var preu = this.dataset.preu;
      $(".nav-user-cart").load('index.php?action=actualitzar_carret&tipus=afegir&quantitat=' + quantitat + '&id=' + id + '&stock=' + stock
        + '&preu=' + preu, function() {
        var text_avis = $("#carret").attr("data-avis");
        $('.text-avis-dialeg').text(text_avis);
        $('#dialeg_notificacio').trigger("click");
      })
  });
});
