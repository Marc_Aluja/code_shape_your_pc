<!doctype html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title>Shape Your PC</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
              integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
              integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
        <link rel="stylesheet" href="<?php echo BASE_URL ?>/views/css/style_wishlist.css">
        <link rel="stylesheet" href="<?php echo BASE_URL ?>/views/css/style_footer.css">
        <link rel="stylesheet" href="<?php echo BASE_URL ?>/views/css/style_header.css">
    </head>
    <body>
        <?php if(isset($_SESSION['sessio_iniciada'])) {
                include __DIR__.'/../views/includes/header_sessio.php';;
            } else {
                include __DIR__.'/../views/includes/header.php';
        } ?>

        <br/><br/>

        <div class="container">
          <div class="row">
            <div class="col-12 titol_super">
              <h3 class="text-uppercase">Wishlist<h3>
              <hr class="linia_titol" />
            </div>
          </div>
          <section>
            <div class="row justify-content-md-start">
              <?php foreach ($productes_wishlist as $producte):?>
                <div class="col-md-3 mb-5">
                  <div class="">
                    <div class="view zoom overlay z-depth-2 rounded">
                      <?php $imatge = explode(" ", $producte['imatge']); ?>
                      <img class="img-fluid w-100"
                        src="<?php echo BASE_URL ?><?php echo $imatge[0] ?>" alt="Sample">
                    </div>
                    <div class="text-center pt-4">
                      <h5><?php echo $producte['nom'] ?></h5>
                      <p class="mb-2 text-muted text-uppercase small"><?php echo $producte['nom_cat'] ?></p>
                      <ul class="rating ml-5">
                        <?php for($i = 1; $i <= 5; $i++) {
                          if($i <= round((float)$producte['avg_puntuacio'])) { ?>
                              <li>
                                <i class="fas fa-star fa-sm"></i>
                              </li>
                        <?php } else { ?>
                              <li>
                                <i class="far fa-star fa-sm"></i>
                              </li>
                        <?php }
                        } ?>
                      </ul>
                      <hr>
                      <h6 class="mb-3"><?php echo $producte['preu'] ?>€</h6>
                      <button type="button" class="detalls btn my-btn btn-light btn-sm mr-1 mb-2" data-nom="<?php echo $producte['nom'] ?>"><i
                          class="fas fa-info-circle pr-2"></i>Detalls</button>
                      <button type="button" class="remove btn my-btn btn-light btn-sm px-3 mb-2 material-tooltip-main"
                        data-toggle="tooltip" data-placement="top" data-producte="<?php echo $producte['id_prod'] ?>"
                        title="Remove from wishlist"><i class="fas fa-times"></i></button>
                    </div>
                  </div>
                </div>
              <?php endforeach; ?>
            </div>
          </section>
        </div>

        <br/><br/>

        <?php include __DIR__.'/../views/includes/footer.php'; ?>

        <!--Scripts particulars de la pagina-->
        <script src="<?php echo BASE_URL ?>/views/js/wishlist.js"></script>
    </body>
</html>
