<?php

function consultaSuperCategorias($conn)
{
    try {
        $sql = "SELECT * FROM super_categoria";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $super_cat = $stmt->fetchAll();

        foreach ($super_cat as $index => $cat) {
          $cat = array_map("htmlentities", $cat);
          $super_cat[$index] = $cat;
        }
        
        return($super_cat);

    } catch(PDOException $e) {
      echo "Error: " . $e->getMessage();
    }
}

?>
