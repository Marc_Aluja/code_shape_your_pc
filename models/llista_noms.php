<?php

function consultaNomsProductes($conn)
{
    try {
        $sql = "SELECT nom FROM producte";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $productes = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($productes as $index => $producte) {
          $productes[$index] = array_map("htmlentities", $producte);
        }

        return($productes);
        
    } catch(PDOException $e) {
      echo "Error: " . $e->getMessage();
    }
}

?>
