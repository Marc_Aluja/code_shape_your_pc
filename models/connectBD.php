<?php

function connectarBD() {
  try {
    $conn = new PDO('mysql:host=' . DATABASE_HOST . ';dbname=' . DATABASE_NAME . ';charset=utf8mb4',
                    DATABASE_USER,
                    DATABASE_PASSWORD);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    //echo "Connexió correcta";
    return $conn;
  }
  catch (PDOException $e) {
    echo "Error en la connexió: " . $e->getMessage();
  }
}

?>
