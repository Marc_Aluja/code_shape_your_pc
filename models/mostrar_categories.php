<?php

function consultaCategories($conn, $nom)
{
    try {
        $sql = "";
        if($nom == "CREADOR") {
          $sql = "SELECT id_cat, nom, logo FROM categoria";
          $stmt = $conn->prepare($sql);
        } else {
          $sql = "SELECT * FROM categoria WHERE id_supercat_fk=:id_super";
          $stmt = $conn->prepare($sql);
          $stmt->bindParam('id_super', $_GET['id'], PDO::PARAM_INT);
        }
        $stmt->execute();
        $categories = $stmt->fetchAll();

        foreach ($categories as $index => $categoria) {
          $categories[$index] = array_map("htmlentities", $categoria);
        }

        return($categories);

    } catch(PDOException $e) {
      echo "Error: " . $e->getMessage();
    }
}

?>
