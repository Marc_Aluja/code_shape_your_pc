<?php

function consultaConfirmarCompra($conn) {
    try {
      $missatge = "";
      //afegim la ordre a la base de dades
      $preu = 0;
      foreach ($_SESSION['carret']['productes'] as $producte) {
        $preu = $preu + $producte['quantitat'] * $producte['preu'];
      }
      $now = date("y-m-d");
      $sql="INSERT INTO ordre (quantitat, preu, data, id_usuari_fk) VALUES (?,?,?,?)";
      $stmt = $conn->prepare($sql);
      $stmt->execute([$_SESSION['carret']['quantitat'], $preu + 3.00, $now, $_SESSION['usuari']['user_id']]);

      //obtenim la id de l'ultima ordre introduida
      $sql = "SELECT id_ordre FROM ordre ORDER BY id_ordre DESC LIMIT 1";
      $stmt = $conn->prepare($sql);
      $stmt->execute();
      $ordre = $stmt->fetch(PDO::FETCH_ASSOC);

      //afegirem cada producte de la ordre a les linies de l'ordre
      foreach($_SESSION['carret']['ids_productes'] as $id_prod) {
          $sql="INSERT INTO linia_ordre (quantitat, preu, id_ordre_fk, id_producte_fk) VALUES (?,?,?,?)";
          $stmt = $conn->prepare($sql);
          $stmt->execute([$_SESSION['carret']['productes'][$id_prod]['quantitat'], $_SESSION['carret']['productes'][$id_prod]['preu'],
                            $ordre['id_ordre'], $id_prod]);

          $sql = "SELECT stock FROM producte WHERE id_prod=:id";
          $stmt = $conn->prepare($sql);
          $stmt->bindParam('id', $id_prod, PDO::PARAM_INT);
          $stmt->execute();
          $producte = $stmt->fetch();

          $new_stock = $producte['stock'] - $_SESSION['carret']['productes'][$id_prod]['quantitat'];

          $sql = "UPDATE producte SET stock=:n_stock WHERE id_prod=:id_producte";
          $stmt = $conn->prepare($sql);
          $stmt->bindParam('n_stock', $new_stock, PDO::PARAM_INT);
          $stmt->bindParam('id_producte', $id_prod, PDO::PARAM_INT);
          $stmt->execute();
      }

      //inserim dades de facturacio de la persona
      $nom = $_POST['factura-nom'];
      $direccio = $_POST['factura-direccio'];
      $detalls_direccio = $_POST['factura-detalls-dir'];
      $pais = $_POST['factura-pais'];
      $provincia = $_POST['factura-provincia'];
      $ciutat = $_POST['factura-ciutat'];
      $cp = $_POST['factura-cp'];
      $telefon = $_POST['factura-telefon'];

      $sql="INSERT INTO dades_facturacio (nom, pais, provincia, ciutat, direccio, detall_dir, codi_postal, telefon, id_ordre_fk)
            VALUES (?,?,?,?,?,?,?,?,?)";
      $stmt = $conn->prepare($sql);
      $stmt->execute([$nom, $pais, $provincia, $ciutat, $direccio, $detalls_direccio, $cp, $telefon, $ordre['id_ordre']]);

      //netejem valors del carret
      unset($_SESSION['carret']);
      $_SESSION['carret']['quantitat'] = 0;
      $_SESSION['carret']['ids_productes'] = array();
      $_SESSION['carret']['productes'] = null;
      $_SESSION['carret']['avis'] = "";
      $_SESSION['carret']['ordinador'] = "false";
      $missatge = "La teva ordre s'ha confirmat, pots controlar el seu estat a les ordres del teu perfil";

      return($missatge);

    } catch(PDOException $e) {
      echo "Error: " . $e->getMessage();
    }
}
