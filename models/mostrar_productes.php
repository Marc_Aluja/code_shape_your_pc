<?php

function consultaProductes($conn)
{
    try {
        $id_cat = $_GET['id'];
        $nom_cat = $_GET['nom'];
        $nom_cat = str_replace(' ', '_', $nom_cat);

        $sql = "SHOW COLUMNS FROM ".$nom_cat;
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $columnes = $stmt->fetchAll();
        $columnes = array_column($columnes, 'Field', 'Field');

        if(array_key_exists('tipus', $columnes)) {
          $sql = "SELECT producte.*, " . $nom_cat . ".tipus AS tipus_prod
                  FROM " . $nom_cat . "
                  JOIN producte
                  WHERE " . $nom_cat . ".id_producte_fk=producte.id_prod AND producte.id_categoria_fk=:id_cat";
          $stmt = $conn->prepare($sql);
          $stmt->bindParam('id_cat', $id_cat, PDO::PARAM_INT);
          $stmt->execute();
          $productes = $stmt->fetchAll(PDO::FETCH_ASSOC);

          foreach($productes as &$producte) {
              $producte['tipus_prod'] = explode(" ", $producte['tipus_prod']);
          }

          $tipus_multi = array_column($productes, 'tipus_prod');
          $tipus = array_unique(array_reduce($tipus_multi, 'array_merge', array()));

          foreach($productes as &$producte) {
              $producte['tipus_prod'] = implode(' ', $producte['tipus_prod']);
          }
        } else {
          $sql = "SELECT * FROM producte WHERE id_categoria_fk=:id_cat";
          $stmt = $conn->prepare($sql);
          $stmt->bindParam('id_cat', $id_cat, PDO::PARAM_INT);
          $stmt->execute();
          $productes = $stmt->fetchAll();
          $tipus = array();
        }

        foreach ($productes as &$producte) {
          //seleccionem mitjana de puntuacio del producte
          $id_producte = $producte['id_prod'];
          $sql = "SELECT AVG(puntuacio) as avg_puntuacio
                  FROM valoracio
                  WHERE id_producte_fk=:id_prod";
          $stmt = $conn->prepare($sql);
          $stmt->bindParam('id_prod', $id_producte, PDO::PARAM_INT);
          $stmt->execute();
          $puntuacio = $stmt->fetch(PDO::FETCH_ASSOC);
          $producte = array_merge($producte,$puntuacio);
          $producte = array_map("htmlentities", $producte);
        }

        return array($productes, $tipus);

    } catch(PDOException $e) {
      echo "Error: " . $e->getMessage();
    }
}

?>
