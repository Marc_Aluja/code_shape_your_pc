<?php

function consultaWishlist($conn)
{
    $productes = [];
    try {
      foreach($_SESSION['wishlist'] as $id) {
        $sql = "SELECT producte.*, categoria.nom AS nom_cat
                FROM categoria
                JOIN producte
                WHERE producte.id_prod=:id_prod AND categoria.id_cat=producte.id_categoria_fk";
        $stmt = $conn->prepare($sql);
        $stmt->bindParam('id_prod', $id, PDO::PARAM_INT);
        $stmt->execute();
        $producte = $stmt->fetch(PDO::FETCH_ASSOC);

        //necessitem agafar també la puntuació
        $sql = "SELECT AVG(puntuacio) as avg_puntuacio
                FROM valoracio
                WHERE id_producte_fk=:id_prod";
        $stmt = $conn->prepare($sql);
        $stmt->bindParam('id_prod', $id, PDO::PARAM_INT);
        $stmt->execute();
        $puntuacio = $stmt->fetch(PDO::FETCH_ASSOC);

        $producte = array_merge($producte,$puntuacio);
        array_push($productes, $producte);
      }

      foreach ($productes as $index => $producte) {
        $productes[$index] = array_map("htmlentities", $producte);
      }

      return($productes);

    } catch(PDOException $e) {
      echo "Error: " . $e->getMessage();
    }
}

?>
