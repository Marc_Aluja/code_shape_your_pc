<?php

function consultaActualitzarCarret()
{
    $id = $_GET['id'];
    $stock = $_GET['stock'];
    $preu = $_GET['preu'];
    $missatge = "El carret s'ha actualitzat correctament.";

    switch ($_GET['tipus']) {
        case 'afegir':
            if($_SESSION['carret']['ordinador'] != "true") {
              $quantitat = $_GET['quantitat'];
              if(isset($_SESSION['carret']['productes'][$id])) {
                if($_SESSION['carret']['productes'][$id]['quantitat'] + $quantitat <= $stock) {
                  $_SESSION['carret']['productes'][$id]['quantitat'] = $_SESSION['carret']['productes'][$id]['quantitat'] + $quantitat;
                  $_SESSION['carret']['quantitat'] = $_SESSION['carret']['quantitat'] + $quantitat;
                } else {
                  $missatge = "S'ha arribat al màxim d'stock disponible o la quantitat total supera l'stock.";
                }
              } else {
                $array_ids = $_SESSION['carret']['ids_productes'];
                array_push($array_ids, $id);
                $_SESSION['carret']['ids_productes'] = $array_ids;
                $_SESSION['carret']['productes'][$id]['quantitat'] = $quantitat;
                $_SESSION['carret']['productes'][$id]['preu'] = $preu;
                $_SESSION['carret']['quantitat'] = $_SESSION['carret']['quantitat'] + $quantitat;
              }
              $_SESSION['carret']['ordinador'] = "false";
            } else {
              $missatge = "No es poden afegir mes productes a una configuració del creador";
            }
            break;
        case 'incrementar':
            if(($_SESSION['carret']['productes'][$id]['quantitat'] + 1) <= $stock) {
              $_SESSION['carret']['productes'][$id]['quantitat'] = $_SESSION['carret']['productes'][$id]['quantitat'] + 1;
              $_SESSION['carret']['quantitat'] = $_SESSION['carret']['quantitat'] + 1;
            } else {
              $missatge = "S'ha arribat al màxim d'stock disponible o la quantitat total supera l'stock.";
            }
            break;
        case 'decrementar':
            if(($_SESSION['carret']['productes'][$id]['quantitat'] - 1) > 0 && $stock != 0) {
              $_SESSION['carret']['productes'][$id]['quantitat'] = $_SESSION['carret']['productes'][$id]['quantitat'] - 1;
              $_SESSION['carret']['quantitat'] = $_SESSION['carret']['quantitat'] - 1;
            } else {
              $_SESSION['carret']['quantitat'] = $_SESSION['carret']['quantitat'] - $_SESSION['carret']['productes'][$id]['quantitat'];
              unset($_SESSION['carret']['productes'][$id]);
              $array_ids = $_SESSION['carret']['ids_productes'];
              foreach ($array_ids as $index => $value)  {
                 if($value == $id) {
                   unset($array_ids[$index]);
                   break;
                 }
              }
              $_SESSION['carret']['ids_productes'] = $array_ids;
              $missatge = "Eliminat";
              $_SESSION['carret']['ordinador'] = "false";
            }
            break;
        default:
            break;
    }

    return $missatge;
}
