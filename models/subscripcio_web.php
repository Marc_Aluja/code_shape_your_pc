<?php

function consultaSubscripcio($conn) {
    try {
        $email = $_GET['email'];
        $data = date("Y/m/d");

        if(filter_var($email,FILTER_VALIDATE_EMAIL)) {
            //comprovem si hi ha algun usuari a la base de dades amb aquest email, ja que no es pot repetir
            $sql="SELECT * FROM subscripcio WHERE email=:email";
            $stmt = $conn->prepare($sql);
            $stmt->bindParam('email', $email, PDO::PARAM_STR);
            $stmt->execute();
            $user = $stmt->fetch();

            if($user == null) {
                $sql="INSERT INTO subscripcio (email, data) VALUES (?,?)";
                $stmt = $conn->prepare($sql);
                $stmt->execute([$email, $data]);
                $missatge = "Subscripció efectuada correctament";
            } else {
                $missatge = 'Aquest email ja esta subscrit a la web.';
            }
        } else {
            $missatge = 'El filtrat del mail ha fallat, no utilitzi codi extrany';
        }

        return $missatge;

    } catch(PDOException $e) {
        echo "Error: " . $e->getMessage();
    }
}
