<?php

function consultaCancelarOrdre($conn)
{
    try {
        $id_ordre = $_GET['id_ordre'];
        $retorn = "";

        $sql = "DELETE FROM ordre WHERE id_ordre=:id";
        $stmt = $conn->prepare($sql);
        $stmt->bindParam('id', $id_ordre, PDO::PARAM_INT);
        $stmt->execute();
        $retorn = "Ordre cancelada correctament";

        return($retorn);

    } catch(PDOException $e) {
        echo "Error: " . $e->getMessage();
    }
}

?>
