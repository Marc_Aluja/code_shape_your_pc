<?php

function consultaValoracions($conn)
{
    $noms = [];
    try {
        $id_producte = $_GET['id_prod'];
        $sql = "SELECT * FROM valoracio WHERE id_producte_fk=:id_prod";
        $stmt = $conn->prepare($sql);
        $stmt->bindParam('id_prod', $id_producte, PDO::PARAM_INT);
        $stmt->execute();
        $valoracions = $stmt->fetchAll();

        foreach ($valoracions as $valoracio) {
          $sql = "SELECT nom FROM usuari WHERE id_usuari=:id_user";
          $stmt = $conn->prepare($sql);
          $stmt->bindParam('id_user', $valoracio['id_usuari_fk'], PDO::PARAM_INT);
          $stmt->execute();
          $usuari = $stmt->fetch(PDO::FETCH_ASSOC);
          array_push($noms, $usuari['nom']);
        }

        foreach ($valoracions as $index => $valoracio) {
          $valoracions[$index] = array_map("htmlentities", $valoracio);
        }
        $noms = array_map("htmlentities", $noms);
        return array($valoracions, $noms);

    } catch(PDOException $e) {
      echo "Error: " . $e->getMessage();
    }
}

?>
