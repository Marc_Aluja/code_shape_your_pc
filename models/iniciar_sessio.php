<?php

function consultaSessio($conn)
{
    try {
      $email = $_POST['sessio_usuari'];
      $pass = $_POST['sessio_contrassenya'];
      $correcte = "";

      //Multipliquem per comprovar si algun dels valors esta buit
      $reqlen = strlen($pass) * strlen($email);

      if ($reqlen > 0) { //Tots els camps tenen informacio
          $sql = "SELECT id_usuari, nom, email, contrassenya FROM usuari WHERE email=:email";
          $stmt = $conn->prepare($sql);
          $stmt->bindParam('email', $_POST['sessio_usuari'], PDO::PARAM_STR);
          $stmt->execute();
          $sessio = $stmt->fetch(PDO::FETCH_ASSOC);

          //comprovem si existeix el usuari i comprovem la contrassenya
          if($sessio != null) {
              if(count($sessio) > 0 && password_verify($pass,$sessio['contrassenya'])) {
                  $correcte = "correcte";
                  $_SESSION['sessio_iniciada'] = "true";
                  $_SESSION['usuari']['user_id'] = $sessio['id_usuari'];
                  $_SESSION['usuari']['nom'] = $sessio['nom'];
                  $_SESSION['wishlist'] = array();
                  //seleccionem els productes que el usuari te guardats a la wishlist
                  $sql = "SELECT id_producte_fk FROM wishlist WHERE id_usuari_fk=:id_user";
                  $stmt = $conn->prepare($sql);
                  $stmt->bindParam('id_user', $_SESSION['usuari']['user_id'], PDO::PARAM_INT);
                  $stmt->execute();
                  $productes = $stmt->fetchAll(PDO::FETCH_ASSOC);
                  $_SESSION['wishlist'] = array_column($productes, 'id_producte_fk');
              } else{
                  $correcte = "La contrasenya es incorrecta";
              }
          } else {
              $correcte = "El usuari indicat no existeix";
          }
      }

      return $correcte;

    } catch(PDOException $e) {
      echo "Error: " . $e->getMessage();
    }
}
