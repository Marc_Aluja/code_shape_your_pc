<?php

function consultaPerfil($conn, $actualitzar) {
    try {
      $format_guardat = "";
      if($actualitzar == true) {
          //Agafem els valors actuals de la BD de l'usuari en questió
          $sql = "SELECT * FROM usuari WHERE id_usuari=:id_user";
          $stmt = $conn->prepare($sql);
          $stmt->bindParam('id_user', $_SESSION['usuari']['user_id'], PDO::PARAM_INT);
          $stmt->execute();
          $dades_old_usuari = $stmt->fetch();

          //valors possibles que poden haver canviat
          $nom = $_POST['dades_nom'];
          $email = $_POST['dades_email'];
          $pass = $_POST['dades_contrassenya'];
          $rpass = $_POST['dades_rcontrassenya'];
          $direccio = $_POST['dades_direccio'];
          $detalls_direccio = $_POST['dades_detalls_direccio'];
          $pais = $_POST['dades_pais'];
          $provincia = $_POST['dades_provincia'];
          $ciutat = $_POST['dades_ciutat'];
          $cp = $_POST['dades_cp'];
          $telefon = $_POST['dades_telefon'];

          if($pass === $rpass) {
            if(filter_var($email,FILTER_VALIDATE_EMAIL) AND filter_var($cp,FILTER_SANITIZE_STRING) AND
                filter_var($nom,FILTER_SANITIZE_STRING) AND filter_var($direccio,FILTER_SANITIZE_STRING) AND
                filter_var($detalls_direccio,FILTER_SANITIZE_STRING) AND filter_var($provincia,FILTER_SANITIZE_STRING) AND
                filter_var($ciutat,FILTER_SANITIZE_STRING) AND filter_var($pais,FILTER_SANITIZE_STRING) AND
                filter_var($telefon,FILTER_SANITIZE_STRING)) {
                  //comprovem si s'ha de canviar la contrassenya
                  if($pass != null) {
                      $pass = password_hash($pass, PASSWORD_DEFAULT);
                  } else {
                      $pass = $dades_old_usuari['contrassenya'];
                  }
                  //comprovamos si el email nou es valid
                  $usuari = null;
                  if($email != $dades_old_usuari['email']) {
                      $sql="SELECT email FROM usuari WHERE email=:email";
                      $stmt = $conexion->prepare($sql);
                      $stmt->bindParam('email', $email, PDO::PARAM_STR);
                      $stmt->execute();
                      $usuari = $stmt->fetch();
                  }

                  if($usuari == null) {
                      //actualizamos valores
                      $sql="UPDATE usuari
                        SET nom = '".$nom."',
                            contrassenya = '".$pass."',
                            email = '".$email."',
                            direccio = '".$direccio."',
                            detall_dir = '".$detalls_direccio."',
                            pais = '".$pais."',
                            provincia = '".$provincia."',
                            ciutat = '".$ciutat."',
                            codi_postal = '".$cp."',
                            telefon = '".$telefon."'
                        WHERE id_usuari=".$_SESSION['usuari']['user_id'];
                      $stmt = $conn->prepare($sql);
                      $stmt->execute();
                  } else {
                      $format_guardat = "El email ja esta registrat al sistema, utilitza un diferent.";
                  }
              } else {
                  $format_guardat = "Validacio de dades introduides fallida";
              }
          } else {
              $format_guardat = "Les contrassenyes no han coincidit";
          }
      }

      //Ahora hacemos la consulta para mostrar los parametros del usuario, se hayan actualizado o no
      $sql = "SELECT * FROM usuari WHERE id_usuari=:id_user";
      $stmt = $conn->prepare($sql);
      $stmt->bindParam('id_user', $_SESSION['usuari']['user_id'], PDO::PARAM_INT);
      $stmt->execute();
      $dades_usuari = $stmt->fetch();

      $dades_usuari = array_map("htmlentities", $dades_usuari);

      return array($dades_usuari, $format_guardat);

    } catch(PDOException $e) {
      echo "Error: " . $e->getMessage();
    }
}
