<?php

function consultaDetalls($conn, $nom_producte)
{
    try {
        $general = "";
        $detalls = "";
        //seleccionem caracteristiques generals del producte
        $sql = "SELECT producte.*, categoria.nom AS nom_cat
                FROM categoria
                JOIN producte
                WHERE producte.nom=:nom_prod AND categoria.id_cat=producte.id_categoria_fk";
        $stmt = $conn->prepare($sql);
        $stmt->bindParam(':nom_prod', $nom_producte, PDO::PARAM_STR);
        $stmt->execute();
        $general = $stmt->fetch(PDO::FETCH_ASSOC);

        if($general != null) {
          $general = array_map("htmlentities", $general);
          $general['nom_cat'] = str_replace(' ', '_',  $general['nom_cat']);
          //seleccionem mitjana de puntuacio del producte i el nombre de valoracions
          $id_producte = $general['id_prod'];
          $sql = "SELECT AVG(puntuacio) as avg_puntuacio, COUNT(puntuacio) as n_valoracions
                  FROM valoracio
                  WHERE id_producte_fk=:id_prod";
          $stmt = $conn->prepare($sql);
          $stmt->bindParam('id_prod', $id_producte, PDO::PARAM_INT);
          $stmt->execute();
          $puntuacio = $stmt->fetch(PDO::FETCH_ASSOC);
          $general = array_merge($general,$puntuacio);
          //seleccionem caracteristiques particulars del producte
          $sql = "SELECT * FROM ".$general['nom_cat']." WHERE id_producte_fk=:id_prod";
          $stmt = $conn->prepare($sql);
          $stmt->bindParam('id_prod', $id_producte, PDO::PARAM_INT);
          $stmt->execute();
          $detalls = $stmt->fetch(PDO::FETCH_ASSOC);

          $detalls = array_map("htmlentities", $detalls);

          return array($detalls, $general);

        } else {
          return array("", "");
        }
    } catch(PDOException $e) {
      echo "Error: " . $e->getMessage();
    }
}

?>
