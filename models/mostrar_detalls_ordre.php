<?php

function consultarDetallsOrdre($conn){
    try {
      $sql = "SELECT * FROM ordre WHERE id_ordre=:id_or";
      $stmt = $conn->prepare($sql);
      $stmt->bindParam('id_or', $_GET['id_ordre'], PDO::PARAM_INT);
      $stmt->execute();
      $ordre = $stmt->fetch();

      $sql = "SELECT linia_ordre.*, producte.nom AS nom_prod, producte.imatge AS img_prod, producte.marca AS marca_prod
              FROM producte
              JOIN linia_ordre
              WHERE id_ordre_fk=:id_or AND producte.id_prod=linia_ordre.id_producte_fk";
      $stmt = $conn->prepare($sql);
      $stmt->bindParam('id_or', $_GET['id_ordre'], PDO::PARAM_INT);
      $stmt->execute();
      $detalls_ordre = $stmt->fetchAll();

      $sql = "SELECT * FROM dades_facturacio WHERE id_ordre_fk=:id_or";
      $stmt = $conn->prepare($sql);
      $stmt->bindParam('id_or', $_GET['id_ordre'], PDO::PARAM_INT);
      $stmt->execute();
      $dades_fact = $stmt->fetch();

      $ordre = array_map("htmlentities", $ordre);
      foreach ($detalls_ordre as $index => $detall) {
        $detalls_ordre[$index] = array_map("htmlentities", $detall);
      }
      $dades_fact[$index] = array_map("htmlentities", $dades_fact);

      return array($ordre, $detalls_ordre, $dades_fact);
      
    } catch(PDOException $e) {
      echo "Error: " . $e->getMessage();
    }
}
