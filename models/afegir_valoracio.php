<?php

function consultaAfegirValoracio($conn)
{
    try {
        $puntuacio = $_GET['punts'];
        $comentari = $_GET['comentari'];
        $data = date("Y/m/d");
        $id_prod = $_GET['id_prod'];
        $id_user = $_SESSION['usuari']['user_id'];


        $reqlen = strlen($puntuacio) * strlen($comentari) * strlen($id_prod) * strlen($id_user);

        if ($reqlen > 0) {
            $sql="SELECT * FROM valoracio WHERE id_usuari_fk=:id_user AND id_producte_fk=:id_prod";
            $stmt = $conn->prepare($sql);
            $stmt->bindParam('id_user', $id_user, PDO::PARAM_INT);
            $stmt->bindParam('id_prod', $id_prod, PDO::PARAM_INT);
            $stmt->execute();
            $valoracio = $stmt->fetch();

            if($valoracio == null) {
                $sql="INSERT INTO valoracio (puntuacio, comentari, data, id_producte_fk, id_usuari_fk) VALUES (?,?,?,?,?)";
                $stmt = $conn->prepare($sql);
                $stmt->execute([$puntuacio, $comentari, $data, $id_prod, $id_user]);
                $correcte = 'Gràcies per la valoració!';
            } else {
                $correcte = 'Ja has afegit una valoració per aquest producte.';
            }
        } else {
            $correcte = 'No ha iniciat sessio o el producte no existeix.';
        }

        return $correcte;

    } catch(PDOException $e) {
        echo "Error: " . $e->getMessage();
    }
}
