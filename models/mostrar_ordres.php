<?php

function consultarHistorialOrdres($conn){
    try {
      $sql = "SELECT * FROM ordre WHERE id_usuari_fk=:id_user";
      $stmt = $conn->prepare($sql);
      $stmt->bindParam('id_user', $_SESSION['usuari']['user_id'], PDO::PARAM_INT);
      $stmt->execute();
      $ordres = $stmt->fetchAll();

      foreach ($ordres as $index => $ordre) {
        $ordres[$index] = array_map("htmlentities", $ordre);
      }

      return $ordres;
      
    } catch(PDOException $e) {
      echo "Error: " . $e->getMessage();
    }
}
