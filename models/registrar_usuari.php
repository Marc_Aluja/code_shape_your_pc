<?php

function consultaRegistrar($conn)
{
    try {
      $nom = $_POST['registre_nom'];
      $email = $_POST['registre_email'];
      $pass = $_POST['registre_contrassenya'];
      $rpass = $_POST['registre_rcontrassenya'];
      $direccio = $_POST['registre_direccio'];
      $detalls_dir = $_POST['registre_detalls'];
      $pais = $_POST['registre_pais'];
      $provincia = $_POST['registre_provincia'];
      $ciutat = $_POST['registre_ciutat'];
      $cp = $_POST['registre_cp'];
      $telefon = $_POST['registre_telefon'];
      $correcte = "";

      $reqlen = strlen($nom) * strlen($pass) * strlen($rpass) * strlen($email) * strlen($direccio) * strlen($detalls_dir)
                * strlen($pais) * strlen($provincia) * strlen($ciutat) * strlen($cp) * strlen($telefon);

      if ($reqlen > 0) {
          if($pass === $rpass) {
              //Filtrem les diferents dades rebudes abans d'iniciar la pujada de les dades, excepte de la contrasenya que es tranforma a hash despres
              if(filter_var($email,FILTER_VALIDATE_EMAIL) AND filter_var($cp,FILTER_SANITIZE_STRING) AND
                  filter_var($nom,FILTER_SANITIZE_STRING) AND filter_var($direccio,FILTER_SANITIZE_STRING) AND
                  filter_var($detalls_dir,FILTER_SANITIZE_STRING) AND filter_var($provincia,FILTER_SANITIZE_STRING) AND
                  filter_var($ciutat,FILTER_SANITIZE_STRING) AND filter_var($pais,FILTER_SANITIZE_STRING) AND
                  filter_var($telefon,FILTER_SANITIZE_STRING)) {

                  //comprovem si hi ha algun usuari a la base de dades amb aquest email, ja que no es pot repetir
                  $sql="SELECT id_usuari, email, contrassenya FROM usuari WHERE email=:email";
                  $stmt = $conn->prepare($sql);
                  $stmt->bindParam('email', $email, PDO::PARAM_STR);
                  $stmt->execute();
                  $user = $stmt->fetch();

                  if($user == null) {
                      $sql="INSERT INTO usuari (nom, email, contrassenya, direccio, detall_dir, pais, provincia, ciutat,
                            codi_postal, telefon) VALUES (?,?,?,?,?,?,?,?,?,?)";
                      $stmt = $conn->prepare($sql);
                      $stmt->execute([$nom, $email, password_hash($pass,PASSWORD_DEFAULT), $direccio, $detalls_dir, $pais, $provincia,
                                          $ciutat, $cp, $telefon]);
                      $correcte = "correcte";
                  } else {
                      $correcte = 'Aquest email ja esta registrat a la web, utilizeu un nou o inicieu sessio';
                  }
              } else {
                  $correcte = 'El filtrat de les dades ha fallat, no utilitzi codi extrany';
              }
          } else {
              $correcte = 'Las contraseñes no son idènticques. Intenti-ho de nou.';
          }
      } else {
          $correcte = 'Si us plau, ompli totes les dades necessaries.';
      }

      return $correcte;

    } catch(PDOException $e) {
        echo "Error: " . $e->getMessage();
    }
}
