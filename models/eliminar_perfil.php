<?php

function consultaEliminarPerfil($conn)
{
    $retorn = "";
    try {
        $sql = "DELETE FROM usuari WHERE id_usuari=:id_user";
        $stmt = $conn->prepare($sql);
        $stmt->bindParam('id_user', $_SESSION['usuari']['user_id'], PDO::PARAM_INT);
        $stmt->execute();
        $retorn = "Compte eliminat correctament";
        
        return($retorn);

    } catch(PDOException $e) {
      echo "Error: " . $e->getMessage();
    }
}

?>
