<?php

function consultaActualitzarWishlist($conn)
{
    $id_prod = $_GET['id'];
    $usuari = $_SESSION['usuari']['user_id'];

    switch ($_GET['tipus']) {
        case 'afegir':
            array_push($_SESSION['wishlist'], $id_prod);
            try {
                $sql="INSERT INTO wishlist (id_producte_fk, id_usuari_fk) VALUES (?,?)";
                $stmt = $conn->prepare($sql);
                $stmt->execute([$id_prod, $usuari]);
            } catch(PDOException $e) {
                echo "Error: " . $e->getMessage();
            }
            break;
        case 'eliminar':
            foreach ($_SESSION['wishlist'] as $index => $value)  {
               if($value == $id_prod) {
                 unset($_SESSION['wishlist'][$index]);
                 break;
               }
            }
            try {
                $sql = "DELETE FROM wishlist WHERE id_producte_fk=:id_prod AND id_usuari_fk=:id_user";
                $stmt = $conn->prepare($sql);
                $stmt->bindParam('id_prod', $id_prod, PDO::PARAM_INT);
                $stmt->bindParam('id_user', $usuari, PDO::PARAM_INT);
                $stmt->execute();
            } catch(PDOException $e) {
                echo "Error: " . $e->getMessage();
            }
            break;
        default:
            break;
    }
}
