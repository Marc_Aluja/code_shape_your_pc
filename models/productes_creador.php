<?php

function consultaNomsProductesCreador($conn) {
  try {
    $nom_cat = $_GET['categoria'];
    if($_GET['tipus'] != "") {
      $tipus_mem = $_GET['tipus'];
      $sql = "SELECT producte.*, categoria.id_cat as id_cat, memories.tipus as tipus_mem
              FROM categoria, memories
              JOIN producte
              WHERE categoria.nom=:nom_cat AND producte.id_categoria_fk=id_cat
                    AND " . $nom_cat . ".id_producte_fk=producte.id_prod AND " . $nom_cat . ".tipus LIKE '%" . $tipus_mem . "%'";
      $stmt = $conn->prepare($sql);
      $stmt->bindParam('nom_cat', $nom_cat, PDO::PARAM_STR);
      $stmt->execute();
      $productes = $stmt->fetchAll(PDO::FETCH_ASSOC);
    } else {
      $sql = "SELECT producte.*, categoria.id_cat as id_cat
              FROM categoria
              JOIN producte
              WHERE categoria.nom=:nom_cat AND producte.id_categoria_fk=id_cat";
      $stmt = $conn->prepare($sql);
      $stmt->bindParam('nom_cat', $nom_cat, PDO::PARAM_STR);
      $stmt->execute();
      $productes = $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    foreach ($productes as $index => $producte) {
      $imatge = explode(" ", $producte['imatge']);
      $imatge[0] =  BASE_URL . $imatge[0];
      $producte['imatge'] = $imatge[0];
      $productes[$index] = $producte;
    }

    foreach ($productes as $index => $producte) {
      $productes[$index] = array_map("htmlentities", $producte);
    }

    return $productes;

  } catch(PDOException $e) {
    echo "Error: " . $e->getMessage();
  }
}
