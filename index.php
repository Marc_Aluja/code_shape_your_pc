<?php
session_start();

if(!isset($_SESSION['carret'])) {
  $_SESSION['carret']['quantitat'] = 0;
  $_SESSION['carret']['ids_productes'] = array();
  $_SESSION['carret']['productes'] = null;
  $_SESSION['carret']['avis'] = "";
  $_SESSION['carret']['ordinador'] = "false";
}

require_once __DIR__.'/config.php';

$action = $_GET['action'] ?? null;

switch ($action) {
    case 'mostrar_creador':
        require __DIR__.'/controllers/mostrar_creador.php';
        break;
    case 'productes_creador':
        require __DIR__.'/controllers/productes_creador.php';
        break;
    case 'afegir_ordinador':
        require __DIR__.'/controllers/afegir_ordinador.php';
        break;
    case 'mostrar_categories':
        require __DIR__.'/controllers/mostrar_categories.php';
        break;
    case 'mostrar_productes':
        require __DIR__.'/controllers/mostrar_productes.php';
        break;
    case 'mostrar_detalls_producte':
        require __DIR__.'/controllers/mostrar_detalls_producte.php';
        break;
    case 'comparar_productes':
        require __DIR__.'/controllers/comparar_productes.php';
        break;
    case 'mostrar_carret':
        require __DIR__.'/controllers/mostrar_carret.php';
        break;
    case 'actualitzar_carret':
        require __DIR__.'/controllers/actualitzar_carret.php';
        break;
    case 'mostrar_wishlist':
        require __DIR__.'/controllers/mostrar_wishlist.php';
        break;
    case 'actualitzar_wishlist':
        require __DIR__.'/controllers/actualitzar_wishlist.php';
        break;
    case 'mostrar_valoracions':
        require __DIR__.'/controllers/mostrar_valoracions.php';
        break;
    case 'afegir_valoracio':
        require __DIR__.'/controllers/afegir_valoracio.php';
        break;
    case 'iniciar_sessio':
        require __DIR__.'/controllers/iniciar_sessio.php';
        break;
    case 'registrar_usuari':
        require __DIR__.'/controllers/registrar_usuari.php';
        break;
    case 'editar_perfil':
        require __DIR__.'/controllers/editar_perfil.php';
        break;
    case 'eliminar_perfil':
        require __DIR__.'/controllers/eliminar_perfil.php';
        break;
    case 'mostrar_ordres':
        require __DIR__.'/controllers/mostrar_ordres.php';
        break;
    case 'mostrar_detalls_ordre':
        require __DIR__.'/controllers/mostrar_detalls_ordre.php';
        break;
    case 'cancelar_ordre':
        require __DIR__.'/controllers/cancelar_ordre.php';
        break;
    case 'subscripcio_web':
        require __DIR__.'/controllers/subscripcio_web.php';
        break;
    case 'llista_noms':
        require __DIR__.'/controllers/llista_noms.php';
        break;
    case 'realitzar_compra':
        require __DIR__.'/controllers/realitzar_compra.php';
        break;
    case 'confirmar_compra':
        require __DIR__.'/controllers/confirmar_compra.php';
        break;
    case 'tancar_sessio':
        session_destroy();
        header('Location: index.php?accion=');
        break;
    case 'mostrar_informacio':
        require __DIR__.'/controllers/mostrar_informacio.php';
        break;
    default:
        require __DIR__.'/controllers/pagina_inicial.php';
        break;
}

?>
