<?php
require __DIR__.'/../models/connectBD.php';
require __DIR__ . '/../models/llista_noms.php';

$connexio = connectarBD();
$productes = consultaNomsProductes($connexio);
echo json_encode($productes);
