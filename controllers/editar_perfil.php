<?php
require __DIR__ . '/../models/connectBD.php';
require __DIR__ . '/../models/editar_perfil.php';

$actualitzar = false;
if(isset($_POST['submit_editar'])) {
    $actualitzar = true;
    $format_guardat = "correcte";
}

if(isset($_SESSION['usuari']['user_id'])) {
  $connexio = connectarBD();
  list($dades, $missatge) = consultaPerfil($connexio,$actualitzar);

  if($actualitzar == true && $missatge == "") {
      $missatge = "Actualitzat amb èxit";
  }

  include __DIR__ . '/../views/editar_perfil.php';
} else {
  header('Location: index.php?accion=');
}
