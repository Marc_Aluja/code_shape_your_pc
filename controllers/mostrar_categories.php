<?php
require __DIR__.'/../models/connectBD.php';
require __DIR__ . '/../models/mostrar_categories.php';

$connexio = connectarBD();
$nom_super_cat = $_GET['nom'];
$categories = consultaCategories($connexio, $nom_super_cat);

if($nom_super_cat == "CREADOR") {
  include __DIR__ . '/../views/mostrar_creador.php';
} else {
  include __DIR__ . '/../views/mostrar_categories.php';
}
