<?php
require __DIR__.'/../models/connectBD.php';
require __DIR__ . '/../models/mostrar_ordres.php';

if(isset($_SESSION['usuari']['user_id'])) {
  $connexio = connectarBD();
  $ordres = consultarHistorialOrdres($connexio);

  include __DIR__ . '/../views/mostrar_ordres.php';
} else {
  header('Location: index.php?accion=');
}
