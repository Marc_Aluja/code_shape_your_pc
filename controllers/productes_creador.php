<?php
require __DIR__.'/../models/connectBD.php';
require __DIR__ . '/../models/productes_creador.php';

$connexio = connectarBD();
$productes = consultaNomsProductesCreador($connexio);
echo json_encode($productes);
