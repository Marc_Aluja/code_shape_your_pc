<?php
require __DIR__.'/../models/connectBD.php';
require __DIR__ . '/../models/mostrar_wishlist.php';

if(isset($_SESSION['usuari']['user_id'])) {
  $connexio = connectarBD();
  $productes_wishlist = consultaWishlist($connexio);

  include __DIR__ . '/../views/mostrar_wishlist.php';
} else {
  header('Location: index.php?accion=');
}
