<?php
require __DIR__.'/../models/connectBD.php';
require __DIR__ . '/../models/afegir_valoracio.php';

$connexio = connectarBD();
$missatge = consultaAfegirValoracio($connexio);

include __DIR__ . '/../views/afegir_valoracio.php';
