<?php
require __DIR__.'/../models/connectBD.php';
require __DIR__ . '/../models/eliminar_perfil.php';

$connexio = connectarBD();
$missatge = consultaEliminarPerfil($connexio);

if($missatge != "") {
  session_destroy();
  session_start();
  $_SESSION['avis_eliminat'] = '<script type="text/JavaScript">
                                  $(".text-avis-dialeg").text("'.$missatge.'");
                                  $("#dialeg_notificacio").trigger("click");
                                </script>';
  header('Location: index.php?action=');
}
