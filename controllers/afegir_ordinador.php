<?php
require __DIR__ . '/../models/afegir_ordinador.php';

$data = json_decode(file_get_contents('php://input'), true);
$dict_productes = $data["dict_prods"];
$productes = array_values($dict_productes);

//Buidem carret
$_SESSION['carret']['quantitat'] = 0;
$_SESSION['carret']['ids_productes'] = array();
$_SESSION['carret']['productes'] = null;
$_SESSION['carret']['avis'] = "";
$_SESSION['carret']['ordinador'] = "true";

foreach ($productes as $index => $producte) {
  consultaAfegirOrdinador($producte['id_prod'], 1, $producte['preu']);
}

$missatge = "correcte";

echo json_encode($missatge);
