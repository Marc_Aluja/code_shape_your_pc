<?php
$opcio = $_GET['opcio'];

switch ($opcio) {
    case 1:
        include __DIR__.'/../views/mostrar_privacitat.php';
        break;
    case 2:
        include __DIR__.'/../views/mostrar_termes.php';
        break;
    case 3:
        include __DIR__.'/../views/mostrar_enviaments.php';
        break;
    default:
        break;
}
