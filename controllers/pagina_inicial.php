<?php

require_once __DIR__.'/../models/connectBD.php';
require_once __DIR__.'/../models/pagina_inicial.php';

$connexio = connectarBD();
$super_categories = consultaSuperCategorias($connexio);

include __DIR__.'/../views/pagina_inicial.php';

?>
