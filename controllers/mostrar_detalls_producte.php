<?php
require __DIR__.'/../models/connectBD.php';
require __DIR__ . '/../models/mostrar_detalls_producte.php';

$connexio = connectarBD();
list($detalls, $carac_generals) = consultaDetalls($connexio, $_GET['producte']);

if($detalls == "") {
  include __DIR__ . '/../views/sense_resultats.php';
} else {
  //eliminem la primera columna del id i la ultima de la id referida
  array_shift($detalls);
  array_pop($detalls);
  include __DIR__ . '/../views/mostrar_detalls_producte.php';
}

?>
