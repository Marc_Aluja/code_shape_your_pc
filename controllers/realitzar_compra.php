<?php
require __DIR__.'/../models/connectBD.php';
require __DIR__ . '/../models/editar_perfil.php';
require __DIR__ . '/../models/mostrar_carret.php';

if(isset($_SESSION['usuari']['user_id'])) {
  $connexio = connectarBD();

  $actualitzar = false;
  list($dades, $missatge) = consultaPerfil($connexio,$actualitzar);
  $productes_carret = consultaCarret($connexio);

  $preu_carret = 0;
  foreach ($_SESSION['carret']['productes'] as $producte) {
    $preu_carret = $preu_carret + $producte['quantitat'] * $producte['preu'];
  }

  include __DIR__ . '/../views/realitzar_compra.php';
} else {
  header('Location: index.php?accion=');
}
