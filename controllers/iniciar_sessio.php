<?php
require_once __DIR__.'/../models/connectBD.php';
require_once __DIR__.'/../models/iniciar_sessio.php';

$valor = "";

if(isset($_POST['submit_sessio'])) {
    $connexio = connectarBD();
    $valor = consultaSessio($connexio);
}

if($valor == "correcte") {
    header("Location: index.php?action=''");
    exit();
}else{
    include __DIR__ . '/../views/iniciar_sessio.php';
}

?>
