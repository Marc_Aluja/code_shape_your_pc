<?php
require __DIR__.'/../models/connectBD.php';
require __DIR__ . '/../models/mostrar_productes.php';

$connexio = connectarBD();
list($productes, $tipus) = consultaProductes($connexio);
$nom_cat = $_GET['nom'];

include __DIR__ . '/../views/mostrar_productes.php';
