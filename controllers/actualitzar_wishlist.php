<?php
require __DIR__ . '/../models/connectBD.php';
require __DIR__ . '/../models/actualitzar_wishlist.php';
require __DIR__ . '/../models/mostrar_wishlist.php';

$connexio = connectarBD();
consultaActualitzarWishlist($connexio);

if(isset($_GET["recarregar"])) {
  $productes_wishlist = consultaWishlist($connexio);
  include __DIR__ . '/../views/mostrar_wishlist.php';
} else {
  include __DIR__ . '/../views/actualitzar_wishlist.php';  
}
