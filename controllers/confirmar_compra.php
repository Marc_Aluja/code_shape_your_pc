<?php
require __DIR__.'/../models/connectBD.php';
require __DIR__ . '/../models/confirmar_compra.php';

$connexio = connectarBD();
if(isset($_POST['factura-nom'])) {
  $missatge = consultaConfirmarCompra($connexio);
  include __DIR__ . '/../views/confirmar_compra.php';
} else {
  header("Location: index.php?action=");
  exit();
}
