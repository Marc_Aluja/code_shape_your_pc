<?php
require __DIR__.'/../models/connectBD.php';
require __DIR__ . '/../models/subscripcio_web.php';

$connexio = connectarBD();
$missatge = consultaSubscripcio($connexio);

include __DIR__ . '/../views/subscripcio_web.php';
