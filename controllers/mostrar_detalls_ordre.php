<?php
require __DIR__.'/../models/connectBD.php';
require __DIR__ . '/../models/mostrar_detalls_ordre.php';
require __DIR__ . '/../models/editar_perfil.php';

if(isset($_SESSION['usuari']['user_id'])) {
  $connexio = connectarBD();
  $actualitzar = false;
  list($dades_envia, $missatge) = consultaPerfil($connexio,$actualitzar);
  list($ordre, $detalls, $dades_facturacio) = consultarDetallsOrdre($connexio);

  include __DIR__ . '/../views/mostrar_detalls_ordre.php';
} else {
  header('Location: index.php?accion=');
}
