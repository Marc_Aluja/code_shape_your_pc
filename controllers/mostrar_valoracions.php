<?php
require __DIR__.'/../models/connectBD.php';
require __DIR__ . '/../models/mostrar_valoracions.php';

$connexio = connectarBD();
list($valoracions, $usuaris) = consultaValoracions($connexio);
$nom_prod = $_GET['nom_prod'];

include __DIR__ . '/../views/mostrar_valoracions.php';
