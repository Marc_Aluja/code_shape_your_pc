<?php
require __DIR__.'/../models/connectBD.php';
require __DIR__ . '/../models/mostrar_detalls_producte.php';

$connexio = connectarBD();

$productes = json_decode($_GET['productes']);
$detalls_productes = array();
$generals_productes = array();
foreach ($productes as $nom_producte) {
  list($detalls, $carac_generals) = consultaDetalls($connexio, $nom_producte);
  array_push($detalls_productes, $detalls);
  array_push($generals_productes, $carac_generals);
}
$detalls_keys = array_keys($detalls_productes[0]);
array_shift($detalls_keys);
array_pop($detalls_keys);

include __DIR__ . '/../views/comparar_productes.php';
