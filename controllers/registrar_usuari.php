<?php
require __DIR__ . '/../models/connectBD.php';
require __DIR__ . '/../models/registrar_usuari.php';

$valor = "";

if(isset($_POST['submit_registrar'])) {
    $connexio = connectarBD();
    $valor = consultaRegistrar($connexio);
}

if($valor == "correcte") {
    $valor = "El registre s'ha realitzat correctament";
}

if($valor != "") {
  $_SESSION['avis_registrat'] = '<script type="text/JavaScript">
                                  $(".text-avis-dialeg").text("'.$valor.'");
                                  $("#dialeg_notificacio").trigger("click");
                                </script>';
}

include __DIR__ . '/../views/registrar_usuari.php';

?>
