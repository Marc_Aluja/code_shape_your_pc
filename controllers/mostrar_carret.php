<?php
require __DIR__.'/../models/connectBD.php';
require __DIR__ . '/../models/mostrar_carret.php';

$connexio = connectarBD();
$productes_carret = consultaCarret($connexio);
$preu_carret = 0;

if (!empty($_SESSION['carret']['productes'])) {
  foreach ($_SESSION['carret']['productes'] as $producte) {
    $preu_carret = $preu_carret + $producte['quantitat'] * $producte['preu'];
  }
}

include __DIR__ . '/../views/mostrar_carret.php';
