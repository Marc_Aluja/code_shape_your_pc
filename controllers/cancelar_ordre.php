<?php
require __DIR__.'/../models/connectBD.php';
require __DIR__ . '/../models/cancelar_ordre.php';

$connexio = connectarBD();
$missatge = consultaCancelarOrdre($connexio);

if($missatge != "") {
  $_SESSION['avis_cancelat'] = '<script type="text/JavaScript">
                                  $(".text-avis-dialeg").text("'.$missatge.'");
                                  $("#dialeg_notificacio").trigger("click");
                                </script>';
  header('Location: index.php?action=mostrar_ordres');
}
