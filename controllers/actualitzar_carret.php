<?php
require __DIR__ . '/../models/actualitzar_carret.php';

$missatge = consultaActualitzarCarret();
$_SESSION['carret']['avis'] = $missatge;

if($missatge == "Eliminat" || $_GET['tipus'] != "afegir") {
  header("Location: index.php?action=mostrar_carret");
  exit();
} else {
  include __DIR__ . '/../views/actualitzar_carret.php';
}
